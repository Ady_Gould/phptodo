<?php
/**
 * Demo Classes using Login, Register, Forgotten Password
 *
 * Home page that is displayed to the user on successful login.
 * At the moment it is just the user's profile in brief. Details such
 * as number of ToDos could be added later.
 *
 * @File     :  /users-todos.php
 * @Project  :  phpToDo
 * @Author   :  Adrian Gould <Adrian.Gould@polytechnic.wa.edu.au>
 * @Date     :  14/11/2016
 * @Version  :  1.0
 * @Copyright:  Copyright 2016 TAFE OpenSource,
 * Released under the Creative Commons Share Alike license
 *
 * History:
 *
 * v 1.0 09/11/2016
 * Initial version
 */

require_once __DIR__ . "/vendor/autoload.php";
$title = "Users | Admin";

use TAFEOpenSource\Users;
use TAFEOpenSource\ToDos;

$usr        = new Users();
$todos_list = new ToDos();

$url = new \TAFEOpenSource\Url();
$root = $url->getUriNoScript();


if (empty($usr->isUserLoggedIn())) {
    session_destroy();
    $_SESSION['userSession'] = false;
    $usr->redirect('./index.php');
} // end if user is logged in

if (!$usr->isAdmin()) {
    $usr->redirect('./user-home.php');
}

$user     = $usr->getUserById($usr->getLoggedInUserId());

$id       = $user->id;
$username = $user->username;
$given    = $user->givenname;
$last     = $user->lastname;


$usersList = $usr->getAllUsers();

include_once __DIR__ . "/admin-header.php";
?>
    <div class="col-sm-2 col-sm-offset-0 hidden-xs">
        <div class="row">
            <div class="col-xs-12">
                <img class="img-circle img-responsive img-responsive"
                     src="<?= $root . $usr->getPhoto($id) ?>"
                     alt="Picture of <?= $user->givenname; ?> <?= $user->lastname; ?>">
                <p class="text-center"><?= $given; ?> <?= $last; ?></p>

            </div>

        </div>
    </div>
    <div class="col-sm-10 col-xs-12">
        <div class="row">
            <h1 class="col-xs-9">Users</h1>
            <div class="col-xs-3 text-right"><br>
                <button class="btn btn-primary">NEW</button>
            </div>
        </div>
        <div class="table-responsive">
            <table
                class="table table-bordered table-striped table-hover">
                <thead class="bg-primary">
                <tr>
                    <th>OL</th>
                    <th>ID</th>
                    <th>Given</th>
                    <th>Last</th>
                    <th>User</th>
                    <th>eMail</th>
                    <th>Password</th>
                    <th>Role</th>
                    <th>Status</th>
                    <th colspan="2">Action</th>
                </tr>
                </thead>
                <tbody class="">
                <?php

                if ($usersList != false) {
                    foreach ($usersList as $theUser) {
                        $active = $usr->isActive($theUser->id);
                        ?>
                        <tr>
                            <td><?= $active ?></td>
                            <td><?= $theUser->id ?></td>
                            <td><?= $theUser->givenname ?></td>
                            <td><?= $theUser->lastname ?></td>
                            <td>
                                <a href="admin-user-view-profile.php?userID=<?=id;?>">
                                    <?= $theUser->username ?>
                                </a>
                            </td>
                            <td><?= $theUser->email ?></td>
                            <td><?= $theUser->passwd ?></td>
                            <td><?= $theUser->userrole ?></td>
                            <td><?= $theUser->userstatus ?></td>
                            <td>
                                <button class="btn btn-sm btn-primary">
                                    Edit
                                </button>
                            </td>
                            <td>
                                <button class="btn btn-sm btn-danger">
                                    Delete
                                </button>
                            </td>
                        </tr>
                        <?php
                    } // end foreach
                } // end if $todos
                ?>
                </tbody>
                <tfoot class="bg-primary">
                <tr>
                    <td colspan="10">Total Users:
                        <?= count($usersList) ?>
                    </td>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
    </div>

<?php
include_once __DIR__ . "/admin-footer.php";

