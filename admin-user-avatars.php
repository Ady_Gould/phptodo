<?php
/**
 * Demo Classes using Login, Register, Forgotten Password
 *
 * Home page that is displayed to the user on successful login.
 * At the moment it is just the user's profile in brief. Details such
 * as number of ToDos could be added later.
 *
 * @File     :  /user-home.php
 * @Project  :  phpToDo
 * @Author   :  Adrian Gould <Adrian.Gould@polytechnic.wa.edu.au>
 * @Date     :  09/11/2016 2:11 PM
 * @Version  :  1.0
 * @Copyright:  Copyright 2016 TAFE OpenSource,
 * Released under the Creative Commons Share Alike license
 *
 * History:
 *
 * v 1.0 09/11/2016
 * Initial version
 */

require_once __DIR__ . "/vendor/autoload.php";

$title = "Avatars | User | Admin";

include_once __DIR__ . "/admin-header.php";


?>
    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Admin Dashboard
                <small>Avatars</small>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-dashboard"></i> <a
                        href="admin-home.php">Dashboard</a>
                </li>
                <li>
                    <i class="fa fa-adjust"></i> <a
                        href="admin-home.php">General</a>
                </li>
                <li class="active">
                    <i class="fa fa-file"></i> Avatars
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->

<?php

$colsWide    = 2;
$colsPerLine = 12 / $colsWide;
$count       = 0;
$oldPart     = '';

foreach (new DirectoryIterator(__DIR__ . '/assets/img/users') as $file) {
    if ($file->isFile()) {
        $fn        = $file->getFilename();
        $firstPart = strstr($fn, '-', true);

        if (strpos($fn, '@1x')) {
            if ($count == $colsPerLine) {
                echo '</div></div></div>';
                $count = 0;
            } // end if done all columns or if first part has changed

            if ($count == 0) {
                echo '<div class="row"><div class="col-xs-10 col-xs-offset-1"><div class="row">';
            } // end if count is zero

            if ($firstPart != $oldPart) {
                echo '<div class="row"><div class="col-xs-10"><h2>';
                echo ucfirst($firstPart) . '</h2></div></div>';
                $count = 0;
            } // end if first part is different from previous

            $oldPart = $firstPart;
            ?>
            <div class="col-xs-<?= $colsWide; ?>">
                <img
                    class="img-thumbnail img-thumbnail-rounded img-responsive"
                    src="assets/img/users/<?= $file->getFilename() ?>"
                    alt="Picture of <?= $file->getFilename() ?>">
                <p class="text-center">
                    <small>
                        <small>
                            <?= $file->getFilename() ?></small>
                    </small>
                </p>
            </div>
            <?php
            $count++;
        }// end if @1x found

    }// end if is file (not a directory)

} // end foreach 'file'

include_once __DIR__ . "/admin-footer.php";
