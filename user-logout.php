<?php
/**
 * Demo Classes using Login, Register, Forgotten Password
 *
 * Longer description, may be more than one line,
 * usually broken into lines of 80 characters or less.
 *
 * @File:       /user-logout.php
 * @Project:    phpToDo
 * @Author:     Adrian Gould <Adrian.Gould@polytechnic.wa.edu.au>
 * @Date:       09/11/2016 2:11 PM
 * @Version:    1.0
 * @Copyright:  Copyright 2016 TAFE OpenSource,
 * Released under the Creative Commons Share Alike license
 *
 * History:
 *
 * v 1.0 09/11/2016
 * Initial version
 */

require_once __DIR__ . "/vendor/autoload.php";

if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}

$title    = "Logout";

use TAFEOpenSource\Users;

$user = new Users();

if(!$user->isUserLoggedIn())
{
    $user->redirect('index.php');
}

if($user->isUserLoggedIn()!="")
{
    $user->logout();
    $user->redirect('index.php');
}


