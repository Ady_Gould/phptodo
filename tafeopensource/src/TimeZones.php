<?php
/**
 * Created by PhpStorm.
 * User: 13046820
 * Date: 29/11/2016
 * Time: 11:45 AM
 */
/**
 * Short Description (1 Line)
 *
 * Longer description, may be more than one line,
 * usually broken into lines of 80 characters or less.
 *
 * @File     :       /FOLDERS/TimeZones.php
 * @Project  :    phpToDo
 * @Author   :     13046820 <13046820@EMAIL.ADDRESS>
 * @Date     :       29/11/2016
 * @Version  :    RELEASE.MINOR.BUGFIX (eg 2.5.11)
 * @Copyright:  Copyright 2016 TAFE OpenSource,
 * Released under the Creative Commons Share Alike license
 *
 * History:
 *
 * v 1.0 29/11/2016
 * Initial version
 */

namespace TAFEOpenSource;

use DateTime;
use DateTimeZone;

class TimeZones
{
    // Properties
    /**
     * @var \PDO
     */
    private $conn;
    /**
     * @var int
     */
    private $lastId;

    // Methods
    /**
     * ToDos constructor.
     */
    public function __construct()
    {
        $db         = new Database();
        $this->conn = $db->dbConnection();
    } // end __construct

    public function getCountries()
    {
        $sql  = "SELECT * FROM country ORDER BY country_name";
        $stmt = $this->runQuery($sql);
        $stmt->execute();
        $records = $stmt->fetchAll();
        if ($records) {
            return $records;
        } else {
            return false;
        }
    }// end get all ToDos

    /**
     * @param $sql
     *
     * @return \PDOStatement
     */
    public function runQuery($sql)
    {
        $stmt = $this->conn->prepare($sql);

        return $stmt;
    }

    public function getTimezonesInCountry($countryCode = '')
    {
        return $this->generate_timezone_list($countryCode);
    }

    public function getTimezonesInCountryDB($countryCode = '')
    {
        $sql  = "SELECT * FROM zone WHERE country_code = :code";
        $stmt = $this->runQuery($sql);
        $stmt->execute(['code' => $countryCode]);
        $records = $stmt->fetchAll();
        if ($records) {
            return $records;
        } else {
            return false;
        }
    }

    public function getAllTimeZones($orderByName = true)
    {
        if ($orderByName) {
            $orderBy = "zone_name, country_code";
        } else {
            $orderBy = "country_code, zone_name";
        }
        $sql  = "SELECT * FROM zone ORDER BY :orderByThis";
        $stmt = $this->runQuery($sql);
        $stmt->execute(['orderByThis' => $orderBy]);
        $records = $stmt->fetchAll();
        if ($records) {
            return $records;
        } else {
            return false;
        }
    }

    public function time_sort($item1, $item2)
    {
        if ($item1['region'] == $item2['region']) {

            if ($item1['offset'] == $item2['offset']) {

                if ($item1['city'] == $item2['city']) {
                    return 0;
                }

                return ($item1['city'] > $item2['city']) ? 1 : -1;
            }

            return ($item1['offset'] > $item2['offset']) ? 1 : -1;

        }

        return ($item1['region'] > $item2['region']) ? 1 : -1;
    }


    // Modified version of the timezone list function from http://stackoverflow.com/a/17355238/507629
// Includes current time for each timezone (would help users who don't know what their timezone is)

    public function generate_timezone_list($country='')
    {
        static $regions = array(
            DateTimeZone::AFRICA,
            DateTimeZone::AMERICA,
            DateTimeZone::ANTARCTICA,
            DateTimeZone::ASIA,
            DateTimeZone::ATLANTIC,
            DateTimeZone::AUSTRALIA,
            DateTimeZone::EUROPE,
            DateTimeZone::INDIAN,
            DateTimeZone::PACIFIC,

        );

        $timezones = array();
        foreach ($regions as $region) {
            $timezones = array_merge($timezones,
                DateTimeZone::listIdentifiers($region));
        }

        $timezone_offsets = array();
        foreach ($timezones as $timezone) {
            $tz                          = new DateTimeZone($timezone);
            $timezone_offsets[$timezone] = $tz->getOffset(new DateTime);
        }

        // sort timezone by timezone name
        ksort($timezone_offsets);

        $timezone_list = array();
        foreach ($timezone_offsets as $timezone => $offset) {
            $offset_prefix    = $offset < 0 ? '-' : '+';
            $offset_formatted = gmdate('H:i', abs($offset));

            $prettyOffset = "UTC${offset_prefix}${offset_formatted}";

            $t             = new DateTimeZone($timezone);
            $c             = new DateTime(null, $t);
            $currentTime   = $c->format('g:i A');
            $currentTime24 = $c->format('G:i');

            $tzRegion        = strstr($timezone, '/', true);
            $tzCity          = str_replace('/', '', strstr($timezone, '/'));
            $timezone_list[$timezone] = [
                'offset'      => $prettyOffset,
                'timezone'    => $timezone,
                'region'      => $tzRegion,
                'city'        => $tzCity,
                'currently'   => $currentTime,
                'currently24' => $currentTime24,
            ];
        }

        usort($timezone_list, array('this','time_sort'));

        return $timezone_list;
    }


}
