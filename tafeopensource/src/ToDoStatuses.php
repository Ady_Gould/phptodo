<?php
/**
 * ToDoStatuses Class
 *
 * This class is responsible for interacting with the todostatuses table.
 *
 * @File     :  /tafeopensource/src/ToDoStatuses.php
 * @Project  :  phpToDo
 * @Author   :  Adrian Gould <Adrian.Gould@polytechnic.wa.edu.au>
 * @Date     :  24/11/2016
 * @Version  :  1.0
 * @Copyright:  Copyright 2016 TAFE OpenSource,
 * Released under the Creative Commons Share Alike license
 *
 * History:
 *
 * v 1.0 24/11/2016
 * Initial version
 */

namespace TAFEOpenSource;

use \TAFEOpenSource\Database;
use \PDO;
use \PDOException;

class ToDoStatuses
{

    // Define Properties
    private $conn;

    public function __construct()
    {
        $db         = new Database();
        $this->conn = $db->dbConnection();
    } // end __construct

    public function runQuery($sql)
    {
        $stmt = $this->conn->prepare($sql);

        return $stmt;
    }

    public function getAllToDoStatuses()
    {
        // create SQL to retrieve all users
        $sql = "SELECT * FROM todostatuses";
        // Prepare the SQL statement
        $stmt = $this->runQuery($sql);
        // Execute the statement
        $stmt->execute();
        // Fetch all the results
        $records = $stmt->fetchAll();
        // Return results to caller
        if ($stmt->rowCount() > 0) {
            return $records;
        }

        // send false result if no records found
        return false;
    }


    /**
     * @param $statusId
     *
     * @return array|mixed
     */
    public function getStatusByID($statusId)
    {
        $sql  = "SELECT * FROM todostatuses
                WHERE id = :theStatusId";
        $stmt = $this->runQuery($sql);
        $stmt->bindParam(':theStatusId', $statusId);
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            // Fetch ONE result
            $record = $stmt->fetch(PDO::FETCH_OBJ);

            return $record;
        }

        // if the getStatusById did not find a status then return a
        // false result
        return [];
    } // end getStatusById

}
