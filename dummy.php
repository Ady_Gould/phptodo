<?php
/**
 * This is a dummy page to use for testing methods as we create them
 */

require_once __DIR__ . "/vendor/autoload.php";

if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}

$title = "Dummy";

use TAFEOpenSource\Users;
use TAFEOpenSource\Url;
use TAFEOpenSource\UserRoles;
use TAFEOpenSource\UserStatuses;
use \Respect\Validation as valid;

$url      = new Url();
$users    = new Users();
$todos    = new \TAFEOpenSource\ToDos();
$roles    = new UserRoles();
$statuses = new UserStatuses();

/**
 * Timezones list with GMT offset
 *
 * @return array
 * @link http://stackoverflow.com/a/9328760
 */
function tz_list() {
    $zones_array = array();
    $timestamp = time();
    foreach(timezone_identifiers_list() as $key => $zone) {
        date_default_timezone_set($zone);
        $zones_array[$key]['zone'] = $zone;
        $zones_array[$key]['diff_from_GMT'] = 'UTC/GMT ' . date('P', $timestamp);
    }
    return $zones_array;
}

Users::dump( DateTimeZone::listIdentifiers(DateTimeZone::ALL));
echo "<h1>Show all of user 4's todos</h1>";
echo "<pre>";
var_dump($todos->getAllToDosByUser(4));
echo "</pre>";

echo "<h1>Show User 4's ToDos with ALL in the title/description</h1>";
echo "<pre>";
var_dump($todos->getAllToDosByUser(4, "all"));
echo "</pre>";

echo "<h1>Show all of user 4's todos in alphabetical order</h1>";
echo "<pre>";
var_dump($todos->getAllToDosByUser(4, '', 'title'));
echo "</pre>";

$number = 123;
$result = valid::numericVal()->validate($number); // true


$usernameValidator = valid::alnum()->noWhitespace()->length(1, 15);
$result            = $usernameValidator->validate('alganet'); // true


try {
    $usernameValidator->assert('really messed up screen#name');
} catch (NestedValidationException $exception) {
    print_r($exception->getMessages());
}
