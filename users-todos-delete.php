<?php
/**
 * Delete ToDos
 *
 * This page is used to Mark ToDos for Deletion on a user's list. It is
 * for logged in users only. Admin users will need a special page for deleting
 * todos for any user besides themselves.
 *
 * @File     :  /users-todos-delete.php
 * @Project  :  phpToDo
 * @Author   :  Adrian Gould <Adrian.Gould@polytechnic.wa.edu.au>
 * @Date     :  26/11/2016
 * @Version  :  1.0
 * @Copyright:  Copyright 2016 TAFE OpenSource,
 * Released under the Creative Commons Share Alike license
 *
 * History:
 *
 * v 1.0 24611/2016
 * Initial version
 */

if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}

require_once __DIR__ . "/vendor/autoload.php";

$title = "Delete | ToDos | User";

use TAFEOpenSource\Users;
use TAFEOpenSource\Url;
use TAFEOpenSource\UserRoles;
use TAFEOpenSource\UserStatuses;
use TAFEOpenSource\ToDos;
use \Respect\Validation\Validator as validator;

$users    = new Users();
$roles    = new UserRoles();
$statuses = new UserStatuses();
$todos    = new ToDos();
$url      = new Url();
$root     = $url->getUriNoScript();

if ( ! isset($_POST) || empty($users->isUserLoggedIn())) {
    session_destroy();
    $_SESSION['userSession'] = false;
    $users->redirect('index.php');
} else {

    // get logged in user details
    $user       = $users->getUserById($users->getLoggedInUserId());
    $id         = $user->id;
    $username   = $user->username;
    $given      = $user->givenname;
    $last       = $user->lastname;
    $userRole   = $roles->getRoleByID($user->userrole);
    $userStatus = $statuses->getUserStatusByID($user->userstatus);
    $loginTime  = $user->date_login;

    if (isset($_POST['hidDeleteToDoID'])) {
        // get the ToDos ID
        $toDoID          = $_POST['hidDeleteToDoID'];
        $theToDoToDelete = $todos->getToDoByUserandID($id, $toDoID);

        // check to see if the form posted and that the submit button was pressed
        if ( ! empty($_POST['btnConfirmDeleteToDo'])) {
            // id = user id of whom is logged in
            $todos->deleteToDo($id, $toDoID);

            // presume no errors and redirect
            $users->redirect("{$root}users-todos.php");

        } else {

            if ( ! isset($_POST['btnToDoDelete'])) {
                // redirect back to the user's ToDos
                $users->redirect($root . 'users-todos.php');
            } // end if not btnToDoDelete

        } //end if btnConfirmDeleteToDo

    } // end ToDoID is known

} // end if not logged in

include_once __DIR__ . "/site-header.php";

?>
    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                User Dashboard
                <small>ToDos</small>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="user-home.php"> Home</a>
                </li>
                <li class="active">
                    <i class="fa fa-list"></i>
                    <a href="users-todos.php"> ToDos</a>
                </li>
                <li class="active">
                    <i class="fa fa-remove"></i> Delete
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->
    <div class="col-xs-12">

        <div class="row">
            <div class="col-sm-3">Title</div>
            <div class="col-sm-9"><?= $theToDoToDelete->title; ?></div>
        </div>
        <div class="row">
            <div class="col-sm-3">Description</div>
            <div
                class="col-sm-6"><?= $theToDoToDelete->description; ?></div>
        </div>
        <div class="row">
            <div class="col-sm-3">Priority</div>
            <div class="col-sm-6"><?= $theToDoToDelete->priority; ?></div>
        </div>
        <div class="row">
            <div class="col-sm-3">Date Due</div>
            <div class="col-sm-6"><?= $theToDoToDelete->date_due; ?></div>
        </div>

        <form class="form-horizontal" method="post">
            <div class="form-group">
                <div class="col-sm-6 col-sm-offset-3">
                    <a class="btn btn-success"
                       href="<?= $root ?>users-todos.php">
                        <i class="fa fa-stop"></i>
                        Cancel
                    </a>

                    <button class="btn btn-danger" type="submit"
                            name="btnConfirmDeleteToDo"
                            id="btnConfirmDeleteToDo"
                            value="Delete!">
                        <i class="fa fa-remove"></i>
                        Delete!
                    </button>

                    <input type="hidden"
                           name="hidDeleteToDoID"
                           value="<?= ($theToDoToDelete->id); ?>" />
                </div>
            </div>
        </form>


    </div>

<?php

include_once __DIR__ . "/site-footer.php";
