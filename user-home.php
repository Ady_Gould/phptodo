<?php
/**
 * Demo Classes using Login, Register, Forgotten Password
 *
 * Home page that is displayed to the user on successful login.
 * At the moment it is just the user's profile in brief. Details such
 * as number of ToDos could be added later.
 *
 * @File     :  /user-home.php
 * @Project  :  phpToDo
 * @Author   :  Adrian Gould <Adrian.Gould@polytechnic.wa.edu.au>
 * @Date     :  09/11/2016 2:11 PM
 * @Version  :  1.0
 * @Copyright:  Copyright 2016 TAFE OpenSource,
 * Released under the Creative Commons Share Alike license
 *
 * History:
 *
 * v 1.0 09/11/2016
 * Initial version
 */

require_once __DIR__ . "/vendor/autoload.php";

if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}

$title = "Home";

use TAFEOpenSource\UserRoles;
use TAFEOpenSource\Users;
use TAFEOpenSource\UserStatuses;

$user_home = new Users();
$roles     = new UserRoles();
$statuses  = new UserStatuses();


if (empty($user_home->isUserLoggedIn())) {
    session_destroy();
    $_SESSION['userSession'] = false;
    $user_home->redirect('index.php');
}

$user       = $user_home->getUserById($_SESSION['userSession']);
$id         = $user->id;
$username   = $user->username;
$given      = $user->givenname;
$last       = $user->lastname;
$country    = $user->country;
$timezone   = $user->timezone;
$userRole   = $roles->getRoleByID($user->userrole);
$userStatus = $statuses->getUserStatusByID($user->userstatus);

include_once __DIR__ . "/site-header.php";

?>
    <div class="row">
        <div class="col-sm-2 col-sm-offset-0 col-xs-12 hidden-xs">
            <img class="img-circle img-responsive img-responsive"
                 src="<?= $user_home->getPhoto($id) ?>"
                 alt="Picture of <?= $user->givenname; ?> <?= $user->lastname; ?>">
            <div class="text-center">
                <h4><?= $given; ?> <?= $last; ?></h4>
                <p>Username: <?= $username; ?></p>
                <p>Logged in: <br><?= $user->date_login; ?></p>
                <p>Country: <?= $user->country; ?></p>
                <p>Timezone: <?= $user->timezone; ?></p>
            </div>
        </div>
        <div class="col-md-8 col-md-offset-0 col-sm-8 col-sm-offset-0 col-xs-12">
            <div class="row">
                <div class="col-xs-12">
                    <h1>User Dashboard <small><?=$given;?>'s Home</small></h1>
                </div>
                <div class="col-xs-6 col-sm-4">
                    <div class="panel panel-primary panel-inverse">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-list-alt fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <p class="fa-3x">8</p>
                                    <h4>Open</h4>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                <span class="pull-right">
                                    <i class="fa fa-arrow-circle-right"></i>
                                </span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4">
                    <div class="panel panel-danger panel-inverse">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-clock-o fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <p class="fa-3x">16</p>
                                    <h4>Waiting</h4>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right">
                                    <i class="fa fa-arrow-circle-right"></i>
                                </span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4">
                    <div class="panel panel-success panel-inverse">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-check-circle fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <p class="fa-3x">126</p>
                                    <h4>Completed</h4>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right">
                                    <i class="fa fa-arrow-circle-right"></i>
                                </span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4">
                    <div class="panel panel-default panel-inverse">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-coffee fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <p class="fa-3x">99</p>
                                    <h4>Default Inverse Panel</h4>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right">
                                    <i class="fa fa-arrow-circle-right"></i>
                                </span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4">
                    <div class="panel panel-gold panel-inverse">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-briefcase fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <p class="fa-3x">999</p>
                                    <h4>Gold Inverse Panel</h4>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right">
                                    <i class="fa fa-arrow-circle-right"></i>
                                </span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-xs-6 col-sm-4">
                    <div class="panel panel-purple panel-inverse">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-area-chart fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <p class="fa-3x">9999</p>
                                    <h4>Purple Inverse Panel</h4>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right">
                                    <i class="fa fa-arrow-circle-right"></i>
                                </span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
include_once __DIR__ . "/site-footer.php";
