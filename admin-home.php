<?php
/**
 * Demo Classes using Login, Register, Forgotten Password
 *
 * Home page that is displayed to the user on successful login.
 * At the moment it is just the user's profile in brief. Details such
 * as number of ToDos could be added later.
 *
 * @File     :  /user-home.php
 * @Project  :  phpToDo
 * @Author   :  Adrian Gould <Adrian.Gould@polytechnic.wa.edu.au>
 * @Date     :  09/11/2016 2:11 PM
 * @Version  :  1.0
 * @Copyright:  Copyright 2016 TAFE OpenSource,
 * Released under the Creative Commons Share Alike license
 *
 * History:
 *
 * v 1.0 09/11/2016
 * Initial version
 */

require_once __DIR__ . "/vendor/autoload.php";
$title = "Home";

use TAFEOpenSource\Url;
use TAFEOpenSource\UserRoles;
use TAFEOpenSource\Users;
use TAFEOpenSource\UserStatuses;

$user_home = new Users();
$roles     = new UserRoles();
$statuses  = new UserStatuses();
$url       = new Url();

$root = $url->getUriNoScript();

if (empty($user_home->isUserLoggedIn())) {
    session_destroy();
    $_SESSION['userSession'] = false;
    $user_home->redirect('index.php');
}

if ( ! $user_home->isAdmin()) {
    $user_home->redirect($root . 'user-home.php');
}

$user       = $user_home->getUserById($user_home->getLoggedInUserId());
$id         = $user->id;
$username   = $user->username;
$given      = $user->givenname;
$last       = $user->lastname;
$fullName   = $given . ' ' . $last;
$loginAt    = $user->date_login;
$userRole   = $roles->getRoleByID($user->userrole);
$userStatus = $statuses->getUserStatusByID($user->userstatus);

$loginTime = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $loginAt)
                           ->format('H:i:s');
$loginDate = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $loginAt)
                           ->format('D j M, Y');

include_once __DIR__ . "/admin-header.php";

?>
    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Admin Dashboard
                <small>Welcome <?= $given; ?></small>
            </h1>

            <small class="text-muted">
                Logged in at <?= $loginTime; ?> on <?= $loginDate; ?>
            </small>

            <ol class="breadcrumb">
                <li class="active">
                    <i class="fa fa-dashboard"></i> <a
                        href="admin-home.php">Dashboard</a>
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-list-alt fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">26</div>
                            <div>New ToDos</div>
                        </div>
                    </div>
                </div>
                <a href="#">
                    <div class="panel-footer">
                                    <span
                                        class="pull-left">View Details</span>
                        <span class="pull-right"><i
                                class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-md-3">
            <div class="panel panel-green">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-users fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">12</div>
                            <div>New Users!</div>
                        </div>
                    </div>
                </div>
                <a href="#">
                    <div class="panel-footer">
                                    <span
                                        class="pull-left">View Details</span>
                        <span class="pull-right"><i
                                class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-md-3">
            <div class="panel panel-yellow">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-tags fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">124</div>
                            <div>New Tags!</div>
                        </div>
                    </div>
                </div>
                <a href="#">
                    <div class="panel-footer">
                                    <span
                                        class="pull-left">View Details</span>
                        <span class="pull-right"><i
                                class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-md-3">
            <div class="panel panel-red">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-envelope fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">13</div>
                            <div>Enquiries!</div>
                        </div>
                    </div>
                </div>
                <a href="#">
                    <div class="panel-footer">
                                    <span
                                        class="pull-left">View Details</span>
                        <span class="pull-right"><i
                                class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
    </div>


    </div>

<?php
include_once __DIR__ . "/admin-footer.php";
