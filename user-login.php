<?php
/**
 * Demo Classes using Login, Register, Forgotten Password
 *
 * Longer description, may be more than one line,
 * usually broken into lines of 80 characters or less.
 *
 * @File     :       /user-login.php
 * @Project  :    phpToDo
 * @Author   :     Adrian Gould <Adrian.Gould@polytechnic.wa.edu.au>
 * @Date     :       09/11/2016 2:11 PM
 * @Version  :    1.0
 * @Copyright:  Copyright 2016 TAFE OpenSource,
 * Released under the Creative Commons Share Alike license
 *
 * History:
 *
 * v 1.0 09/11/2016
 * Initial version
 */

require_once __DIR__ . "/vendor/autoload.php";

if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}

// Set the title for the page
$title = "Login";
use TAFEOpenSource\Users;
use TAFEOpenSource\Url;

$user_login = new Users();

if ($user_login->isUserLoggedIn() != "") {
    $user_login->redirect('user-home.php');
}

if (isset($_POST['btn-login'])) {
    $email = trim($_POST['txtemail']);
    $upass = trim($_POST['txtupass']);

    if ($user_login->login($email, $upass)) {
        $user_login->updateActivity();
        $user_login->redirect('user-home.php');
    }
}

$errorCode = '';
if (isset($_GET['error'])) {
    $errorCode = $_GET['error'];
} elseif (isset($_GET['inactive'])) {
    $errorCode = 'W';
}


include_once __DIR__ . "/site-header.php";

?>

    <div class="row">
        <div
            class="col-md-2 col-md-offset-2 col-sm-2 col-sm-offset-1 col-xs-offset-0 hidden-xs">
            <img class="img-responsive img-circle"
                 src="assets/img/users/anonymous-01@1x.png"></div>
        <div
            class="col-md-6 col-md-offset-0 col-sm-8 col-sm-offset-0 col-xs-10 col-xs-offset-1">
            <h1>Log In</h1>
            <form class="form-horizontal" method="post">
                <div class="form-group">
                    <div class="col-sm-4">
                        <label class="control-label" for="txtemail">
                            eMail / Username
                        </label>
                    </div>
                    <div class="col-sm-8">
                        <input class="form-control" type="text"
                               name="txtemail"
                               placeholder="Username or eMail address">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-4">
                        <label class="control-label">
                            Password
                        </label>
                    </div>
                    <div class="col-sm-8">
                        <input class="form-control" type="password"
                               name="txtupass" required
                               placeholder="Password">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-4">
                        <label class="control-label">
                        </label>
                    </div>
                    <div class="col-sm-2">
                        <button class="btn btn-large btn-primary"
                                type="submit" name="btn-login">
                            Sign in
                        </button>
                    </div>
                    <div class="col-sm-2">
                        <a href="user-registration.php" class="btn btn-large">
                            Sign Up
                        </a>
                    </div>
                    <div class="col-sm-4">
                        <a href="user-forgotpass.php" class="btn btn-large">
                            Lost your Password?
                        </a>
                    </div>
                </div>
                <?php

                switch ($errorCode) {
                    case 'S':
                        $eLevel = 'info';
                        $eMsg   = '<h4>Account Suspended</h4>
                        <p>The user has suspended the account. Reinstating 
                        the account requires the user to verify their 
                        details and request a new password.</p>';
                        break;
                    case 'D':
                        $eLevel = 'danger';
                        $eMsg   = '<h4>Account Deleted</h4>
                        <p>The user has deleted this account. Access has 
                        been removed.</p>
                        <p>The Account may be reinstated within the 
                        guidelines given in the Help/FAQ.</p>';
                        break;
                    case 'T':
                        $eLevel = 'danger';
                        $eMsg   = '<p>Account Terminated</p>
                        <p>This account has been terminated by the 
                        adminsitration due to one or breaches of the 
                        acceptible usage policy.</p>
                        <p>Explanations of the termination reasons are 
                        found in the FAQ/Help.</p>';
                        break;
                    case 'W':
                        $eLevel = 'warning';
                        $eMsg   = '<h4>Account not Activated.</h4>
                        <p>We sent you an email with account verification 
                        details. </p>
                        <p>Please go to your eMail inbox and Activate 
                        it. If you cannot see the email, please check your 
                        Spam folder.</p>
                         <p>If you did not get the verification eMail then 
                         you may request a new one.</p>
                        <p><button class="btn btn-danger">Request A New 
                        Verification eMail</button></p>';
                        break;
                    case 'X' :
                        $eLevel = 'info';
                        $eMsg   = '<h4>Account in Unknown State</h4>
                        <p>The account required an administrator to remove 
                        this unknown state. Please contact them via the 
                        contact page.</p>';
                        break;
                    case 'L':
                        $eLevel = 'danger';
                        $eMsg   = '<h4>Incorrect Account Details</h4>
                        <p>The account details you gave were incorrect. If 
                        you have forgotten your password then you may 
                        request a new one by pressing the 
                        "<a href="user-forgotpass.php" >Lost your Password?</a>" 
                        button.</p>
                        <p><a href="user-forgotpass.php" class="btn btn-default">
                        Lost your Password?
                        </a></p>';
                        break;
                    default:
                        $eLevel = '';
                        $eMsg   = '';
                }
                if ( ! empty($eLevel)) {
                    ?>
                    <div class='alert alert-<?= $eLevel; ?>'>
                        <button class='close'
                                data-dismiss='alert'>&times;</button>
                        <?= $eMsg; ?>
                    </div>
                    <?php
                } ?>
        </div>
    </div>


<?php
include_once __DIR__ . "/site-footer.php";
