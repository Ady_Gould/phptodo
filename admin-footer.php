<?php
/**
 * Demo Classes using Login, Register, Forgotten Password
 *
 * The admin footer content provides a consistent layout and reduces
 * editing of files.
 *
 * @File     :   /admin-footer.php
 * @Project  :   phpToDo
 * @Author   :   Adrian Gould <Adrian.Gould@polytechnic.wa.edu.au>
 * @Date     :   26/11/2016 2:11 PM
 * @Version  :   1.1
 * @Copyright:  Copyright 2016 TAFE OpenSource,
 * Released under the Creative Commons Share Alike license
 *
 * History:
 *
 * v 1.1 26/11/2016
 * Added application JS to run datepicker on non supporting browsers.
 *
 * v 1.0 09/11/2016
 * Initial version
 */

$url = new \TAFEOpenSource\Url();

$root = $url->getUriNoScript();
?>

    </div>
    <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->

    <div class="footer-dark-admin text-center">
        <footer>
            <div class="container">

                <p class="copyright">TAFE OpenSource © 2016</p>
            </div>
        </footer>

    </div>

    </div>
    <!-- /#wrapper -->
    <script src="<?= $root; ?>assets/js/jquery.min.js"></script>
    <script src="<?= $root; ?>assets/js/vendor/moment.min.js"></script>
    <script src="<?= $root; ?>assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?= $root; ?>assets/js/bootstrap-datetimepicker.min.js"></script>
    <script src="<?= $root; ?>assets/js/application.js"></script>
    </body>

    </html>

<?php
