<?php
/**
 * Demo Classes using Login, Register, Forgotten Password
 *
 * The footer content provides a consistent layout and reduces
 * editing of files.
 *
 * @File     :  /site-footer.php
 * @Project  :  phpToDo
 * @Author   :  Adrian Gould <Adrian.Gould@polytechnic.wa.edu.au>
 * @Date     :  23/11/2016 2:11 PM
 * @Version  :  1.1
 * @Copyright:  Copyright 2016 TAFE OpenSource,
 * Released under the Creative Commons Share Alike license
 *
 * History:
 *
 * v 1.1 23/11/2016
 * Added application JS to run datepicker on non supporting browsers.
 *
 * v 1.0 09/11/2016
 * Initial version
 */

$url = new \TAFEOpenSource\Url();

$root = $url->getUriNoScript();

?>
    </div> <!-- end container -->

    <div class="footer-dark">
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-push-6 item text">
                        <h3><img
                                src="assets/img/logos/TAFE-OpenSource-Wordmark-Logo-no-fill@1x.png"
                                alt="TAFE OpenSource Logo"
                                class="img-small">TAFE OpenSource</h3>
                        <p>TAFE OpenSource is an open source community
                            for TAFE Lecturers, Students and
                            Administrators alike. TAFEos aims to provide
                            resources, sample code and more to assist
                            students in their studies and the
                            development of
                            a variety of skills. For details on how to
                            contribute and help, see the About
                            section.</p>
                    </div>
                    <div class="col-md-3 col-md-pull-6 col-sm-4 item">
                        <h3>Quick Nav</h3>
                        <ul>
                            <li><a href="user-home.php">Profile</a></li>
                            <li><a href="index.php">Log-in</a></li>
                            <li><a href="user-registration.php">Register</a>
                            </li>
                            <li><a href="user-logout.php">Logout</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3 col-md-pull-6 col-sm-4 item">
                        <h3>About</h3>
                        <ul>
                            <li><a href="company.php">Company</a></li>
                            <li><a href="team.php">Team</a></li>
                            <li><a href="careers.php">Careers</a></li>
                            <li><a href="contact.php">Contact</a></li>
                        </ul>
                    </div>
                    <div class="col-md-12 col-sm-4 item social">
                        <a href="#">
                            <i class="fa fa-facebook-f"></i>
                        </a>
                        <a href="#">
                            <i class="fa fa-twitter"></i>
                        </a>
                        <a href="#">
                            <i class="fa fa-bitbucket"></i>
                        </a>
                        <a href="#">
                            <i class="fa fa-creative-commons"></i>
                        </a>
                    </div>
                </div>
                <p class="copyright">TAFE OpenSource © 2016</p>
            </div>
        </footer>
    </div>
    <script src="<?= $root; ?>assets/js/jquery.min.js"></script>
    <script src="<?= $root; ?>assets/js/vendor/moment.min.js"></script>
    <script src="<?= $root; ?>assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?= $root; ?>assets/js/bootstrap-datetimepicker.min.js"></script>
    <script src="<?= $root; ?>assets/js/application.js"></script>

    </body>

    </html>

<?php
