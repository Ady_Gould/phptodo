<?php
/**
 * Demo Classes using Login, Register, Forgotten Password
 *
 * Longer description, may be more than one line,
 * usually broken into lines of 80 characters or less.
 *
 * @File     :  /user-registration.php
 * @Project  :  phpToDo
 * @Author   :  Adrian Gould <Adrian.Gould@polytechnic.wa.edu.au>
 * @Date     :  12/11/2016
 * @Version  :  1.1
 * @Copyright:  Copyright 2016 TAFE OpenSource,
 * Released under the Creative Commons Share Alike license
 *
 * History:
 *
 * v 1.1 12/11/2016
 * Added automated base URL inclusion for the email message. This
 * revolves around the Url class.
 *
 * v 1.0 09/11/2016
 * Initial version
 */

require_once __DIR__ . "/vendor/autoload.php";
$title = "Register with Us";

if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}

use TAFEOpenSource\Users;
use TAFEOpenSource\Url;

$url = new Url();
$base = $url->getUriNoScript();

$reg_user = new Users();

$eMsg = '';
$eLevel = '';

if ($reg_user->isUserLoggedIn() != '') {
    $reg_user->redirect('user-home.php');
}

if (isset($_POST['btn-registration'])) {
    $gname = trim($_POST['txtgname']);
    $lname = trim($_POST['txtlname']);
    $uname = trim($_POST['txtuname']);
    $email = trim($_POST['txtemail']);
    $upass = trim($_POST['txtpass']);
    $code = md5(uniqid(rand()));

    // Late we need to do server side validation of the user's form
    // submission. This will be added after the basic management
    // of a user's ToDos.

    $emailRegistered = $reg_user->getUser($email);
    $userNameRegistered = $reg_user->getUser($uname);

    if ($emailRegistered) {
        $eLevel = 'warning';
        $eMsg = '
        <h4>eMail already in use</h4>
        <p>That email has been used before. Please Try another one.</p>
        <p>If you have forgotten your password then <a href="user-forgotpass.php">
            request a password reset</a>.
        </p>';
    }
    if ($userNameRegistered) {
        $eLevel = 'warning';
        $eMsg = '
        <h4>Username already in use</h4>
        <p>That username has been used before. Please Try another one.</p>
        <p>If you have forgotten your password then 
        <a href="user-forgotpass.php">request a password reset</a>.
        </p>';
    }

    if ((!$emailRegistered) && (!$userNameRegistered)) {
        $userRegistered = $reg_user->register($gname, $lname, $uname, $email, $upass, $code);

        if ($userRegistered) {
            //
            $lastID = $reg_user->getLastID();
            // Encode the user account ID so that it is less likely
            // to be just 'read' and used.
            $key = base64_encode($lastID);
            $id = $key;

            $message = "     
                    Hello $gname,
                    <br /><br />
                    Welcome to phpToDo!<br/>
                    To complete your registration, please click the following 
                    link.</br>
                    <br /><br />
                    <a href='{$base}user-verify.php?id=$id&code=$code'>Press me to 
                    Activate your account :)</a>
                    <br /><br />
                    Thanks,";

            $subject = "Confirm Registration";

            $reg_user->send_mail($email, $message, $subject);
            $eLevel = 'success';
            $eMsg = "
                     <h4>Success!</h4>  
                     <p>We've sent an email to $email.</p>
                     <p>Please click on the confirmation link 
                     in the email to create your account.</p> 
                      ";
        } else {
            echo "sorry , Query could not be executed...";
        }
    }
}

include_once __DIR__ . "/site-header.php";

?>

    <div class="row">
        <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2
                col-xs-10 col-xs-offset-1">
            <h2>Sign Up / Registration</h2>
            <?php
            if (!empty($eLevel)) {
                ?>
                <div class='alert alert-<?= $eLevel; ?>'>
                    <button class='close'
                            data-dismiss='alert'>&times;</button>
                    <?= $eMsg; ?>
                </div>
                <?php
            } ?>
            <form class="form-horizontal" method="post">
                <div class="form-group">
                    <div class="col-sm-4">
                        <label class="control-label" for="txtgname">
                            Given Name
                        </label>
                    </div>
                    <div class="col-sm-8">
                        <input class="form-control" type="text"
                               name="txtgname" required
                               placeholder="Given Name (first name)">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-4">
                        <label class="control-label" for="txtlname">
                            Last Name
                        </label>
                    </div>
                    <div class="col-sm-8">
                        <input class="form-control" type="text"
                               name="txtlname" required
                               placeholder="Last name (surname)">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-4">
                        <label class="control-label" for="txtuname">
                            Username
                        </label>
                    </div>
                    <div class="col-sm-8">
                        <input class="form-control" type="text"
                               name="txtuname" required
                               placeholder="Username">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-4">
                        <label class="control-label" for="txtemail">
                            eMail Address
                        </label>
                    </div>
                    <div class="col-sm-8">
                        <input class="form-control" type="text"
                               name="txtemail" required
                               placeholder="eMail Address">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-4">
                        <label class="control-label" for="txtpass">
                            Password
                        </label>
                    </div>
                    <div class="col-sm-8">
                        <input class="form-control" type="text"
                               name="txtpass" required
                               placeholder="Password">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-4">
                        <label class="control-label"></label>
                    </div>
                    <div class="col-sm-3">
                        <button class="btn btn-primary"
                                name="btn-registration"
                                type="submit">
                            Sign Up
                        </button>
                    </div>
                    <div class="col-sm-2">
                        <a class="btn btn-default"
                           name="btn-login"
                           href="user-login.php">
                            Sign In
                        </a>
                    </div>
                    <div class="col-sm-3">
                        <a class="btn btn-default"
                           name="btn-forgot"
                           href="user-forgotpass.php">Lost Password?</a>
                    </div>
                </div>

            </form>
        </div>
    </div>
<?php
include_once __DIR__ . "/site-footer.php";
