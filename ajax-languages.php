<?php
/**
 * Short Description (1 Line)
 *
 * Longer description, may be more than one line,
 * usually broken into lines of 80 characters or less.
 *
 * @File:       /ajax-countries.php
 * @Project:    phpToDo
 * @Author:     goulda <goulda@EMAIL.ADDRESS>
 * @Date:       30/11/2016 12:46 PM
 * @Version:    RELEASE.MINOR.BUGFIX (eg 2.5.11)
 * @Copyright:  Copyright 2016 TAFE OpenSource,
 * Released under the Creative Commons Share Alike license
 *
 * History:
 *
 * v 1.0 30/11/2016
 * Initial version
 */


require_once __DIR__ . "/vendor/autoload.php";

use TAFEOpenSource\Database;
use TAFEOpenSource\Languages;
use TAFEOpenSource\Url;
use Carbon\Carbon;

$url = new Url();
$root = $url->getUriNoScript();
$langLang = new Languages();

$currentTime = Carbon::now('Australia/Perth')->format('H:i');
$currentDate = Carbon::now('Australia/Perth')->formatLocalized('%A %d %B %Y');

if (isset($_POST["selCountry"]) && !empty($_POST["selCountry"])) {
    //Get all state data
    $countryCode = $_POST['selCountry'];
    $currentLang = $_POST['hidLanguage'];
    $lingos = $langLang->getLanguages();

    //Display states list
    if ($lingos) {
        echo '<option value="">Select Language</option>';

        foreach ($lingos as $language) {
            $selected = '';
            if ($language->code == $currentLang) {
                $selected = 'selected';
            }
            echo "<option value='{$language->code}' 
            {$selected}>{$language->title}</option>";
        }
    } else {
        echo '<option value="">Country not available</option>';
    }
}

