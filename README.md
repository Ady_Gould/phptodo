# phpToDo

This repository is a 'crude' version of a ToDo application.

Written in a mix of procedural and object oriented code, it has much to be desired, but demonstrates the interaction between database, php, user interface and the user.

## Database Structures

The database structures are defined for Users, UserRoles, UserStatuses, ToDos, ToDoStatuses.

## Resources used

This list should be most, if not all, the respources used in creating this demo application.

- Datetimepicker for Bootstrap 3 (https://github.com/Eonasdan/bootstrap-datetimepicker/)
- Start Bootstrap / SB Admin (http://startbootstrap.com/)
- Bootstrap v3.3.7 (http://getbootstrap.com)
- Font Awesome 4.6.3 by @davegandy (http://fontawesome.io)
- Composer
- Doctrine/Inflector
- Nesbot/Carbon
- PhpMailer/PhpMailer
- Respect/Validation
- MomentJS (http://momentjs.com)
- jQuery (https://jquery.com)

## License

Creative Commons Share Alike, and others as per each subproject.