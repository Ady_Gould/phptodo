<?php
/**
 * Short Description (1 Line)
 *
 * Longer description, may be more than one line,
 * usually broken into lines of 80 characters or less.
 *
 * @File:       /test.php
 * @Project:    phpToDo
 * @Author:     goulda <goulda@EMAIL.ADDRESS>
 * @Date:       09/11/2016 11:10 AM
 * @Version:    RELEASE.MINOR.BUGFIX (eg 2.5.11)
 * @Copyright:  Copyright 2016 TAFE OpenSource,
 * Released under the Creative Commons Share Alike license
 *
 * History:
 *
 * v 1.0 09/11/2016
 * Initial version
 */

// for testing only we require teh Database file
require_once "tafeopensource/src/Database.php";

// Now we tell PHP we want to USE the tafeopensource\Database Class
// using the alias Database
use TAFEOpenSource\Database as DB;

// create new database object
$db=new DB();

echo "<p>Database Object</p>";
echo "<pre>";
var_dump($db); // dump contents of the variable $db to screen
echo "</pre>";

// Create connection
$conn = $db->dbConnection();

echo "<p>conn (connection) variable</p>";
echo "<pre>";
var_dump($conn);
echo "</pre>";

















