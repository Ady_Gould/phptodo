-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 14, 2016 at 05:35 AM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 7.0.4

SET SQL_MODE = "no_auto_value_on_zero";
SET time_zone = "+00:00";


/*!40101 SET @`old_character_set_client` = @@`character_set_client` */;
/*!40101 SET @`old_character_set_results` = @@`character_set_results` */;
/*!40101 SET @`old_collation_connection` = @@`collation_connection` */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ajg_todo`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id`           INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `givenname`    VARCHAR(32)               DEFAULT NULL,
  `lastname`     VARCHAR(32)               DEFAULT NULL,
  `username`     VARCHAR(16)               DEFAULT NULL,
  `email`        VARCHAR(255)              DEFAULT NULL,
  `passwd`       VARCHAR(255)              DEFAULT NULL,
  `userstatus`   CHAR(1)          NOT NULL DEFAULT 'X',
  `userrole`     CHAR(1)          NOT NULL DEFAULT 'X',
  `token`        VARCHAR(255)              DEFAULT NULL,
  `date_login`   DATETIME                  DEFAULT NULL,
  `date_logout`  DATETIME                  DEFAULT NULL,
  `date_added`   DATETIME                  DEFAULT CURRENT_TIMESTAMP,
  `date_updated` DATETIME                  DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 34
  DEFAULT CHARSET = `latin1`;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `givenname`, `lastname`, `username`, `email`, `passwd`, `userstatus`, `userrole`, `token`, `date_login`, `date_logout`, `date_added`, `date_updated`)
VALUES
  (1, 'Ad', 'Ministrator', 'admin', 'ad.ministrator@host.localhost', 'Password', 'A', 'S', NULL, '1970-01-01 00:00:00',
      '1970-01-01 00:00:00', '2016-10-31 05:28:50', '2016-10-31 05:28:50'),
  (2, 'Adrian', 'Gould', 'goulda', 'adrian.gould@host.localhost', 'Password', 'A', 'A', NULL, '1970-01-01 00:00:00',
      '1970-01-01 00:00:00', '2016-10-31 05:28:50', '2016-10-31 05:28:50'),
  (3, 'GIVEN NAME HERE', 'LAST NAME HERE', 'USER.NAME.HERE', 'givenname.lastname@host.localhost', 'Password', 'A', 'A',
      NULL, '1970-01-01 00:00:00', '1970-01-01 00:00:00', '2016-10-31 05:28:50', '2016-10-31 05:28:50'),
  (4, 'Adam', 'Zapple', 'zapplea', 'adam.zapple@host.localhost', 'Password', 'S', 'X', NULL, '1970-01-01 00:00:00',
      '1970-01-01 00:00:00', '2016-10-31 05:28:50', '2016-10-31 05:28:50');
INSERT INTO `users` (`id`, `givenname`, `lastname`, `username`, `email`, `passwd`, `userstatus`, `userrole`, `token`, `date_login`, `date_logout`, `date_added`, `date_updated`)
VALUES
  (5, 'Al', 'Beback', 'bebacka', 'al.beback@host.localhost', 'Password', 'A', 'X', NULL, '1970-01-01 00:00:00',
      '1970-01-01 00:00:00', '2016-10-31 05:28:50', '2016-10-31 05:28:50'),
  (6, 'Bjorn', 'Free', 'freeb', 'bjorn.free@host.localhost', 'Password', 'T', 'A', NULL, '1970-01-01 00:00:00',
      '1970-01-01 00:00:00', '2016-10-31 05:28:50', '2016-10-31 05:28:50'),
  (7, 'Boss', 'Anova', 'anovab', 'boss.anova@host.localhost', 'Password', 'D', 'U', NULL, '1970-01-01 00:00:00',
      '1970-01-01 00:00:00', '2016-10-31 05:28:50', '2016-10-31 05:28:50'),
  (8, 'Cole', 'Slaw', 'slawc', 'cole.slaw@host.localhost', 'Password', 'T', 'U', NULL, '1970-01-01 00:00:00',
      '1970-01-01 00:00:00', '2016-10-31 05:28:50', '2016-10-31 05:28:50');
INSERT INTO `users` (`id`, `givenname`, `lastname`, `username`, `email`, `passwd`, `userstatus`, `userrole`, `token`, `date_login`, `date_logout`, `date_added`, `date_updated`)
VALUES
  (9, 'Ed', 'Venture', 'venturee', 'ed.venture@host.localhost', 'Password', 'W', 'X', NULL, '1970-01-01 00:00:00',
      '1970-01-01 00:00:00', '2016-10-31 05:28:50', '2016-10-31 05:28:50'),
  (10, 'Ella', 'Vader', 'vadere', 'ella.vader@host.localhost', 'Password', 'A', 'U', NULL, '1970-01-01 00:00:00',
       '1970-01-01 00:00:00', '2016-10-31 05:28:50', '2016-10-31 05:28:50'),
  (11, 'Ella', 'Fant', 'fante', 'ella.fant@host.localhost', 'Password', 'W', 'U', NULL, '1970-01-01 00:00:00',
       '1970-01-01 00:00:00', '2016-10-31 05:28:50', '2016-10-31 05:28:50'),
  (12, 'Faith', 'Less', 'lessf', 'faith.less@host.localhost', 'Password', 'D', 'A', NULL, '1970-01-01 00:00:00',
       '1970-01-01 00:00:00', '2016-10-31 05:28:50', '2016-10-31 05:28:50');
INSERT INTO `users` (`id`, `givenname`, `lastname`, `username`, `email`, `passwd`, `userstatus`, `userrole`, `token`, `date_login`, `date_logout`, `date_added`, `date_updated`)
VALUES
  (13, 'Hellen', 'Bach', 'bachh', 'hellen.bach@host.localhost', 'Password', 'D', 'U', NULL, '1970-01-01 00:00:00',
       '1970-01-01 00:00:00', '2016-10-31 05:28:50', '2016-10-31 05:28:50'),
  (14, 'Kitty', 'Katt', 'kattk', 'kitty.katt@host.localhost', 'Password', 'W', 'U', NULL, '1970-01-01 00:00:00',
       '1970-01-01 00:00:00', '2016-10-31 05:28:50', '2016-10-31 05:28:50'),
  (15, 'Stan', 'Back', 'backs', 'stan.back@host.localhost', 'Password', 'D', 'U', NULL, '1970-01-01 00:00:00',
       '1970-01-01 00:00:00', '2016-10-31 05:28:50', '2016-10-31 05:28:50'),
  (16, 'Sonya', 'Mind', 'minds', 'sonya.mind@host.localhost', 'Password', 'X', 'U', NULL, '1970-01-01 00:00:00',
       '1970-01-01 00:00:00', '2016-10-31 05:28:50', '2016-10-31 05:28:50');
INSERT INTO `users` (`id`, `givenname`, `lastname`, `username`, `email`, `passwd`, `userstatus`, `userrole`, `token`, `date_login`, `date_logout`, `date_added`, `date_updated`)
VALUES
  (17, 'Eileen', 'Dover', 'dovere', 'eileen.dover@host.localhost', 'Password', 'T', 'X', NULL, '1970-01-01 00:00:00',
       '1970-01-01 00:00:00', '2016-10-31 05:28:50', '2016-10-31 05:28:50'),
  (18, 'Russel', 'Leaves', 'leavesr', 'russel.leaves@host.localhost', 'Password', 'S', 'U', NULL, '1970-01-01 00:00:00',
       '1970-01-01 00:00:00', '2016-10-31 05:28:50', '2016-10-31 05:28:50'),
  (19, 'Summer', 'Day', 'days', 'summer.day@host.localhost', 'Password', 'S', 'U', NULL, '1970-01-01 00:00:00',
       '1970-01-01 00:00:00', '2016-10-31 05:28:50', '2016-10-31 05:28:50'),
  (20, 'Coral', 'Reef', 'reefc', 'coral.reef@host.localhost', 'Password', 'A', 'A', NULL, '1970-01-01 00:00:00',
       '1970-01-01 00:00:00', '2016-10-31 05:28:50', '2016-10-31 05:28:50');
INSERT INTO `users` (`id`, `givenname`, `lastname`, `username`, `email`, `passwd`, `userstatus`, `userrole`, `token`, `date_login`, `date_logout`, `date_added`, `date_updated`)
VALUES
  (21, 'August', 'Awind', 'awinda', 'august.awind@host.localhost', 'Password', 'A', 'U', NULL, '1970-01-01 00:00:00',
       '1970-01-01 00:00:00', '2016-10-31 05:28:50', '2016-10-31 05:28:50'),
  (22, 'Ann', 'Tartica', 'tarticaa', 'ann.tartica@host.localhost', 'Password', 'S', 'U', NULL, '1970-01-01 00:00:00',
       '1970-01-01 00:00:00', '2016-10-31 05:28:50', '2016-10-31 05:28:50'),
  (23, 'Avery', 'Goodbook', 'goodbooka', 'avery.goodbook@host.localhost', 'Password', 'T', 'A', NULL,
       '1970-01-01 00:00:00', '1970-01-01 00:00:00', '2016-10-31 05:28:50', '2016-10-31 05:28:50'),
  (24, 'Barry D.', 'Hatchet', 'hatchetb', 'barry.d.hatchet@host.localhost', 'Password', 'T', 'U', NULL,
       '1970-01-01 00:00:00', '1970-01-01 00:00:00', '2016-10-31 05:28:50', '2016-10-31 05:28:50');
INSERT INTO `users` (`id`, `givenname`, `lastname`, `username`, `email`, `passwd`, `userstatus`, `userrole`, `token`, `date_login`, `date_logout`, `date_added`, `date_updated`)
VALUES
  (25, 'Mo', 'Torr', 'torrm', 'mo.torr@host.localhost', 'Password', 'D', 'U', NULL, '1970-01-01 00:00:00',
       '1970-01-01 00:00:00', '2016-10-31 05:28:50', '2016-10-31 05:28:50'),
  (26, 'Constance', 'Paine', 'painec', 'constance.paine@host.localhost', 'Password', 'T', 'A', NULL,
       '1970-01-01 00:00:00', '1970-01-01 00:00:00', '2016-10-31 05:28:50', '2016-10-31 05:28:50'),
  (27, 'Doris', 'Schutt', 'schuttd', 'doris.schutt@host.localhost', 'Password', 'A', 'U', NULL, '1970-01-01 00:00:00',
       '1970-01-01 00:00:00', '2016-10-31 05:28:50', '2016-10-31 05:28:50'),
  (28, 'Frank Lee', 'Speaking', 'speakingf', 'frank.lee.speaking@host.localhost', 'Password', 'T', 'A', NULL,
       '1970-01-01 00:00:00', '1970-01-01 00:00:00', '2016-10-31 05:28:50', '2016-10-31 05:28:50');
INSERT INTO `users` (`id`, `givenname`, `lastname`, `username`, `email`, `passwd`, `userstatus`, `userrole`, `token`, `date_login`, `date_logout`, `date_added`, `date_updated`)
VALUES
  (29, 'Chase', 'de Katt', 'de.kattc', 'chase.de.katt@host.localhost', 'Password', 'W', 'U', NULL,
       '1970-01-01 00:00:00', '1970-01-01 00:00:00', '2016-10-31 05:28:50', '2016-10-31 05:28:50'),
  (33, 'Fred', 'Bloggs', 'bloggsf', 'fred.blogs@somewhere.local', 'Password', 'W', 'X',
       'f021eeb5ff955473d2f0b6e293e656b1', NULL, NULL, '2016-11-14 09:37:43', '2016-11-14 09:37:43');

/*!40101 SET CHARACTER_SET_CLIENT = @`old_character_set_client` */;
/*!40101 SET CHARACTER_SET_RESULTS = @`old_character_set_results` */;
/*!40101 SET COLLATION_CONNECTION = @`old_collation_connection` */;

-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 14, 2016 at 05:35 AM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 7.0.4

SET SQL_MODE = "no_auto_value_on_zero";
SET time_zone = "+00:00";


/*!40101 SET @`old_character_set_client` = @@`character_set_client` */;
/*!40101 SET @`old_character_set_results` = @@`character_set_results` */;
/*!40101 SET @`old_collation_connection` = @@`collation_connection` */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ajg_todo`
--

-- --------------------------------------------------------

--
-- Table structure for table `todos`
--

DROP TABLE IF EXISTS `todos`;
CREATE TABLE IF NOT EXISTS `todos` (
  `id`           INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id`      INT(11) UNSIGNED NOT NULL DEFAULT '0'
  COMMENT '  ',
  `title`        VARCHAR(64)               DEFAULT '--UNKNOWN--',
  `description`  TEXT,
  `date_due`     DATETIME                  DEFAULT '1970-01-01 00:00:00',
  `priority`     INT(1)           NOT NULL DEFAULT '0',
  `date_added`   DATETIME                  DEFAULT '1970-01-01 00:00:00',
  `date_updated` DATETIME                  DEFAULT '1970-01-01 00:00:00',
  PRIMARY KEY (`id`),
  KEY `user_id_index` (`user_id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 346
  DEFAULT CHARSET = `latin1`;

--
-- Dumping data for table `todos`
--

INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (1, 23, 'Try to climb through all the rooms in your home withou …',
   'Try to climb through all the rooms in your home without touching the floor', '2016-11-13 00:00:00', 5,
   '2016-11-13 17:31:46', '2016-11-13 17:31:46'),
  (2, 12, 'Try to climb through all the rooms in your home withou …',
   'Try to climb through all the rooms in your home without touching the floor', '2016-11-13 17:37:24', 1,
   '2016-11-13 17:37:24', '2016-11-13 17:37:24'),
  (3, 29, 'Try to climb through all the rooms in your home withou …',
   'Try to climb through all the rooms in your home without touching the floor', '2016-11-13 17:38:24', 4,
   '2016-11-13 17:38:24', '2016-11-13 17:38:24');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (4, 25, 'Start a free blog on WordPress or Blogger and tell the …',
   'Start a free blog on WordPress or Blogger and tell the world about the silly things you have done',
   '2016-11-13 17:38:24', 3, '2016-11-13 17:38:24', '2016-11-13 17:38:24'),
  (5, 18, 'Read a book. Use your time to learn something new …', 'Read a book. Use your time to learn something new',
   '2016-11-13 17:38:24', 3, '2016-11-13 17:38:24', '2016-11-13 17:38:24'),
  (6, 2, 'Create an upside-down room. Choose a room and take a f …',
   'Create an upside-down room. Choose a room and take a few photos of it. Then, invert the entire room by sticking all of the things on the floor up on the ceiling and vice versa. Don’t forget to invert any framed pictures. You will need a drill, nails and lots of glue',
   '2016-11-13 17:38:24', 1, '2016-11-13 17:38:24', '2016-11-13 17:38:24');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (7, 23, 'Learn how to moonwalk …', 'Learn how to moonwalk', '2016-11-13 17:38:24', 5, '2016-11-13 17:38:24',
   '2016-11-13 17:38:24'),
  (8, 27, 'Change your facebook relationship status to the opposi …',
   'Change your facebook relationship status to the opposite of what it normally is. Wait for “friends” to question your status',
   '2016-11-13 17:38:24', 1, '2016-11-13 17:38:24', '2016-11-13 17:38:24'),
  (9, 11, 'Dress up like a burglar and try to break in to your ow …',
   'Dress up like a burglar and try to break in to your own home', '2016-11-13 17:38:24', 1, '2016-11-13 17:38:24',
   '2016-11-13 17:38:24'),
  (10, 10, 'Write a series poem of no more than four lines …', 'Write a series poem of no more than four lines',
   '2016-11-13 17:38:24', 5, '2016-11-13 17:38:24', '2016-11-13 17:38:24');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (11, 11, 'Try to move an object using the power of your mind …', 'Try to move an object using the power of your mind',
   '2016-11-13 17:38:24', 5, '2016-11-13 17:38:24', '2016-11-13 17:38:24'),
  (12, 16, 'Become invisible …', 'Become invisible', '2016-11-13 17:38:24', 5, '2016-11-13 17:38:24',
   '2016-11-13 17:38:24'),
  (13, 13, 'Shave your head and reinvent yourself …', 'Shave your head and reinvent yourself', '2016-11-13 17:38:24', 3,
   '2016-11-13 17:38:24', '2016-11-13 17:38:24'),
  (14, 5, 'Fail a series of intelligence tests …', 'Fail a series of intelligence tests', '2016-11-13 17:38:24', 3,
   '2016-11-13 17:38:24', '2016-11-13 17:38:24'),
  (15, 17, 'Explore the galaxy …', 'Explore the galaxy', '2016-11-13 17:38:24', 2, '2016-11-13 17:38:24',
   '2016-11-13 17:38:24');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (16, 19, 'Try to perfect the art of counting seconds accurately …',
   'Try to perfect the art of counting seconds accurately', '2016-11-13 17:38:24', 5, '2016-11-13 17:38:24',
   '2016-11-13 17:38:24'),
  (17, 8, 'Make an army out of gummy bears. …', 'Make an army out of gummy bears.', '2016-11-13 17:38:24', 4,
   '2016-11-13 17:38:24', '2016-11-13 17:38:24'),
  (18, 4, 'Prove a teacher wrong. …', 'Prove a teacher wrong.', '2016-11-13 17:38:24', 4, '2016-11-13 17:38:24',
   '2016-11-13 17:38:24'),
  (19, 6, 'Cut a bar of soap in half and put it in the microwave  …',
   'Cut a bar of soap in half and put it in the microwave for a minute. Watch it bubble and grow! Aha! ',
   '2016-11-13 17:38:24', 3, '2016-11-13 17:38:24', '2016-11-13 17:38:24');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (20, 6, 'Make DIY Clouds Night Light - http://sheharzad.deviant …',
   'Make DIY Clouds Night Light - http://sheharzad.deviantart.com/art/Clouds-night-light-284569247',
   '2016-11-13 17:38:24', 5, '2016-11-13 17:38:24', '2016-11-13 17:38:24'),
  (21, 15, 'Make DIY Planter & Candle Holder - http://wordofdecor. …',
   'Make DIY Planter & Candle Holder - http://wordofdecor.com/svoimi-rukami/cvetochnye-gorshki-svoimi-rukami/',
   '2016-11-13 17:38:24', 4, '2016-11-13 17:38:24', '2016-11-13 17:38:24'),
  (22, 4, 'Try to climb through all the rooms in your home withou …',
   'Try to climb through all the rooms in your home without touching the floor', '2016-11-13 17:57:40', 2,
   '2016-11-13 17:57:40', '2016-11-13 17:57:40');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (23, 17, 'Start a free blog on WordPress or Blogger and tell the …',
   'Start a free blog on WordPress or Blogger and tell the world about the silly things you have done',
   '2016-11-13 17:57:40', 5, '2016-11-13 17:57:40', '2016-11-13 17:57:40'),
  (24, 6, 'Read a book. Use your time to learn something new …', 'Read a book. Use your time to learn something new',
   '2016-11-13 17:57:40', 3, '2016-11-13 17:57:40', '2016-11-13 17:57:40'),
  (25, 6, 'Create an upside-down room. Choose a room and take a f …',
   'Create an upside-down room. Choose a room and take a few photos of it. Then, invert the entire room by sticking all of the things on the floor up on the ceiling and vice versa. Don’t forget to invert any framed pictures. You will need a drill, nails and lots of glue',
   '2016-11-13 17:57:40', 3, '2016-11-13 17:57:40', '2016-11-13 17:57:40');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (26, 20, 'Learn how to moonwalk …', 'Learn how to moonwalk', '2016-11-13 17:57:40', 4, '2016-11-13 17:57:40',
   '2016-11-13 17:57:40'),
  (27, 3, 'Change your facebook relationship status to the opposi …',
   'Change your facebook relationship status to the opposite of what it normally is. Wait for “friends” to question your status',
   '2016-11-13 17:57:40', 3, '2016-11-13 17:57:40', '2016-11-13 17:57:40'),
  (28, 4, 'Dress up like a burglar and try to break in to your ow …',
   'Dress up like a burglar and try to break in to your own home', '2016-11-13 17:57:40', 2, '2016-11-13 17:57:40',
   '2016-11-13 17:57:40'),
  (29, 10, 'Write a series poem of no more than four lines …', 'Write a series poem of no more than four lines',
   '2016-11-13 17:57:40', 3, '2016-11-13 17:57:40', '2016-11-13 17:57:40');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (30, 17, 'Try to move an object using the power of your mind …', 'Try to move an object using the power of your mind',
   '2016-11-13 17:57:40', 4, '2016-11-13 17:57:40', '2016-11-13 17:57:40'),
  (31, 2, 'Become invisible …', 'Become invisible', '2016-11-13 17:57:40', 3, '2016-11-13 17:57:40',
   '2016-11-13 17:57:40'),
  (32, 6, 'Shave your head and reinvent yourself …', 'Shave your head and reinvent yourself', '2016-11-13 17:57:40', 1,
   '2016-11-13 17:57:40', '2016-11-13 17:57:40'),
  (33, 9, 'Fail a series of intelligence tests …', 'Fail a series of intelligence tests', '2016-11-13 17:57:40', 3,
   '2016-11-13 17:57:40', '2016-11-13 17:57:40'),
  (34, 27, 'Explore the galaxy …', 'Explore the galaxy', '2016-11-13 17:57:40', 4, '2016-11-13 17:57:40',
   '2016-11-13 17:57:40');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (35, 27, 'Try to perfect the art of counting seconds accurately …',
   'Try to perfect the art of counting seconds accurately', '2016-11-13 17:57:40', 4, '2016-11-13 17:57:40',
   '2016-11-13 17:57:40'),
  (36, 10, 'Make an army out of gummy bears. …', 'Make an army out of gummy bears.', '2016-11-13 17:57:40', 2,
   '2016-11-13 17:57:40', '2016-11-13 17:57:40'),
  (37, 13, 'Prove a teacher wrong. …', 'Prove a teacher wrong.', '2016-11-13 17:57:40', 4, '2016-11-13 17:57:40',
   '2016-11-13 17:57:40'),
  (38, 20, 'Cut a bar of soap in half and put it in the microwave  …',
   'Cut a bar of soap in half and put it in the microwave for a minute. Watch it bubble and grow! Aha! ',
   '2016-11-13 17:57:40', 3, '2016-11-13 17:57:40', '2016-11-13 17:57:40');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (39, 8, 'Make DIY Clouds Night Light - http:////sheharzad.devia …',
   'Make DIY Clouds Night Light - http:////sheharzad.deviantart.com//art//Clouds-night-light-284569247',
   '2016-11-13 17:57:40', 4, '2016-11-13 17:57:40', '2016-11-13 17:57:40'),
  (40, 3, 'Make DIY Planter & Candle Holder - http:////wordofdeco …',
   'Make DIY Planter & Candle Holder - http:////wordofdecor.com//svoimi-rukami//cvetochnye-gorshki-svoimi-rukami//',
   '2016-11-13 17:57:40', 1, '2016-11-13 17:57:40', '2016-11-13 17:57:40'),
  (41, 23, 'Try to climb through all the rooms in your home withou …',
   'Try to climb through all the rooms in your home without touching the floor', '2016-11-13 17:59:15', 3,
   '2016-11-13 17:59:15', '2016-11-13 17:59:15');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (42, 24, 'Start a free blog on WordPress or Blogger and tell the …',
   'Start a free blog on WordPress or Blogger and tell the world about the silly things you have done',
   '2016-11-13 17:59:15', 5, '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (43, 6, 'Read a book. Use your time to learn something new …', 'Read a book. Use your time to learn something new',
   '2016-11-13 17:59:15', 3, '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (44, 1, 'Create an upside-down room. Choose a room and take a f …',
   'Create an upside-down room. Choose a room and take a few photos of it. Then, invert the entire room by sticking all of the things on the floor up on the ceiling and vice versa. Don’t forget to invert any framed pictures. You will need a drill, nails and lots of glue',
   '2016-11-13 17:59:15', 4, '2016-11-13 17:59:15', '2016-11-13 17:59:15');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (45, 14, 'Learn how to moonwalk …', 'Learn how to moonwalk', '2016-11-13 17:59:15', 1, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (46, 22, 'Change your facebook relationship status to the opposi …',
   'Change your facebook relationship status to the opposite of what it normally is. Wait for “friends” to question your status',
   '2016-11-13 17:59:15', 4, '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (47, 15, 'Dress up like a burglar and try to break in to your ow …',
   'Dress up like a burglar and try to break in to your own home', '2016-11-13 17:59:15', 4, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (48, 27, 'Write a series poem of no more than four lines …', 'Write a series poem of no more than four lines',
   '2016-11-13 17:59:15', 4, '2016-11-13 17:59:15', '2016-11-13 17:59:15');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (49, 27, 'Try to move an object using the power of your mind …', 'Try to move an object using the power of your mind',
   '2016-11-13 17:59:15', 4, '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (50, 25, 'Become invisible …', 'Become invisible', '2016-11-13 17:59:15', 5, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (51, 5, 'Shave your head and reinvent yourself …', 'Shave your head and reinvent yourself', '2016-11-13 17:59:15', 2,
   '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (52, 19, 'Fail a series of intelligence tests …', 'Fail a series of intelligence tests', '2016-11-13 17:59:15', 4,
   '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (53, 10, 'Explore the galaxy …', 'Explore the galaxy', '2016-11-13 17:59:15', 5, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (54, 23, 'Try to perfect the art of counting seconds accurately …',
   'Try to perfect the art of counting seconds accurately', '2016-11-13 17:59:15', 4, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (55, 5, 'Make an army out of gummy bears. …', 'Make an army out of gummy bears.', '2016-11-13 17:59:15', 3,
   '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (56, 11, 'Prove a teacher wrong. …', 'Prove a teacher wrong.', '2016-11-13 17:59:15', 5, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (57, 10, 'Cut a bar of soap in half and put it in the microwave  …',
   'Cut a bar of soap in half and put it in the microwave for a minute. Watch it bubble and grow! Aha! ',
   '2016-11-13 17:59:15', 3, '2016-11-13 17:59:15', '2016-11-13 17:59:15');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (58, 16, 'Make DIY Clouds Night Light - http:////sheharzad.devia …',
   'Make DIY Clouds Night Light - http:////sheharzad.deviantart.com//art//Clouds-night-light-284569247',
   '2016-11-13 17:59:15', 3, '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (59, 10, 'Make DIY Planter & Candle Holder - http:////wordofdeco …',
   'Make DIY Planter & Candle Holder - http:////wordofdecor.com//svoimi-rukami//cvetochnye-gorshki-svoimi-rukami//',
   '2016-11-13 17:59:15', 4, '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (60, 10, 'Make a Lightbulb Bud Vase - http:////www.readymade.com …',
   'Make a Lightbulb Bud Vase - http:////www.readymade.com//projects//lightbulb_bud_vase', '2016-11-13 17:59:15', 1,
   '2016-11-13 17:59:15', '2016-11-13 17:59:15');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (61, 9, 'Make a DIY TetraBox Lamp - http:////www.demilked.com// …',
   'Make a DIY TetraBox Lamp - http:////www.demilked.com//diy-tetrabox-lamp-ed-chew//', '2016-11-13 17:59:15', 4,
   '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (62, 6, 'make some Toilet Paper Roll Wall Art - http:////www.cu …',
   'make some Toilet Paper Roll Wall Art - http:////www.cutoutandkeep.net//projects//toilet-paper-roll-wall-art',
   '2016-11-13 17:59:15', 3, '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (63, 9, 'make cup cakes and put on Dried Pineapple Flowers - ht …',
   'make cup cakes and put on Dried Pineapple Flowers - http:////annies-eats.com//2011//02//28//how-to-make-dried-pineapple-flowers//',
   '2016-11-13 17:59:15', 4, '2016-11-13 17:59:15', '2016-11-13 17:59:15');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (64, 9, 'make a DIY Spoon Lamp - http:////www.futurenow.su//en/ …',
   'make a DIY Spoon Lamp - http:////www.futurenow.su//en//view_article//152//', '2016-11-13 17:59:15', 3,
   '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (65, 13, 'Make Beverage Can Tab Bag - http:////www.instructables …',
   'Make Beverage Can Tab Bag - http:////www.instructables.com//id//Genuine-chainmaille-from-pop-tabs//?ALLSTEPS#step1',
   '2016-11-13 17:59:15', 3, '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (66, 19, 'Test out display mattresses …', 'Test out display mattresses', '2016-11-13 17:59:15', 1,
   '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (67, 23, 'Decorate a bus stop shelter …', 'Decorate a bus stop shelter', '2016-11-13 17:59:15', 3,
   '2016-11-13 17:59:15', '2016-11-13 17:59:15');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (68, 27, 'Alphabetize your book, movie, or record collection …', 'Alphabetize your book, movie, or record collection',
   '2016-11-13 17:59:15', 1, '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (69, 27, 'Buy yourself some new office supplies …', 'Buy yourself some new office supplies', '2016-11-13 17:59:15', 1,
   '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (70, 1, 'Build a time capsule to open in five years …', 'Build a time capsule to open in five years',
   '2016-11-13 17:59:15', 1, '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (71, 3, 'Call a friend and talk for no less than one hour …', 'Call a friend and talk for no less than one hour',
   '2016-11-13 17:59:15', 5, '2016-11-13 17:59:15', '2016-11-13 17:59:15');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (72, 28, 'Cut a pair of jeans into shorts and decorate them …', 'Cut a pair of jeans into shorts and decorate them',
   '2016-11-13 17:59:15', 2, '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (73, 25, 'Make a fortune teller out of paper …', 'Make a fortune teller out of paper', '2016-11-13 17:59:15', 2,
   '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (74, 5, 'Write a list of things that you’ve done in the past ye …',
   'Write a list of things that you’ve done in the past year', '2016-11-13 17:59:15', 4, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (75, 15, 'Plan a road trip and map out places to stop along the  …',
   'Plan a road trip and map out places to stop along the way', '2016-11-13 17:59:15', 5, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (76, 5, 'Stretch your muscles for thirty minutes …', 'Stretch your muscles for thirty minutes', '2016-11-13 17:59:15',
   3, '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (77, 10, 'Hold a meeting at an office supply store …', 'Hold a meeting at an office supply store',
   '2016-11-13 17:59:15', 1, '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (78, 19, 'Drive around until you have seen fifteen dogs on walks …',
   'Drive around until you have seen fifteen dogs on walks', '2016-11-13 17:59:15', 5, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (79, 25, 'Read a story book out loud to your pet …', 'Read a story book out loud to your pet', '2016-11-13 17:59:15',
   2, '2016-11-13 17:59:15', '2016-11-13 17:59:15');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (80, 10, 'Hold a bubble gun while you bike around a neighborhood …',
   'Hold a bubble gun while you bike around a neighborhood', '2016-11-13 17:59:15', 1, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (81, 25, 'Run through a sprinkler …', 'Run through a sprinkler', '2016-11-13 17:59:15', 5, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (82, 3, 'Make a list of everyone you’ve ever kissed …', 'Make a list of everyone you’ve ever kissed',
   '2016-11-13 17:59:15', 2, '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (83, 15, 'Figure out how many goats you’d be worth …', 'Figure out how many goats you’d be worth',
   '2016-11-13 17:59:15', 2, '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (84, 6, 'Take a nap facing the “wrong” way on your bed …', 'Take a nap facing the “wrong” way on your bed',
   '2016-11-13 17:59:15', 1, '2016-11-13 17:59:15', '2016-11-13 17:59:15');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (85, 28, 'Videotape an epic montage …', 'Videotape an epic montage', '2016-11-13 17:59:15', 3, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (86, 5, 'Live tweet a very old movie as you watch it on DVD …', 'Live tweet a very old movie as you watch it on DVD',
   '2016-11-13 17:59:15', 1, '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (87, 25, 'Write three minutes worth of stand-up comedy material …',
   'Write three minutes worth of stand-up comedy material', '2016-11-13 17:59:15', 3, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (88, 16, 'Count how many rings a tree stump has …', 'Count how many rings a tree stump has', '2016-11-13 17:59:15', 2,
   '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (89, 20, 'Buy a homeless person a meal …', 'Buy a homeless person a meal', '2016-11-13 17:59:15', 2,
   '2016-11-13 17:59:15', '2016-11-13 17:59:15');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (90, 29, 'Memorize all the cheers from the first Bring It On mov …',
   'Memorize all the cheers from the first Bring It On movie', '2016-11-13 17:59:15', 3, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (91, 25, 'Read all the Terms Of Service …', 'Read all the Terms Of Service', '2016-11-13 17:59:15', 1,
   '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (92, 23, 'Schedule a cake testing, even if you’re not getting ma …',
   'Schedule a cake testing, even if you’re not getting married', '2016-11-13 17:59:15', 1, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (93, 23, 'Learn how to juggle …', 'Learn how to juggle', '2016-11-13 17:59:15', 1, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (94, 4, 'Watch a movie with subtitles …', 'Watch a movie with subtitles', '2016-11-13 17:59:15', 1,
   '2016-11-13 17:59:15', '2016-11-13 17:59:15');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (95, 21, 'Make up a secret handshake …', 'Make up a secret handshake', '2016-11-13 17:59:15', 5,
   '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (96, 27, 'Recreate a famous movie scene …', 'Recreate a famous movie scene', '2016-11-13 17:59:15', 2,
   '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (97, 14, 'Read twenty entries on Urban Dictionary …', 'Read twenty entries on Urban Dictionary',
   '2016-11-13 17:59:15', 3, '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (98, 16, 'Write your own epitaph …', 'Write your own epitaph', '2016-11-13 17:59:15', 2, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (99, 5, 'Sign up for an improv comedy class …', 'Sign up for an improv comedy class', '2016-11-13 17:59:15', 4,
   '2016-11-13 17:59:15', '2016-11-13 17:59:15');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (100, 1, 'Play sleepover games like Light As A Feather or Truth  …',
   'Play sleepover games like Light As A Feather or Truth Or Dare', '2016-11-13 17:59:15', 1, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (101, 15, 'Put a crazy color highlight in your hair …', 'Put a crazy color highlight in your hair',
   '2016-11-13 17:59:15', 1, '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (102, 4, 'Eat a jar of baby food …', 'Eat a jar of baby food', '2016-11-13 17:59:15', 1, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (103, 2, 'Organize your fridge or pantry by color …', 'Organize your fridge or pantry by color',
   '2016-11-13 17:59:15', 5, '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (104, 13, 'Take an extremely strange selfie …', 'Take an extremely strange selfie', '2016-11-13 17:59:15', 2,
   '2016-11-13 17:59:15', '2016-11-13 17:59:15');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (105, 27, 'Make your own sushi …', 'Make your own sushi', '2016-11-13 17:59:15', 1, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (106, 10, 'Dance with strangers in public places …', 'Dance with strangers in public places', '2016-11-13 17:59:15',
   5, '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (107, 24, 'Make jewelry out of office supplies …', 'Make jewelry out of office supplies', '2016-11-13 17:59:15', 4,
   '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (108, 22, 'Donate all of your old electronics and cartridges …', 'Donate all of your old electronics and cartridges',
   '2016-11-13 17:59:15', 3, '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (109, 21, 'Research the history of another country in depth …', 'Research the history of another country in depth',
   '2016-11-13 17:59:15', 1, '2016-11-13 17:59:15', '2016-11-13 17:59:15');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (110, 12, 'Jump in a fountain …', 'Jump in a fountain', '2016-11-13 17:59:15', 4, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (111, 11, 'Build a terrarium …', 'Build a terrarium', '2016-11-13 17:59:15', 2, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (112, 24, 'Get a pedicure …', 'Get a pedicure', '2016-11-13 17:59:15', 2, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (113, 3, 'Get a caricature done of yourself …', 'Get a caricature done of yourself', '2016-11-13 17:59:15', 4,
   '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (114, 23, 'Dance on top of bubble wrap …', 'Dance on top of bubble wrap', '2016-11-13 17:59:15', 4,
   '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (115, 7, 'Make a zine …', 'Make a zine', '2016-11-13 17:59:15', 2, '2016-11-13 17:59:15', '2016-11-13 17:59:15');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (116, 13, 'Fill up a disposable camera …', 'Fill up a disposable camera', '2016-11-13 17:59:15', 2,
   '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (117, 16, 'Come up with your life story in six words …', 'Come up with your life story in six words',
   '2016-11-13 17:59:15', 5, '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (118, 7, 'Visit a garden …', 'Visit a garden', '2016-11-13 17:59:15', 2, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (119, 14, 'Make a blackout poem …', 'Make a blackout poem', '2016-11-13 17:59:15', 1, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (120, 27, 'Go to a museum, but just for the gift shop …', 'Go to a museum, but just for the gift shop',
   '2016-11-13 17:59:15', 2, '2016-11-13 17:59:15', '2016-11-13 17:59:15');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (121, 3, 'Find out how many grapes you can hold in your mouth at …',
   'Find out how many grapes you can hold in your mouth at once', '2016-11-13 17:59:15', 4, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (122, 20, 'Write fan fiction …', 'Write fan fiction', '2016-11-13 17:59:15', 2, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (123, 17, 'Release a balloon into the sky …', 'Release a balloon into the sky', '2016-11-13 17:59:15', 1,
   '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (124, 3, 'Make a playlist of songs you only listened to in high  …',
   'Make a playlist of songs you only listened to in high school', '2016-11-13 17:59:15', 2, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (125, 8, 'Wear a name tag that says something other than your na …',
   'Wear a name tag that says something other than your name', '2016-11-13 17:59:15', 1, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (126, 29, 'Send an envelope full of confetti to your best friend …',
   'Send an envelope full of confetti to your best friend', '2016-11-13 17:59:15', 5, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (127, 8, 'Plan an elaborate scavenger hunt based on correctly an …',
   'Plan an elaborate scavenger hunt based on correctly answering riddles', '2016-11-13 17:59:15', 5,
   '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (128, 26, 'Clean the thing in your house that you really hate cle …',
   'Clean the thing in your house that you really hate cleaning', '2016-11-13 17:59:15', 5, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (129, 2, 'Have a photoshoot where you finally make overalls sexy …',
   'Have a photoshoot where you finally make overalls sexy', '2016-11-13 17:59:15', 1, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (130, 4, 'Make a friendship award for someone you love …', 'Make a friendship award for someone you love',
   '2016-11-13 17:59:15', 1, '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (131, 24, 'Get a slinky to go all the way down the stairs without …',
   'Get a slinky to go all the way down the stairs without stopping', '2016-11-13 17:59:15', 3, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (132, 11, 'Wear camouflage from head to toe and ask people if the …',
   'Wear camouflage from head to toe and ask people if they can see you', '2016-11-13 17:59:15', 4,
   '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (133, 14, 'Water plants …', 'Water plants', '2016-11-13 17:59:15', 1, '2016-11-13 17:59:15', '2016-11-13 17:59:15');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (134, 26, 'Sing your heart out in the shower to “Total Eclipse Of …',
   'Sing your heart out in the shower to “Total Eclipse Of The Heart”', '2016-11-13 17:59:15', 3, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (135, 25, 'Figure out how to fold a fitted bed sheet …', 'Figure out how to fold a fitted bed sheet',
   '2016-11-13 17:59:15', 5, '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (136, 26, 'Hang out at IKEA …', 'Hang out at IKEA', '2016-11-13 17:59:15', 5, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (137, 15, 'Have a costume night …', 'Have a costume night', '2016-11-13 17:59:15', 3, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (138, 18, 'Throw an indoor picnic …', 'Throw an indoor picnic', '2016-11-13 17:59:15', 5, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (139, 1, 'Host an International Dinner Night …', 'Host an International Dinner Night', '2016-11-13 17:59:15', 3,
   '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (140, 7, 'Film a ''newscast'' …', 'Film a ''newscast''', '2016-11-13 17:59:15', 2, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (141, 2, 'Have an indoor scavenger hunt …', 'Have an indoor scavenger hunt', '2016-11-13 17:59:15', 3,
   '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (142, 6, 'Have an outdoor scavenger hunt …', 'Have an outdoor scavenger hunt', '2016-11-13 17:59:15', 1,
   '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (143, 29, 'Plant seedlings …', 'Plant seedlings', '2016-11-13 17:59:15', 5, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (144, 11, 'Grow an avacado plant from its stone …', 'Grow an avacado plant from its stone', '2016-11-13 17:59:15', 1,
   '2016-11-13 17:59:15', '2016-11-13 17:59:15');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (145, 15, 'Play Munchkin …', 'Play Munchkin', '2016-11-13 17:59:15', 5, '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (146, 7, 'Play Killer Bunnies …', 'Play Killer Bunnies', '2016-11-13 17:59:15', 1, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (147, 2, 'Play Castle Panic …', 'Play Castle Panic', '2016-11-13 17:59:15', 2, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (148, 16, 'Play with Lego …', 'Play with Lego', '2016-11-13 17:59:15', 5, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (149, 23, 'Play Cataan …', 'Play Cataan', '2016-11-13 17:59:15', 2, '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (150, 4, 'Play Powergrid …', 'Play Powergrid', '2016-11-13 17:59:15', 1, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (151, 12, 'Make a ''Sharpie'' mug …', 'Make a ''Sharpie'' mug', '2016-11-13 17:59:15', 4, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (152, 5, 'Use bookends as shelving …', 'Use bookends as shelving', '2016-11-13 17:59:15', 2, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (153, 8, 'Make wood block photo transfers …', 'Make wood block photo transfers', '2016-11-13 17:59:15', 4,
   '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (154, 13, 'Nail polish your key ends …', 'Nail polish your key ends', '2016-11-13 17:59:15', 2, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (155, 10, 'Make shelving from Step ladders and Wood Planks …', 'Make shelving from Step ladders and Wood Planks',
   '2016-11-13 17:59:15', 5, '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (156, 29, 'Make PVC Pipe Shoe Rack …', 'Make PVC Pipe Shoe Rack', '2016-11-13 17:59:15', 2, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (157, 22, 'Make chalk board from old photo frames …', 'Make chalk board from old photo frames', '2016-11-13 17:59:15',
   2, '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (158, 11, 'Go sky diving …', 'Go sky diving', '2016-11-13 17:59:15', 3, '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (159, 12, 'Ride a Segway …', 'Ride a Segway', '2016-11-13 17:59:15', 3, '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (160, 10, 'Learn to surf …', 'Learn to surf', '2016-11-13 17:59:15', 2, '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (161, 29, 'Learn to Kite Surf …', 'Learn to Kite Surf', '2016-11-13 17:59:15', 2, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (162, 21, 'Learn to Windsurf …', 'Learn to Windsurf', '2016-11-13 17:59:15', 4, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (163, 16, 'Learn to say hello in 50 languages …', 'Learn to say hello in 50 languages', '2016-11-13 17:59:15', 4,
   '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (164, 28, 'Invent Something …', 'Invent Something', '2016-11-13 17:59:15', 5, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (165, 18, 'Join a Book Club …', 'Join a Book Club', '2016-11-13 17:59:15', 2, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (166, 14, 'Learn the Heimlich Manuever …', 'Learn the Heimlich Manuever', '2016-11-13 17:59:15', 5,
   '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (167, 26, 'Learn to Juggle …', 'Learn to Juggle', '2016-11-13 17:59:15', 2, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (168, 11, 'Leave My Mark in Graffiti …', 'Leave My Mark in Graffiti', '2016-11-13 17:59:15', 3, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (169, 3, 'Make a Balloon Animal …', 'Make a Balloon Animal', '2016-11-13 17:59:15', 2, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (170, 23, 'Make a Model Car …', 'Make a Model Car', '2016-11-13 17:59:15', 5, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (171, 8, 'Own an Original Piece of Artwork …', 'Own an Original Piece of Artwork', '2016-11-13 17:59:15', 3,
   '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (172, 18, 'Photobomb Someone …', 'Photobomb Someone', '2016-11-13 17:59:15', 2, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (173, 1, 'Race an RC Car …', 'Race an RC Car', '2016-11-13 17:59:15', 5, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (174, 2, 'Read a Book on the NY Best Sellers List …', 'Read a Book on the NY Best Sellers List',
   '2016-11-13 17:59:15', 3, '2016-11-13 17:59:15', '2016-11-13 17:59:15');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (175, 12, 'Send a Message in a Bottle …', 'Send a Message in a Bottle', '2016-11-13 17:59:15', 4,
   '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (176, 9, 'Slide Down a Firehouse Pole …', 'Slide Down a Firehouse Pole', '2016-11-13 17:59:15', 4,
   '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (177, 3, 'Stay Awake for 24 Hours …', 'Stay Awake for 24 Hours', '2016-11-13 17:59:15', 3, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (178, 6, 'Surprise Someone …', 'Surprise Someone', '2016-11-13 17:59:15', 4, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (179, 7, 'Use a Paddle to Bid at an Auction …', 'Use a Paddle to Bid at an Auction', '2016-11-13 17:59:15', 2,
   '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (180, 14, 'Walk on Stilts …', 'Walk on Stilts', '2016-11-13 17:59:15', 1, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (181, 21, 'Win a Stuffed Animal at a Carnival …', 'Win a Stuffed Animal at a Carnival', '2016-11-13 17:59:15', 3,
   '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (182, 2, 'Write Your Name in Wet Cement …', 'Write Your Name in Wet Cement', '2016-11-13 17:59:15', 3,
   '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (183, 7, 'Learn to Use Chopsticks …', 'Learn to Use Chopsticks', '2016-11-13 17:59:15', 1, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (184, 12, 'Make a Gingerbread House …', 'Make a Gingerbread House', '2016-11-13 17:59:15', 4, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (185, 8, 'Make Cheese …', 'Make Cheese', '2016-11-13 17:59:15', 3, '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (186, 3, 'Make Fresh Pasta …', 'Make Fresh Pasta', '2016-11-13 17:59:15', 1, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (187, 26, 'Make Ice Cream …', 'Make Ice Cream', '2016-11-13 17:59:15', 1, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (188, 19, 'Own a Food Cart …', 'Own a Food Cart', '2016-11-13 17:59:15', 3, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (189, 16, 'Partake in a Food Fight …', 'Partake in a Food Fight', '2016-11-13 17:59:15', 3, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (190, 29, 'Partake in Afternoon Tea …', 'Partake in Afternoon Tea', '2016-11-13 17:59:15', 3, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (191, 12, 'Pick Fruit From the Tree & Make a Pie …', 'Pick Fruit From the Tree & Make a Pie', '2016-11-13 17:59:15',
   5, '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (192, 25, 'Recreate a Childhood Recipe …', 'Recreate a Childhood Recipe', '2016-11-13 17:59:15', 2,
   '2016-11-13 17:59:15', '2016-11-13 17:59:15');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (193, 27, 'Recreate a Classic Dish …', 'Recreate a Classic Dish', '2016-11-13 17:59:15', 4, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (194, 17, 'Start an Herb Garden …', 'Start an Herb Garden', '2016-11-13 17:59:15', 4, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (195, 26, 'Take a Cooking Class …', 'Take a Cooking Class', '2016-11-13 17:59:15', 2, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (196, 19, 'Toss Pizza Dough in the Air …', 'Toss Pizza Dough in the Air', '2016-11-13 17:59:15', 4,
   '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (197, 20, 'Write a cookbook …', 'Write a cookbook', '2016-11-13 17:59:15', 4, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (198, 20, 'Bake a Cake for Someone Special …', 'Bake a Cake for Someone Special', '2016-11-13 17:59:15', 4,
   '2016-11-13 17:59:15', '2016-11-13 17:59:15');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (199, 18, 'Bake a Loaf of Bread …', 'Bake a Loaf of Bread', '2016-11-13 17:59:15', 5, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (200, 14, 'Bottle a Recipe & Sell it …', 'Bottle a Recipe & Sell it', '2016-11-13 17:59:15', 2, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (201, 17, 'Catch, Cook & Eat a Fish …', 'Catch, Cook & Eat a Fish', '2016-11-13 17:59:15', 3, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (202, 16, 'Cook a Traditional Dish from a Different Culture …', 'Cook a Traditional Dish from a Different Culture',
   '2016-11-13 17:59:15', 2, '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (203, 5, 'Cook Christmas Dinner …', 'Cook Christmas Dinner', '2016-11-13 17:59:15', 4, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (204, 16, 'Create an Ice Sculpture …', 'Create an Ice Sculpture', '2016-11-13 17:59:15', 2, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (205, 26, 'Cook Every Dish in One Cookbook …', 'Cook Every Dish in One Cookbook', '2016-11-13 17:59:15', 4,
   '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (206, 17, 'Cook With a Celebrity Chef …', 'Cook With a Celebrity Chef', '2016-11-13 17:59:15', 1,
   '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (207, 20, 'Create a New Ice Cream Flavor …', 'Create a New Ice Cream Flavor', '2016-11-13 17:59:15', 2,
   '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (208, 15, 'Create Food Art …', 'Create Food Art', '2016-11-13 17:59:15', 1, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (209, 11, 'Create Latte Art …', 'Create Latte Art', '2016-11-13 17:59:15', 4, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (210, 1, 'Create my Own Cocktail …', 'Create my Own Cocktail', '2016-11-13 17:59:15', 2, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (211, 26, 'Create my Own Recipe …', 'Create my Own Recipe', '2016-11-13 17:59:15', 4, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (212, 7, 'Create my Signature Dish …', 'Create my Signature Dish', '2016-11-13 17:59:15', 2, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (213, 12, 'Dismember a Chicken …', 'Dismember a Chicken', '2016-11-13 17:59:15', 2, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (214, 16, 'Create a Passive Income …', 'Create a Passive Income', '2016-11-13 17:59:15', 4, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (215, 14, 'Order Room Service …', 'Order Room Service', '2016-11-13 17:59:15', 4, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (216, 13, 'Sleep in a Castle …', 'Sleep in a Castle', '2016-11-13 17:59:15', 2, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (217, 29, 'Sleep on Satin Sheets …', 'Sleep on Satin Sheets', '2016-11-13 17:59:15', 1, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (218, 24, 'Sell Something on the Internet …', 'Sell Something on the Internet', '2016-11-13 17:59:15', 1,
   '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (219, 24, 'Stay in a 5-Star Resort …', 'Stay in a 5-Star Resort', '2016-11-13 17:59:15', 1, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (220, 26, 'Be Blond for a Day …', 'Be Blond for a Day', '2016-11-13 17:59:15', 3, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (221, 29, 'Buy a Cute Outfit at a Second Hand Store …', 'Buy a Cute Outfit at a Second Hand Store',
   '2016-11-13 17:59:15', 3, '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (222, 10, 'Dye My Hair a Crazy Color …', 'Dye My Hair a Crazy Color', '2016-11-13 17:59:15', 3, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (223, 21, 'Get a Fish Pedicure …', 'Get a Fish Pedicure', '2016-11-13 17:59:15', 2, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (224, 17, 'Get a Foot Massage …', 'Get a Foot Massage', '2016-11-13 17:59:15', 3, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (225, 18, 'Get a Spray Tan …', 'Get a Spray Tan', '2016-11-13 17:59:15', 2, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (226, 5, 'Get a Professional Body Massage …', 'Get a Professional Body Massage', '2016-11-13 17:59:15', 4,
   '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (227, 4, 'Have a Professional Photo Shoot …', 'Have a Professional Photo Shoot', '2016-11-13 17:59:15', 5,
   '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (228, 4, 'Wear Colored Contacts …', 'Wear Colored Contacts', '2016-11-13 17:59:15', 4, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (229, 27, 'Read the Book Before the Movie …', 'Read the Book Before the Movie', '2016-11-13 17:59:15', 5,
   '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (230, 2, 'Record a Song …', 'Record a Song', '2016-11-13 17:59:15', 2, '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (231, 5, 'Ride a Mechanical Bull …', 'Ride a Mechanical Bull', '2016-11-13 17:59:15', 3, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (232, 15, 'Ride on a Ferris Wheel …', 'Ride on a Ferris Wheel', '2016-11-13 17:59:15', 5, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (233, 22, 'See a 3-D movie …', 'See a 3-D movie', '2016-11-13 17:59:15', 1, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (234, 4, 'See a Ballet …', 'See a Ballet', '2016-11-13 17:59:15', 3, '2016-11-13 17:59:15', '2016-11-13 17:59:15');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (235, 26, 'See a Broadway Play …', 'See a Broadway Play', '2016-11-13 17:59:15', 2, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (236, 22, 'A Cirque du Soleil Show …', 'A Cirque du Soleil Show', '2016-11-13 17:59:15', 1, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (237, 5, 'See a Foreign Film …', 'See a Foreign Film', '2016-11-13 17:59:15', 3, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (238, 14, 'See a Las Vegas Show …', 'See a Las Vegas Show', '2016-11-13 17:59:15', 2, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (239, 25, 'See a TED Talk Live …', 'See a TED Talk Live', '2016-11-13 17:59:15', 5, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (240, 21, 'See an Opera …', 'See an Opera', '2016-11-13 17:59:15', 3, '2016-11-13 17:59:15', '2016-11-13 17:59:15');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (241, 5, 'See My Favorite Band Play Live …', 'See My Favorite Band Play Live', '2016-11-13 17:59:15', 2,
   '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (242, 4, 'Sing Karaoke in Public …', 'Sing Karaoke in Public', '2016-11-13 17:59:15', 3, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (243, 15, 'Take a New Route to Work …', 'Take a New Route to Work', '2016-11-13 17:59:15', 4, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (244, 8, 'Take an Improv Class …', 'Take an Improv Class', '2016-11-13 17:59:15', 4, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (245, 22, 'Take Part in a Focus Group …', 'Take Part in a Focus Group', '2016-11-13 17:59:15', 3,
   '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (246, 20, 'Throw a Themed Party …', 'Throw a Themed Party', '2016-11-13 17:59:15', 1, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (247, 12, 'Throw Beads at Mardi Gras …', 'Throw Beads at Mardi Gras', '2016-11-13 17:59:15', 2, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (248, 20, 'Touch a Famous Piece of Art …', 'Touch a Famous Piece of Art', '2016-11-13 17:59:15', 4,
   '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (249, 27, 'Try Pole Dancing …', 'Try Pole Dancing', '2016-11-13 17:59:15', 4, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (250, 19, 'Wear a Sumo Wrestling Suit …', 'Wear a Sumo Wrestling Suit', '2016-11-13 17:59:15', 2,
   '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (251, 8, 'Yodel …', 'Yodel', '2016-11-13 17:59:15', 5, '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (252, 28, 'Act in a Play …', 'Act in a Play', '2016-11-13 17:59:15', 1, '2016-11-13 17:59:15', '2016-11-13 17:59:15');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (253, 26, 'Attend a Foam Party …', 'Attend a Foam Party', '2016-11-13 17:59:15', 2, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (254, 27, 'Attend a Gallery Opening …', 'Attend a Gallery Opening', '2016-11-13 17:59:15', 3, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (255, 10, 'Attend a Gay Pride Event …', 'Attend a Gay Pride Event', '2016-11-13 17:59:15', 3, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (256, 19, 'Attend a Jazz Festival …', 'Attend a Jazz Festival', '2016-11-13 17:59:15', 5, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (257, 8, 'Attend a Masquerade …', 'Attend a Masquerade', '2016-11-13 17:59:15', 3, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (258, 9, 'Attend a Murder Mystery Dinner …', 'Attend a Murder Mystery Dinner', '2016-11-13 17:59:15', 3,
   '2016-11-13 17:59:15', '2016-11-13 17:59:15');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (259, 11, 'Attend a Music Festival …', 'Attend a Music Festival', '2016-11-13 17:59:15', 3, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (260, 27, 'Attend a Poetry Reading …', 'Attend a Poetry Reading', '2016-11-13 17:59:15', 2, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (261, 17, 'Be a Game Show Contestant …', 'Be a Game Show Contestant', '2016-11-13 17:59:15', 2, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (262, 25, 'Be a Member of a TV Studio Audience …', 'Be a Member of a TV Studio Audience', '2016-11-13 17:59:15', 4,
   '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (263, 23, 'Be an Extra in a Movie …', 'Be an Extra in a Movie', '2016-11-13 17:59:15', 1, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (264, 10, 'Be on a Radio Show …', 'Be on a Radio Show', '2016-11-13 17:59:15', 2, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (265, 27, 'Be on a TV Show …', 'Be on a TV Show', '2016-11-13 17:59:15', 3, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (266, 18, 'Be on the Cover of a Magazine …', 'Be on the Cover of a Magazine', '2016-11-13 17:59:15', 4,
   '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (267, 22, 'Be a Street Performer …', 'Be a Street Performer', '2016-11-13 17:59:15', 3, '2016-11-13 17:59:15',
   '2016-11-13 17:59:15'),
  (268, 5, 'Create a Video & Upload it to the Internet …', 'Create a Video & Upload it to the Internet',
   '2016-11-13 17:59:15', 1, '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (269, 21, 'Crowd Surf …', 'Crowd Surf', '2016-11-13 17:59:15', 3, '2016-11-13 17:59:15', '2016-11-13 17:59:15'),
  (270, 10, 'Do the Hula …', 'Do the Hula', '2016-11-13 17:59:16', 2, '2016-11-13 17:59:16', '2016-11-13 17:59:16');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (271, 10, 'Get a Caricature Drawing by a Street Artist …', 'Get a Caricature Drawing by a Street Artist',
   '2016-11-13 17:59:16', 3, '2016-11-13 17:59:16', '2016-11-13 17:59:16'),
  (272, 1, 'Get Hypnotized …', 'Get Hypnotized', '2016-11-13 17:59:16', 5, '2016-11-13 17:59:16',
   '2016-11-13 17:59:16'),
  (273, 9, 'Go on a Cruise …', 'Go on a Cruise', '2016-11-13 17:59:16', 5, '2016-11-13 17:59:16',
   '2016-11-13 17:59:16'),
  (274, 25, 'Go to a Book Signing …', 'Go to a Book Signing', '2016-11-13 17:59:16', 4, '2016-11-13 17:59:16',
   '2016-11-13 17:59:16'),
  (275, 20, 'Go to a Drive-In Movie …', 'Go to a Drive-In Movie', '2016-11-13 17:59:16', 1, '2016-11-13 17:59:16',
   '2016-11-13 17:59:16'),
  (276, 16, 'Go to a Renaissance Festival …', 'Go to a Renaissance Festival', '2016-11-13 17:59:16', 5,
   '2016-11-13 17:59:16', '2016-11-13 17:59:16');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (277, 5, 'Go to a Tattoo Festival …', 'Go to a Tattoo Festival', '2016-11-13 17:59:16', 3, '2016-11-13 17:59:16',
   '2016-11-13 17:59:16'),
  (278, 21, 'Go to Comicon …', 'Go to Comicon', '2016-11-13 17:59:16', 1, '2016-11-13 17:59:16', '2016-11-13 17:59:16'),
  (279, 17, 'Go to the Movies by Myself …', 'Go to the Movies by Myself', '2016-11-13 17:59:16', 1,
   '2016-11-13 17:59:16', '2016-11-13 17:59:16'),
  (280, 28, 'Have 15 Minutes of Fame …', 'Have 15 Minutes of Fame', '2016-11-13 17:59:16', 5, '2016-11-13 17:59:16',
   '2016-11-13 17:59:16'),
  (281, 24, 'Have My Picture Published in a Newspaper …', 'Have My Picture Published in a Newspaper',
   '2016-11-13 17:59:16', 3, '2016-11-13 17:59:16', '2016-11-13 17:59:16'),
  (282, 7, 'Host a Game Night …', 'Host a Game Night', '2016-11-13 17:59:16', 4, '2016-11-13 17:59:16',
   '2016-11-13 17:59:16');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (283, 14, 'Join a Flash Mob …', 'Join a Flash Mob', '2016-11-13 17:59:16', 4, '2016-11-13 17:59:16',
   '2016-11-13 17:59:16'),
  (284, 1, 'Learn a Line Dance …', 'Learn a Line Dance', '2016-11-13 17:59:16', 2, '2016-11-13 17:59:16',
   '2016-11-13 17:59:16'),
  (285, 19, 'Learn How to Sing Within my Octave …', 'Learn How to Sing Within my Octave', '2016-11-13 17:59:16', 4,
   '2016-11-13 17:59:16', '2016-11-13 17:59:16'),
  (286, 18, 'Make a House of Cards …', 'Make a House of Cards', '2016-11-13 17:59:16', 3, '2016-11-13 17:59:16',
   '2016-11-13 17:59:16'),
  (287, 20, 'Master a Video Game …', 'Master a Video Game', '2016-11-13 17:59:16', 4, '2016-11-13 17:59:16',
   '2016-11-13 17:59:16'),
  (288, 22, 'Perform a Magic Trick …', 'Perform a Magic Trick', '2016-11-13 17:59:16', 2, '2016-11-13 17:59:16',
   '2016-11-13 17:59:16');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (289, 23, 'Play a Pinball Machine …', 'Play a Pinball Machine', '2016-11-13 17:59:16', 2, '2016-11-13 17:59:16',
   '2016-11-13 17:59:16'),
  (290, 17, 'Play a Song on a Harmonica …', 'Play a Song on a Harmonica', '2016-11-13 17:59:16', 3,
   '2016-11-13 17:59:16', '2016-11-13 17:59:16'),
  (291, 25, 'Act in a Play …', 'Act in a Play', '2016-11-13 17:59:16', 2, '2016-11-13 17:59:16', '2016-11-13 17:59:16'),
  (292, 17, 'Be Published …', 'Be Published', '2016-11-13 17:59:16', 1, '2016-11-13 17:59:16', '2016-11-13 17:59:16'),
  (293, 26, 'Blow Glass …', 'Blow Glass', '2016-11-13 17:59:16', 4, '2016-11-13 17:59:16', '2016-11-13 17:59:16'),
  (294, 11, 'Create a Bumper Sticker …', 'Create a Bumper Sticker', '2016-11-13 17:59:16', 5, '2016-11-13 17:59:16',
   '2016-11-13 17:59:16');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (295, 17, 'Create a Family Logo …', 'Create a Family Logo', '2016-11-13 17:59:16', 3, '2016-11-13 17:59:16',
   '2016-11-13 17:59:16'),
  (296, 26, 'Create a Family Tree …', 'Create a Family Tree', '2016-11-13 17:59:16', 4, '2016-11-13 17:59:16',
   '2016-11-13 17:59:16'),
  (297, 17, 'Create a Flower Arrangement …', 'Create a Flower Arrangement', '2016-11-13 17:59:16', 1,
   '2016-11-13 17:59:16', '2016-11-13 17:59:16'),
  (298, 14, 'Create a Piece of Art & Sell it …', 'Create a Piece of Art & Sell it', '2016-11-13 17:59:16', 4,
   '2016-11-13 17:59:16', '2016-11-13 17:59:16'),
  (299, 12, 'Create your own Personal Stationary …', 'Create your own Personal Stationary', '2016-11-13 17:59:16', 3,
   '2016-11-13 17:59:16', '2016-11-13 17:59:16'),
  (300, 17, 'Decorate a Blank T-Shirt …', 'Decorate a Blank T-Shirt', '2016-11-13 17:59:16', 1, '2016-11-13 17:59:16',
   '2016-11-13 17:59:16');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (301, 25, 'Design a Website …', 'Design a Website', '2016-11-13 17:59:16', 5, '2016-11-13 17:59:16',
   '2016-11-13 17:59:16'),
  (302, 4, 'Enter Art in an Exhibit …', 'Enter Art in an Exhibit', '2016-11-13 17:59:16', 5, '2016-11-13 17:59:16',
   '2016-11-13 17:59:16'),
  (303, 25, 'Jump Into a Pool Fully Clothed …', 'Jump Into a Pool Fully Clothed', '2016-11-13 17:59:16', 5,
   '2016-11-13 17:59:16', '2016-11-13 17:59:16'),
  (304, 25, 'Knit a Scarf …', 'Knit a Scarf', '2016-11-13 17:59:16', 1, '2016-11-13 17:59:16', '2016-11-13 17:59:16'),
  (305, 20, 'Make a Candle …', 'Make a Candle', '2016-11-13 17:59:16', 1, '2016-11-13 17:59:16', '2016-11-13 17:59:16'),
  (306, 24, 'Make a Coloring Book …', 'Make a Coloring Book', '2016-11-13 17:59:16', 2, '2016-11-13 17:59:16',
   '2016-11-13 17:59:16');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (307, 4, 'Make a Handmade Gift …', 'Make a Handmade Gift', '2016-11-13 17:59:16', 3, '2016-11-13 17:59:16',
   '2016-11-13 17:59:16'),
  (308, 1, 'Make a Handmade Greeting Card …', 'Make a Handmade Greeting Card', '2016-11-13 17:59:16', 3,
   '2016-11-13 17:59:16', '2016-11-13 17:59:16'),
  (309, 4, 'Make a Tie Dye Shirt …', 'Make a Tie Dye Shirt', '2016-11-13 17:59:16', 3, '2016-11-13 17:59:16',
   '2016-11-13 17:59:16'),
  (310, 22, 'Make an Origami Animal …', 'Make an Origami Animal', '2016-11-13 17:59:16', 2, '2016-11-13 17:59:16',
   '2016-11-13 17:59:16'),
  (311, 26, 'Make Mosaic Art …', 'Make Mosaic Art', '2016-11-13 17:59:16', 4, '2016-11-13 17:59:16',
   '2016-11-13 17:59:16'),
  (312, 29, 'Make Paper …', 'Make Paper', '2016-11-13 17:59:16', 5, '2016-11-13 17:59:16', '2016-11-13 17:59:16'),
  (313, 25, 'Make Soap …', 'Make Soap', '2016-11-13 17:59:16', 2, '2016-11-13 17:59:16', '2016-11-13 17:59:16');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (314, 23, 'Paint Something at a Ceramic Store …', 'Paint Something at a Ceramic Store', '2016-11-13 17:59:16', 3,
   '2016-11-13 17:59:16', '2016-11-13 17:59:16'),
  (315, 14, 'Sew Something you can Wear …', 'Sew Something you can Wear', '2016-11-13 17:59:16', 3,
   '2016-11-13 17:59:16', '2016-11-13 17:59:16'),
  (316, 23, 'Start a Blog …', 'Start a Blog', '2016-11-13 17:59:16', 1, '2016-11-13 17:59:16', '2016-11-13 17:59:16'),
  (317, 6, 'Take an Art Class …', 'Take an Art Class', '2016-11-13 17:59:16', 1, '2016-11-13 17:59:16',
   '2016-11-13 17:59:16'),
  (318, 3, 'Take Pictures in a Photo Booth …', 'Take Pictures in a Photo Booth', '2016-11-13 17:59:16', 3,
   '2016-11-13 17:59:16', '2016-11-13 17:59:16'),
  (319, 16, 'Work on a Pottery Wheel …', 'Work on a Pottery Wheel', '2016-11-13 17:59:16', 5, '2016-11-13 17:59:16',
   '2016-11-13 17:59:16');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (320, 20, 'Barefoot Waterski …', 'Barefoot Waterski', '2016-11-13 17:59:16', 4, '2016-11-13 17:59:16',
   '2016-11-13 17:59:16'),
  (321, 18, 'Bet on the Winning Horse …', 'Bet on the Winning Horse', '2016-11-13 17:59:16', 1, '2016-11-13 17:59:16',
   '2016-11-13 17:59:16'),
  (322, 13, 'Bowl a 200+ Game …', 'Bowl a 200+ Game', '2016-11-13 17:59:16', 4, '2016-11-13 17:59:16',
   '2016-11-13 17:59:16'),
  (323, 20, 'Climb an Indoor Rock Wall …', 'Climb an Indoor Rock Wall', '2016-11-13 17:59:16', 1, '2016-11-13 17:59:16',
   '2016-11-13 17:59:16'),
  (324, 22, 'Complete a Ropes Course …', 'Complete a Ropes Course', '2016-11-13 17:59:16', 3, '2016-11-13 17:59:16',
   '2016-11-13 17:59:16'),
  (325, 11, 'Dive Off the High Diving Board …', 'Dive Off the High Diving Board', '2016-11-13 17:59:16', 1,
   '2016-11-13 17:59:16', '2016-11-13 17:59:16');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (326, 12, 'Do a Belly Dance …', 'Do a Belly Dance', '2016-11-13 17:59:16', 4, '2016-11-13 17:59:16',
   '2016-11-13 17:59:16'),
  (327, 13, 'Do a Handstand …', 'Do a Handstand', '2016-11-13 17:59:16', 5, '2016-11-13 17:59:16',
   '2016-11-13 17:59:16'),
  (328, 9, 'Do a Themed Run …', 'Do a Themed Run', '2016-11-13 17:59:16', 3, '2016-11-13 17:59:16',
   '2016-11-13 17:59:16'),
  (329, 16, 'Do the Polar Bear Plunge …', 'Do the Polar Bear Plunge', '2016-11-13 17:59:16', 1, '2016-11-13 17:59:16',
   '2016-11-13 17:59:16'),
  (330, 6, 'Fly a Kite …', 'Fly a Kite', '2016-11-13 17:59:16', 3, '2016-11-13 17:59:16', '2016-11-13 17:59:16'),
  (331, 24, 'Go Ice Fishing …', 'Go Ice Fishing', '2016-11-13 17:59:16', 5, '2016-11-13 17:59:16',
   '2016-11-13 17:59:16'),
  (332, 3, 'Go Deep Sea Fishing …', 'Go Deep Sea Fishing', '2016-11-13 17:59:16', 3, '2016-11-13 17:59:16',
   '2016-11-13 17:59:16');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (333, 13, 'Go Flyfishing …', 'Go Flyfishing', '2016-11-13 17:59:16', 5, '2016-11-13 17:59:16', '2016-11-13 17:59:16'),
  (334, 12, 'Go Heli-Skiing …', 'Go Heli-Skiing', '2016-11-13 17:59:16', 4, '2016-11-13 17:59:16',
   '2016-11-13 17:59:16'),
  (335, 22, 'Go Snowshoeing …', 'Go Snowshoeing', '2016-11-13 17:59:16', 5, '2016-11-13 17:59:16',
   '2016-11-13 17:59:16'),
  (336, 13, 'Go to a World Series Game …', 'Go to a World Series Game', '2016-11-13 17:59:16', 3, '2016-11-13 17:59:16',
   '2016-11-13 17:59:16'),
  (337, 28, 'Go to a Yoga Retreat …', 'Go to a Yoga Retreat', '2016-11-13 17:59:16', 5, '2016-11-13 17:59:16',
   '2016-11-13 17:59:16'),
  (338, 4, 'Go to Nascar …', 'Go to Nascar', '2016-11-13 17:59:16', 3, '2016-11-13 17:59:16', '2016-11-13 17:59:16'),
  (339, 12, 'Go to the SuperBowl …', 'Go to the SuperBowl', '2016-11-13 17:59:16', 4, '2016-11-13 17:59:16',
   '2016-11-13 17:59:16');
INSERT INTO `todos` (`id`, `user_id`, `title`, `description`, `date_due`, `priority`, `date_added`, `date_updated`)
VALUES
  (340, 9, 'Golf 18 Holes …', 'Golf 18 Holes', '2016-11-13 17:59:16', 4, '2016-11-13 17:59:16', '2016-11-13 17:59:16'),
  (341, 5, 'Hit the Archery Bulls-Eye …', 'Hit the Archery Bulls-Eye', '2016-11-13 17:59:16', 4, '2016-11-13 17:59:16',
   '2016-11-13 17:59:16'),
  (342, 15, 'Join a Bowling League …', 'Join a Bowling League', '2016-11-13 17:59:16', 2, '2016-11-13 17:59:16',
   '2016-11-13 17:59:16'),
  (343, 3, 'Jump at a Trampoline House …', 'Jump at a Trampoline House', '2016-11-13 17:59:16', 3,
   '2016-11-13 17:59:16', '2016-11-13 17:59:16'),
  (344, 4, 'Kayak …', 'Kayak', '2016-11-13 17:59:16', 5, '2016-11-13 17:59:16', '2016-11-13 17:59:16'),
  (345, 23, 'Learn to Curl …', 'Learn to Curl', '2016-11-13 17:59:16', 4, '2016-11-13 17:59:16', '2016-11-13 17:59:16');

/*!40101 SET CHARACTER_SET_CLIENT = @`old_character_set_client` */;
/*!40101 SET CHARACTER_SET_RESULTS = @`old_character_set_results` */;
/*!40101 SET COLLATION_CONNECTION = @`old_collation_connection` */;

-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 14, 2016 at 05:11 AM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 7.0.4

SET SQL_MODE = "no_auto_value_on_zero";
SET time_zone = "+00:00";


/*!40101 SET @`old_character_set_client` = @@`character_set_client` */;
/*!40101 SET @`old_character_set_results` = @@`character_set_results` */;
/*!40101 SET @`old_collation_connection` = @@`collation_connection` */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ajg_todo`
--

-- --------------------------------------------------------

--
-- Table structure for table `userstatuses`
--

DROP TABLE IF EXISTS `statuses`;
DROP TABLE IF EXISTS `userstatuses`;
CREATE TABLE IF NOT EXISTS `userstatuses` (
  `id`           CHAR(1)      NOT NULL,
  `name`         VARCHAR(31)  NOT NULL,
  `description`  VARCHAR(255) NOT NULL,
  `date_added`   DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP
)
  ENGINE = InnoDB
  DEFAULT CHARSET = `utf8`;

--
-- Dumping data for table `userstatuses`
--

INSERT INTO `userstatuses` (`id`, `name`, `description`, `date_added`, `date_updated`)
VALUES
  ('A', 'Active', 'Account has been activated.', '2016-11-14 10:41:46', '2016-11-14 10:41:46'),
  ('W', 'Waiting', 'Waiting account verification.', '2016-11-14 10:41:46', '2016-11-14 10:41:46'),
  ('S', 'Suspended', 'User has suspended their account', '2016-11-14 10:42:51', '2016-11-14 10:42:51'),
  ('T', 'Terminated',
   'Account has been terminated by the Administration due to violation of the terms of service, including appropriate use of the system.',
   '2016-11-14 10:42:51', '2016-11-14 10:42:51'),
  ('D', 'Deleted',
   'User has deleted their account. The account stays oin file for a period of time before being deleted by the admin. deleting includes all associate data from the user.',
   '2016-11-14 10:44:50', '2016-11-14 10:44:50'),
  ('H', 'Hold',
   'Account is on hold. This is the same status as Suspended, but an Administrator has placed the account into this status.',
   '2016-11-14 10:44:50', '2016-11-14 10:44:50');

/*!40101 SET CHARACTER_SET_CLIENT = @`old_character_set_client` */;
/*!40101 SET CHARACTER_SET_RESULTS = @`old_character_set_results` */;
/*!40101 SET COLLATION_CONNECTION = @`old_collation_connection` */;

-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 14, 2016 at 05:36 AM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 7.0.4

SET SQL_MODE = "no_auto_value_on_zero";
SET time_zone = "+00:00";


/*!40101 SET @`old_character_set_client` = @@`character_set_client` */;
/*!40101 SET @`old_character_set_results` = @@`character_set_results` */;
/*!40101 SET @`old_collation_connection` = @@`collation_connection` */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ajg_todo`
--

-- --------------------------------------------------------

--
-- Table structure for table `userroles`
--

DROP TABLE IF EXISTS `roles`;
DROP TABLE IF EXISTS `userroles`;
CREATE TABLE IF NOT EXISTS `userroles` (
  `id`           CHAR(1)     NOT NULL,
  `name`         VARCHAR(32) NOT NULL,
  `description`  VARCHAR(255)         DEFAULT NULL,
  `date_added`   DATETIME    NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` DATETIME    NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = `latin1`;

--
-- Dumping data for table `userroles`
--

INSERT INTO `userroles` (`id`, `name`, `description`, `date_added`, `date_updated`)
VALUES
  ('A', 'Administrator', 'Admin users are able to edit other users, but not able to delete or change the Superusers.',
   '2016-11-14 10:25:48', '2016-11-14 10:25:48'),
  ('S', 'Guru',
   'Super User: highest level of administration. Able to do anything with the system. Usually one superuser with a possible secondary for emergencies.',
   '2016-11-14 10:25:48', '2016-11-14 10:25:48'),
  ('U', 'User', 'Ordinary user. These users are able to work on their own accounts only.', '2016-11-14 10:26:59',
   '2016-11-14 10:26:59'),
  ('X', 'Unknown',
   'Unknown userroles are a way of signifying a totally obsolete user, a user that has had an account mysteriously added without correct data, or similar.',
   '2016-11-14 10:26:59', '2016-11-14 10:26:59');

/*!40101 SET CHARACTER_SET_CLIENT = @`old_character_set_client` */;
/*!40101 SET CHARACTER_SET_RESULTS = @`old_character_set_results` */;
/*!40101 SET COLLATION_CONNECTION = @`old_collation_connection` */;

-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 14, 2016 at 05:11 AM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 7.0.4

SET SQL_MODE = "no_auto_value_on_zero";
SET time_zone = "+00:00";


/*!40101 SET @`old_character_set_client` = @@`character_set_client` */;
/*!40101 SET @`old_character_set_results` = @@`character_set_results` */;
/*!40101 SET @`old_collation_connection` = @@`collation_connection` */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ajg_todo`
--

-- --------------------------------------------------------

--
-- Table structure for table `userstatuses`
--

DROP TABLE IF EXISTS `todostatuses`;
CREATE TABLE IF NOT EXISTS `todostatuses` (
  `id`           CHAR(2)      NOT NULL,
  `title`        VARCHAR(31)  NOT NULL,
  `description`  VARCHAR(255) NOT NULL,
  `date_added`   DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP
)
  ENGINE = InnoDB
  DEFAULT CHARSET = `utf8`;

--
-- Dumping data for table `userstatuses`
--

INSERT INTO `todostatuses` (`id`, `title`, `description`) VALUES
  ('IP', 'In progress', 'Item is currently being worked upon'),
  ('WC', 'Waiting', 'Item is waiting to be commenced.'),
  ('WO', 'Waiting', 'Item is waiting for another person or item to be completed'),
  ('CO', 'Complete', 'Item has been completed.'),
  ('DE', 'Deleted', 'Item is deleted.'),
  ('HO', 'Hold', 'Item on hold.'),
  ('XX', 'Unknown', 'Unknown status for item.');

/*!40101 SET CHARACTER_SET_CLIENT = @`old_character_set_client` */;
/*!40101 SET CHARACTER_SET_RESULTS = @`old_character_set_results` */;
/*!40101 SET COLLATION_CONNECTION = @`old_collation_connection` */;

-- Make changes to tables as needed
ALTER TABLE `userroles`
  CHANGE `name` `title` VARCHAR(32)
CHARACTER SET `latin1`
COLLATE `latin1_swedish_ci` NOT NULL;
ALTER TABLE `userstatuses`
  CHANGE `name` `title` VARCHAR(32)
CHARACTER SET `latin1`
COLLATE `latin1_swedish_ci` NOT NULL;


ALTER TABLE `users`
  ADD `photo` VARCHAR(255) NOT NULL DEFAULT 'anonymous-01@1x.png'
  AFTER `passwd`;
ALTER TABLE `users`
  ADD `folder` VARCHAR(255) NOT NULL DEFAULT 'assets//img//users//'
  AFTER `passwd`;
ALTER TABLE `users`
  ADD `gender` CHAR(1) NOT NULL DEFAULT 'X'
  AFTER `lastname`;

ALTER TABLE `todos`
  ADD `status` CHAR(2) NOT NULL DEFAULT 'XX'
  AFTER `priority`;

-- Add randomised and/or set data

UPDATE `users`
SET `gender` = 'f'
WHERE `id` IN (10, 11, 12, 13, 14, 16, 19, 21, 22, 26, 27);

UPDATE `users`
SET `gender` = 'm'
WHERE `id` IN (2, 3, 5, 6, 7, 8, 9, 15, 18, 24, 25, 28);

UPDATE `users`
SET `gender` = 'o'
WHERE `id` IN (20, 29);

UPDATE `users`
SET `gender` = 'i'
WHERE `id` IN (17, 4);

UPDATE `users`
SET `folder` = 'assets\\img\\users\\';

-- RAND() * n --- n is one less than total avatar images
UPDATE `users`
SET `photo` = CONCAT('man-', LPAD(ROUND(1 + RAND() * 48), 2, '0'),
                     '@1x.png')
WHERE `gender` = 'm';

UPDATE `users`
SET `photo` = CONCAT('superuser-', LPAD(ROUND(1 + RAND() * 5), 2, '0'),
                     '@1x.png')
WHERE `id` = 1;

UPDATE `users`
SET `photo` = CONCAT('woman-', LPAD(ROUND(1 + RAND() * 47), 2, '0'),
                     '@1x.png')
WHERE `gender` = 'f';

UPDATE `users`
SET `photo` = CONCAT('alien-', LPAD(ROUND(1 + RAND() * 2), 2, '0'),
                     '@1x.png')
WHERE `gender` = 'o';

UPDATE `users`
SET `photo` = CONCAT('intersex-', LPAD(ROUND(1 + RAND() * 3), 2, '0'),
                     '@1x.png')
WHERE `gender` = 'i';

UPDATE `todos`
SET `status` = ELT(1 + FLOOR(RAND() * 20),
                   'IP', 'IP',
                   'WC', 'WC', 'WC', 'WC', 'WC', 'WC', 'WC',
                   'WO', 'WO', 'WO', 'WO', 'WO',
                   'CO', 'CO',
                   'DE',
                   'HO', 'HO',
                   'XX');

ALTER TABLE `users`
  ADD IF NOT EXISTS `date_activity` DATETIME NULL DEFAULT
NULL AFTER `date_logout`;

ALTER TABLE `users`
  ADD IF NOT EXISTS `timezone` VARCHAR (64) NOT NULL
DEFAULT 'UTC' AFTER `passwd`;

ALTER TABLE `users`
  ADD INDEX IF NOT EXISTS `timezone_index` (`timezone`);


ALTER TABLE `users`
  ADD IF NOT EXISTS `country` VARCHAR (8) NULL DEFAULT NULL
AFTER `passwd`;
ALTER TABLE `users`
  ADD INDEX IF NOT EXISTS `country_index` (`country`);

DROP TABLE IF EXISTS `country`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country` (
  `country_code` CHAR(2)
                 COLLATE utf8_bin DEFAULT NULL,
  `country_name` VARCHAR(45)
                 COLLATE utf8_bin DEFAULT NULL,
  KEY `idx_country_code` (`country_code`)
)
  ENGINE = MyISAM
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;
INSERT INTO `country`
VALUES ('AD', 'Andorra'), ('AE', 'United Arab Emirates'), ('AF', 'Afghanistan'), ('AG', 'Antigua and Barbuda'),
  ('AI', 'Anguilla'), ('AL', 'Albania'), ('AM', 'Armenia'), ('AN', 'Netherlands Antilles'), ('AO', 'Angola'),
  ('AQ', 'Antarctica'), ('AR', 'Argentina'), ('AS', 'American Samoa'), ('AT', 'Austria'), ('AU', 'Australia'),
  ('AW', 'Aruba'), ('AX', 'Aland Islands'), ('AZ', 'Azerbaijan'), ('BA', 'Bosnia and Herzegovina'), ('BB', 'Barbados'),
  ('BD', 'Bangladesh'), ('BE', 'Belgium'), ('BF', 'Burkina Faso'), ('BG', 'Bulgaria'), ('BH', 'Bahrain'),
  ('BI', 'Burundi'), ('BJ', 'Benin'), ('BL', 'Saint Barthélemy'), ('BM', 'Bermuda'), ('BN', 'Brunei'),
  ('BO', 'Bolivia'), ('BQ', 'Bonaire, Saint Eustatius and Saba '), ('BR', 'Brazil'), ('BS', 'Bahamas'),
  ('BT', 'Bhutan'), ('BV', 'Bouvet Island'), ('BW', 'Botswana'), ('BY', 'Belarus'), ('BZ', 'Belize'), ('CA', 'Canada'),
  ('CC', 'Cocos Islands'), ('CD', 'Democratic Republic of the Congo'), ('CF', 'Central African Republic'),
  ('CG', 'Republic of the Congo'), ('CH', 'Switzerland'), ('CI', 'Ivory Coast'), ('CK', 'Cook Islands'),
  ('CL', 'Chile'), ('CM', 'Cameroon'), ('CN', 'China'), ('CO', 'Colombia'), ('CR', 'Costa Rica'),
  ('CS', 'Serbia and Montenegro'), ('CU', 'Cuba'), ('CV', 'Cape Verde'), ('CW', 'Curaçao'), ('CX', 'Christmas Island'),
  ('CY', 'Cyprus'), ('CZ', 'Czech Republic'), ('DE', 'Germany'), ('DJ', 'Djibouti'), ('DK', 'Denmark'),
  ('DM', 'Dominica'), ('DO', 'Dominican Republic'), ('DZ', 'Algeria'), ('EC', 'Ecuador'), ('EE', 'Estonia'),
  ('EG', 'Egypt'), ('EH', 'Western Sahara'), ('ER', 'Eritrea'), ('ES', 'Spain'), ('ET', 'Ethiopia'), ('FI', 'Finland'),
  ('FJ', 'Fiji'), ('FK', 'Falkland Islands'), ('FM', 'Micronesia'), ('FO', 'Faroe Islands'), ('FR', 'France'),
  ('GA', 'Gabon'), ('GB', 'United Kingdom'), ('GD', 'Grenada'), ('GE', 'Georgia'), ('GF', 'French Guiana'),
  ('GG', 'Guernsey'), ('GH', 'Ghana'), ('GI', 'Gibraltar'), ('GL', 'Greenland'), ('GM', 'Gambia'), ('GN', 'Guinea'),
  ('GP', 'Guadeloupe'), ('GQ', 'Equatorial Guinea'), ('GR', 'Greece'),
  ('GS', 'South Georgia and the South Sandwich Islands'), ('GT', 'Guatemala'), ('GU', 'Guam'), ('GW', 'Guinea-Bissau'),
  ('GY', 'Guyana'), ('HK', 'Hong Kong'), ('HM', 'Heard Island and McDonald Islands'), ('HN', 'Honduras'),
  ('HR', 'Croatia'), ('HT', 'Haiti'), ('HU', 'Hungary'), ('ID', 'Indonesia'), ('IE', 'Ireland'), ('IL', 'Israel'),
  ('IM', 'Isle of Man'), ('IN', 'India'), ('IO', 'British Indian Ocean Territory'), ('IQ', 'Iraq'), ('IR', 'Iran'),
  ('IS', 'Iceland'), ('IT', 'Italy'), ('JE', 'Jersey'), ('JM', 'Jamaica'), ('JO', 'Jordan'), ('JP', 'Japan'),
  ('KE', 'Kenya'), ('KG', 'Kyrgyzstan'), ('KH', 'Cambodia'), ('KI', 'Kiribati'), ('KM', 'Comoros'),
  ('KN', 'Saint Kitts and Nevis'), ('KP', 'North Korea'), ('KR', 'South Korea'), ('KW', 'Kuwait'),
  ('KY', 'Cayman Islands'), ('KZ', 'Kazakhstan'), ('LA', 'Laos'), ('LB', 'Lebanon'), ('LC', 'Saint Lucia'),
  ('LI', 'Liechtenstein'), ('LK', 'Sri Lanka'), ('LR', 'Liberia'), ('LS', 'Lesotho'), ('LT', 'Lithuania'),
  ('LU', 'Luxembourg'), ('LV', 'Latvia'), ('LY', 'Libya'), ('MA', 'Morocco'), ('MC', 'Monaco'), ('MD', 'Moldova'),
  ('ME', 'Montenegro'), ('MF', 'Saint Martin'), ('MG', 'Madagascar'), ('MH', 'Marshall Islands'), ('MK', 'Macedonia'),
  ('ML', 'Mali'), ('MM', 'Myanmar'), ('MN', 'Mongolia'), ('MO', 'Macao'), ('MP', 'Northern Mariana Islands'),
  ('MQ', 'Martinique'), ('MR', 'Mauritania'), ('MS', 'Montserrat'), ('MT', 'Malta'), ('MU', 'Mauritius'),
  ('MV', 'Maldives'), ('MW', 'Malawi'), ('MX', 'Mexico'), ('MY', 'Malaysia'), ('MZ', 'Mozambique'), ('NA', 'Namibia'),
  ('NC', 'New Caledonia'), ('NE', 'Niger'), ('NF', 'Norfolk Island'), ('NG', 'Nigeria'), ('NI', 'Nicaragua'),
  ('NL', 'Netherlands'), ('NO', 'Norway'), ('NP', 'Nepal'), ('NR', 'Nauru'), ('NU', 'Niue'), ('NZ', 'New Zealand'),
  ('OM', 'Oman'), ('PA', 'Panama'), ('PE', 'Peru'), ('PF', 'French Polynesia'), ('PG', 'Papua New Guinea'),
  ('PH', 'Philippines'), ('PK', 'Pakistan'), ('PL', 'Poland'), ('PM', 'Saint Pierre and Miquelon'), ('PN', 'Pitcairn'),
  ('PR', 'Puerto Rico'), ('PS', 'Palestinian Territory'), ('PT', 'Portugal'), ('PW', 'Palau'), ('PY', 'Paraguay'),
  ('QA', 'Qatar'), ('RE', 'Reunion'), ('RO', 'Romania'), ('RS', 'Serbia'), ('RU', 'Russia'), ('RW', 'Rwanda'),
  ('SA', 'Saudi Arabia'), ('SB', 'Solomon Islands'), ('SC', 'Seychelles'), ('SD', 'Sudan'), ('SE', 'Sweden'),
  ('SG', 'Singapore'), ('SH', 'Saint Helena'), ('SI', 'Slovenia'), ('SJ', 'Svalbard and Jan Mayen'), ('SK', 'Slovakia'),
  ('SL', 'Sierra Leone'), ('SM', 'San Marino'), ('SN', 'Senegal'), ('SO', 'Somalia'), ('SR', 'Suriname'),
  ('SS', 'South Sudan'), ('ST', 'Sao Tome and Principe'), ('SV', 'El Salvador'), ('SX', 'Sint Maarten'),
  ('SY', 'Syria'), ('SZ', 'Swaziland'), ('TC', 'Turks and Caicos Islands'), ('TD', 'Chad'),
  ('TF', 'French Southern Territories'), ('TG', 'Togo'), ('TH', 'Thailand'), ('TJ', 'Tajikistan'), ('TK', 'Tokelau'),
  ('TL', 'East Timor'), ('TM', 'Turkmenistan'), ('TN', 'Tunisia'), ('TO', 'Tonga'), ('TR', 'Turkey'),
  ('TT', 'Trinidad and Tobago'), ('TV', 'Tuvalu'), ('TW', 'Taiwan'), ('TZ', 'Tanzania'), ('UA', 'Ukraine'),
  ('UG', 'Uganda'), ('UM', 'United States Minor Outlying Islands'), ('US', 'United States'), ('UY', 'Uruguay'),
  ('UZ', 'Uzbekistan'), ('VA', 'Vatican'), ('VC', 'Saint Vincent and the Grenadines'), ('VE', 'Venezuela'),
  ('VG', 'British Virgin Islands'), ('VI', 'U.S. Virgin Islands'), ('VN', 'Vietnam'), ('VU', 'Vanuatu'),
  ('WF', 'Wallis and Futuna'), ('WS', 'Samoa'), ('XK', 'Kosovo'), ('YE', 'Yemen'), ('YT', 'Mayotte'),
  ('ZA', 'South Africa'), ('ZM', 'Zambia'), ('ZW', 'Zimbabwe');

DROP TABLE IF EXISTS `zone`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zone` (
  `zone_id`      INT(10)          NOT NULL AUTO_INCREMENT,
  `country_code` CHAR(2)
                 COLLATE utf8_bin NOT NULL,
  `zone_name`    VARCHAR(35)
                 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`zone_id`),
  KEY `idx_country_code` (`country_code`),
  KEY `idx_zone_name` (`zone_name`)
)
  ENGINE = MyISAM
  AUTO_INCREMENT = 425
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;
INSERT INTO `zone`
VALUES (1, 'AD', 'Europe/Andorra'), (2, 'AE', 'Asia/Dubai'), (3, 'AF', 'Asia/Kabul'), (4, 'AG', 'America/Antigua'),
  (5, 'AI', 'America/Anguilla'), (6, 'AL', 'Europe/Tirane'), (7, 'AM', 'Asia/Yerevan'), (8, 'AO', 'Africa/Luanda'),
  (9, 'AQ', 'Antarctica/McMurdo'), (10, 'AQ', 'Antarctica/Casey'), (11, 'AQ', 'Antarctica/Davis'),
  (12, 'AQ', 'Antarctica/DumontDUrville'), (13, 'AQ', 'Antarctica/Mawson'), (14, 'AQ', 'Antarctica/Palmer'),
  (15, 'AQ', 'Antarctica/Rothera'), (16, 'AQ', 'Antarctica/Syowa'), (17, 'AQ', 'Antarctica/Troll'),
  (18, 'AQ', 'Antarctica/Vostok'), (19, 'AR', 'America/Argentina/Buenos_Aires'),
  (20, 'AR', 'America/Argentina/Cordoba'), (21, 'AR', 'America/Argentina/Salta'), (22, 'AR', 'America/Argentina/Jujuy'),
  (23, 'AR', 'America/Argentina/Tucuman'), (24, 'AR', 'America/Argentina/Catamarca'),
  (25, 'AR', 'America/Argentina/La_Rioja'), (26, 'AR', 'America/Argentina/San_Juan'),
  (27, 'AR', 'America/Argentina/Mendoza'), (28, 'AR', 'America/Argentina/San_Luis'),
  (29, 'AR', 'America/Argentina/Rio_Gallegos'), (30, 'AR', 'America/Argentina/Ushuaia'),
  (31, 'AS', 'Pacific/Pago_Pago'), (32, 'AT', 'Europe/Vienna'), (33, 'AU', 'Australia/Lord_Howe'),
  (34, 'AU', 'Antarctica/Macquarie'), (35, 'AU', 'Australia/Hobart'), (36, 'AU', 'Australia/Currie'),
  (37, 'AU', 'Australia/Melbourne'), (38, 'AU', 'Australia/Sydney'), (39, 'AU', 'Australia/Broken_Hill'),
  (40, 'AU', 'Australia/Brisbane'), (41, 'AU', 'Australia/Lindeman'), (42, 'AU', 'Australia/Adelaide'),
  (43, 'AU', 'Australia/Darwin'), (44, 'AU', 'Australia/Perth'), (45, 'AU', 'Australia/Eucla'),
  (46, 'AW', 'America/Aruba'), (47, 'AX', 'Europe/Mariehamn'), (48, 'AZ', 'Asia/Baku'), (49, 'BA', 'Europe/Sarajevo'),
  (50, 'BB', 'America/Barbados'), (51, 'BD', 'Asia/Dhaka'), (52, 'BE', 'Europe/Brussels'),
  (53, 'BF', 'Africa/Ouagadougou'), (54, 'BG', 'Europe/Sofia'), (55, 'BH', 'Asia/Bahrain'),
  (56, 'BI', 'Africa/Bujumbura'), (57, 'BJ', 'Africa/Porto-Novo'), (58, 'BL', 'America/St_Barthelemy'),
  (59, 'BM', 'Atlantic/Bermuda'), (60, 'BN', 'Asia/Brunei'), (61, 'BO', 'America/La_Paz'),
  (62, 'BQ', 'America/Kralendijk'), (63, 'BR', 'America/Noronha'), (64, 'BR', 'America/Belem'),
  (65, 'BR', 'America/Fortaleza'), (66, 'BR', 'America/Recife'), (67, 'BR', 'America/Araguaina'),
  (68, 'BR', 'America/Maceio'), (69, 'BR', 'America/Bahia'), (70, 'BR', 'America/Sao_Paulo'),
  (71, 'BR', 'America/Campo_Grande'), (72, 'BR', 'America/Cuiaba'), (73, 'BR', 'America/Santarem'),
  (74, 'BR', 'America/Porto_Velho'), (75, 'BR', 'America/Boa_Vista'), (76, 'BR', 'America/Manaus'),
  (77, 'BR', 'America/Eirunepe'), (78, 'BR', 'America/Rio_Branco'), (79, 'BS', 'America/Nassau'),
  (80, 'BT', 'Asia/Thimphu'), (81, 'BW', 'Africa/Gaborone'), (82, 'BY', 'Europe/Minsk'), (83, 'BZ', 'America/Belize'),
  (84, 'CA', 'America/St_Johns'), (85, 'CA', 'America/Halifax'), (86, 'CA', 'America/Glace_Bay'),
  (87, 'CA', 'America/Moncton'), (88, 'CA', 'America/Goose_Bay'), (89, 'CA', 'America/Blanc-Sablon'),
  (90, 'CA', 'America/Toronto'), (91, 'CA', 'America/Nipigon'), (92, 'CA', 'America/Thunder_Bay'),
  (93, 'CA', 'America/Iqaluit'), (94, 'CA', 'America/Pangnirtung'), (95, 'CA', 'America/Atikokan'),
  (96, 'CA', 'America/Winnipeg'), (97, 'CA', 'America/Rainy_River'), (98, 'CA', 'America/Resolute'),
  (99, 'CA', 'America/Rankin_Inlet'), (100, 'CA', 'America/Regina'), (101, 'CA', 'America/Swift_Current'),
  (102, 'CA', 'America/Edmonton'), (103, 'CA', 'America/Cambridge_Bay'), (104, 'CA', 'America/Yellowknife'),
  (105, 'CA', 'America/Inuvik'), (106, 'CA', 'America/Creston'), (107, 'CA', 'America/Dawson_Creek'),
  (108, 'CA', 'America/Fort_Nelson'), (109, 'CA', 'America/Vancouver'), (110, 'CA', 'America/Whitehorse'),
  (111, 'CA', 'America/Dawson'), (112, 'CC', 'Indian/Cocos'), (113, 'CD', 'Africa/Kinshasa'),
  (114, 'CD', 'Africa/Lubumbashi'), (115, 'CF', 'Africa/Bangui'), (116, 'CG', 'Africa/Brazzaville'),
  (117, 'CH', 'Europe/Zurich'), (118, 'CI', 'Africa/Abidjan'), (119, 'CK', 'Pacific/Rarotonga'),
  (120, 'CL', 'America/Santiago'), (121, 'CL', 'Pacific/Easter'), (122, 'CM', 'Africa/Douala'),
  (123, 'CN', 'Asia/Shanghai'), (124, 'CN', 'Asia/Urumqi'), (125, 'CO', 'America/Bogota'),
  (126, 'CR', 'America/Costa_Rica'), (127, 'CU', 'America/Havana'), (128, 'CV', 'Atlantic/Cape_Verde'),
  (129, 'CW', 'America/Curacao'), (130, 'CX', 'Indian/Christmas'), (131, 'CY', 'Asia/Nicosia'),
  (132, 'CY', 'Asia/Famagusta'), (133, 'CZ', 'Europe/Prague'), (134, 'DE', 'Europe/Berlin'),
  (135, 'DE', 'Europe/Busingen'), (136, 'DJ', 'Africa/Djibouti'), (137, 'DK', 'Europe/Copenhagen'),
  (138, 'DM', 'America/Dominica'), (139, 'DO', 'America/Santo_Domingo'), (140, 'DZ', 'Africa/Algiers'),
  (141, 'EC', 'America/Guayaquil'), (142, 'EC', 'Pacific/Galapagos'), (143, 'EE', 'Europe/Tallinn'),
  (144, 'EG', 'Africa/Cairo'), (145, 'EH', 'Africa/El_Aaiun'), (146, 'ER', 'Africa/Asmara'),
  (147, 'ES', 'Europe/Madrid'), (148, 'ES', 'Africa/Ceuta'), (149, 'ES', 'Atlantic/Canary'),
  (150, 'ET', 'Africa/Addis_Ababa'), (151, 'FI', 'Europe/Helsinki'), (152, 'FJ', 'Pacific/Fiji'),
  (153, 'FK', 'Atlantic/Stanley'), (154, 'FM', 'Pacific/Chuuk'), (155, 'FM', 'Pacific/Pohnpei'),
  (156, 'FM', 'Pacific/Kosrae'), (157, 'FO', 'Atlantic/Faroe'), (158, 'FR', 'Europe/Paris'),
  (159, 'GA', 'Africa/Libreville'), (160, 'GB', 'Europe/London'), (161, 'GD', 'America/Grenada'),
  (162, 'GE', 'Asia/Tbilisi'), (163, 'GF', 'America/Cayenne'), (164, 'GG', 'Europe/Guernsey'),
  (165, 'GH', 'Africa/Accra'), (166, 'GI', 'Europe/Gibraltar'), (167, 'GL', 'America/Godthab'),
  (168, 'GL', 'America/Danmarkshavn'), (169, 'GL', 'America/Scoresbysund'), (170, 'GL', 'America/Thule'),
  (171, 'GM', 'Africa/Banjul'), (172, 'GN', 'Africa/Conakry'), (173, 'GP', 'America/Guadeloupe'),
  (174, 'GQ', 'Africa/Malabo'), (175, 'GR', 'Europe/Athens'), (176, 'GS', 'Atlantic/South_Georgia'),
  (177, 'GT', 'America/Guatemala'), (178, 'GU', 'Pacific/Guam'), (179, 'GW', 'Africa/Bissau'),
  (180, 'GY', 'America/Guyana'), (181, 'HK', 'Asia/Hong_Kong'), (182, 'HN', 'America/Tegucigalpa'),
  (183, 'HR', 'Europe/Zagreb'), (184, 'HT', 'America/Port-au-Prince'), (185, 'HU', 'Europe/Budapest'),
  (186, 'ID', 'Asia/Jakarta'), (187, 'ID', 'Asia/Pontianak'), (188, 'ID', 'Asia/Makassar'),
  (189, 'ID', 'Asia/Jayapura'), (190, 'IE', 'Europe/Dublin'), (191, 'IL', 'Asia/Jerusalem'),
  (192, 'IM', 'Europe/Isle_of_Man'), (193, 'IN', 'Asia/Kolkata'), (194, 'IO', 'Indian/Chagos'),
  (195, 'IQ', 'Asia/Baghdad'), (196, 'IR', 'Asia/Tehran'), (197, 'IS', 'Atlantic/Reykjavik'),
  (198, 'IT', 'Europe/Rome'), (199, 'JE', 'Europe/Jersey'), (200, 'JM', 'America/Jamaica'), (201, 'JO', 'Asia/Amman'),
  (202, 'JP', 'Asia/Tokyo'), (203, 'KE', 'Africa/Nairobi'), (204, 'KG', 'Asia/Bishkek'), (205, 'KH', 'Asia/Phnom_Penh'),
  (206, 'KI', 'Pacific/Tarawa'), (207, 'KI', 'Pacific/Enderbury'), (208, 'KI', 'Pacific/Kiritimati'),
  (209, 'KM', 'Indian/Comoro'), (210, 'KN', 'America/St_Kitts'), (211, 'KP', 'Asia/Pyongyang'),
  (212, 'KR', 'Asia/Seoul'), (213, 'KW', 'Asia/Kuwait'), (214, 'KY', 'America/Cayman'), (215, 'KZ', 'Asia/Almaty'),
  (216, 'KZ', 'Asia/Qyzylorda'), (217, 'KZ', 'Asia/Aqtobe'), (218, 'KZ', 'Asia/Aqtau'), (219, 'KZ', 'Asia/Atyrau'),
  (220, 'KZ', 'Asia/Oral'), (221, 'LA', 'Asia/Vientiane'), (222, 'LB', 'Asia/Beirut'), (223, 'LC', 'America/St_Lucia'),
  (224, 'LI', 'Europe/Vaduz'), (225, 'LK', 'Asia/Colombo'), (226, 'LR', 'Africa/Monrovia'),
  (227, 'LS', 'Africa/Maseru'), (228, 'LT', 'Europe/Vilnius'), (229, 'LU', 'Europe/Luxembourg'),
  (230, 'LV', 'Europe/Riga'), (231, 'LY', 'Africa/Tripoli'), (232, 'MA', 'Africa/Casablanca'),
  (233, 'MC', 'Europe/Monaco'), (234, 'MD', 'Europe/Chisinau'), (235, 'ME', 'Europe/Podgorica'),
  (236, 'MF', 'America/Marigot'), (237, 'MG', 'Indian/Antananarivo'), (238, 'MH', 'Pacific/Majuro'),
  (239, 'MH', 'Pacific/Kwajalein'), (240, 'MK', 'Europe/Skopje'), (241, 'ML', 'Africa/Bamako'),
  (242, 'MM', 'Asia/Yangon'), (243, 'MN', 'Asia/Ulaanbaatar'), (244, 'MN', 'Asia/Hovd'), (245, 'MN', 'Asia/Choibalsan'),
  (246, 'MO', 'Asia/Macau'), (247, 'MP', 'Pacific/Saipan'), (248, 'MQ', 'America/Martinique'),
  (249, 'MR', 'Africa/Nouakchott'), (250, 'MS', 'America/Montserrat'), (251, 'MT', 'Europe/Malta'),
  (252, 'MU', 'Indian/Mauritius'), (253, 'MV', 'Indian/Maldives'), (254, 'MW', 'Africa/Blantyre'),
  (255, 'MX', 'America/Mexico_City'), (256, 'MX', 'America/Cancun'), (257, 'MX', 'America/Merida'),
  (258, 'MX', 'America/Monterrey'), (259, 'MX', 'America/Matamoros'), (260, 'MX', 'America/Mazatlan'),
  (261, 'MX', 'America/Chihuahua'), (262, 'MX', 'America/Ojinaga'), (263, 'MX', 'America/Hermosillo'),
  (264, 'MX', 'America/Tijuana'), (265, 'MX', 'America/Bahia_Banderas'), (266, 'MY', 'Asia/Kuala_Lumpur'),
  (267, 'MY', 'Asia/Kuching'), (268, 'MZ', 'Africa/Maputo'), (269, 'NA', 'Africa/Windhoek'),
  (270, 'NC', 'Pacific/Noumea'), (271, 'NE', 'Africa/Niamey'), (272, 'NF', 'Pacific/Norfolk'),
  (273, 'NG', 'Africa/Lagos'), (274, 'NI', 'America/Managua'), (275, 'NL', 'Europe/Amsterdam'),
  (276, 'NO', 'Europe/Oslo'), (277, 'NP', 'Asia/Kathmandu'), (278, 'NR', 'Pacific/Nauru'), (279, 'NU', 'Pacific/Niue'),
  (280, 'NZ', 'Pacific/Auckland'), (281, 'NZ', 'Pacific/Chatham'), (282, 'OM', 'Asia/Muscat'),
  (283, 'PA', 'America/Panama'), (284, 'PE', 'America/Lima'), (285, 'PF', 'Pacific/Tahiti'),
  (286, 'PF', 'Pacific/Marquesas'), (287, 'PF', 'Pacific/Gambier'), (288, 'PG', 'Pacific/Port_Moresby'),
  (289, 'PG', 'Pacific/Bougainville'), (290, 'PH', 'Asia/Manila'), (291, 'PK', 'Asia/Karachi'),
  (292, 'PL', 'Europe/Warsaw'), (293, 'PM', 'America/Miquelon'), (294, 'PN', 'Pacific/Pitcairn'),
  (295, 'PR', 'America/Puerto_Rico'), (296, 'PS', 'Asia/Gaza'), (297, 'PS', 'Asia/Hebron'),
  (298, 'PT', 'Europe/Lisbon'), (299, 'PT', 'Atlantic/Madeira'), (300, 'PT', 'Atlantic/Azores'),
  (301, 'PW', 'Pacific/Palau'), (302, 'PY', 'America/Asuncion'), (303, 'QA', 'Asia/Qatar'),
  (304, 'RE', 'Indian/Reunion'), (305, 'RO', 'Europe/Bucharest'), (306, 'RS', 'Europe/Belgrade'),
  (307, 'RU', 'Europe/Kaliningrad'), (308, 'RU', 'Europe/Moscow'), (309, 'RU', 'Europe/Simferopol'),
  (310, 'RU', 'Europe/Volgograd'), (311, 'RU', 'Europe/Kirov'), (312, 'RU', 'Europe/Astrakhan'),
  (313, 'RU', 'Europe/Saratov'), (314, 'RU', 'Europe/Ulyanovsk'), (315, 'RU', 'Europe/Samara'),
  (316, 'RU', 'Asia/Yekaterinburg'), (317, 'RU', 'Asia/Omsk'), (318, 'RU', 'Asia/Novosibirsk'),
  (319, 'RU', 'Asia/Barnaul'), (320, 'RU', 'Asia/Tomsk'), (321, 'RU', 'Asia/Novokuznetsk'),
  (322, 'RU', 'Asia/Krasnoyarsk'), (323, 'RU', 'Asia/Irkutsk'), (324, 'RU', 'Asia/Chita'), (325, 'RU', 'Asia/Yakutsk'),
  (326, 'RU', 'Asia/Khandyga'), (327, 'RU', 'Asia/Vladivostok'), (328, 'RU', 'Asia/Ust-Nera'),
  (329, 'RU', 'Asia/Magadan'), (330, 'RU', 'Asia/Sakhalin'), (331, 'RU', 'Asia/Srednekolymsk'),
  (332, 'RU', 'Asia/Kamchatka'), (333, 'RU', 'Asia/Anadyr'), (334, 'RW', 'Africa/Kigali'), (335, 'SA', 'Asia/Riyadh'),
  (336, 'SB', 'Pacific/Guadalcanal'), (337, 'SC', 'Indian/Mahe'), (338, 'SD', 'Africa/Khartoum'),
  (339, 'SE', 'Europe/Stockholm'), (340, 'SG', 'Asia/Singapore'), (341, 'SH', 'Atlantic/St_Helena'),
  (342, 'SI', 'Europe/Ljubljana'), (343, 'SJ', 'Arctic/Longyearbyen'), (344, 'SK', 'Europe/Bratislava'),
  (345, 'SL', 'Africa/Freetown'), (346, 'SM', 'Europe/San_Marino'), (347, 'SN', 'Africa/Dakar'),
  (348, 'SO', 'Africa/Mogadishu'), (349, 'SR', 'America/Paramaribo'), (350, 'SS', 'Africa/Juba'),
  (351, 'ST', 'Africa/Sao_Tome'), (352, 'SV', 'America/El_Salvador'), (353, 'SX', 'America/Lower_Princes'),
  (354, 'SY', 'Asia/Damascus'), (355, 'SZ', 'Africa/Mbabane'), (356, 'TC', 'America/Grand_Turk'),
  (357, 'TD', 'Africa/Ndjamena'), (358, 'TF', 'Indian/Kerguelen'), (359, 'TG', 'Africa/Lome'),
  (360, 'TH', 'Asia/Bangkok'), (361, 'TJ', 'Asia/Dushanbe'), (362, 'TK', 'Pacific/Fakaofo'), (363, 'TL', 'Asia/Dili'),
  (364, 'TM', 'Asia/Ashgabat'), (365, 'TN', 'Africa/Tunis'), (366, 'TO', 'Pacific/Tongatapu'),
  (367, 'TR', 'Europe/Istanbul'), (368, 'TT', 'America/Port_of_Spain'), (369, 'TV', 'Pacific/Funafuti'),
  (370, 'TW', 'Asia/Taipei'), (371, 'TZ', 'Africa/Dar_es_Salaam'), (372, 'UA', 'Europe/Kiev'),
  (373, 'UA', 'Europe/Uzhgorod'), (374, 'UA', 'Europe/Zaporozhye'), (375, 'UG', 'Africa/Kampala'),
  (376, 'UM', 'Pacific/Johnston'), (377, 'UM', 'Pacific/Midway'), (378, 'UM', 'Pacific/Wake'),
  (379, 'US', 'America/New_York'), (380, 'US', 'America/Detroit'), (381, 'US', 'America/Kentucky/Louisville'),
  (382, 'US', 'America/Kentucky/Monticello'), (383, 'US', 'America/Indiana/Indianapolis'),
  (384, 'US', 'America/Indiana/Vincennes'), (385, 'US', 'America/Indiana/Winamac'),
  (386, 'US', 'America/Indiana/Marengo'), (387, 'US', 'America/Indiana/Petersburg'),
  (388, 'US', 'America/Indiana/Vevay'), (389, 'US', 'America/Chicago'), (390, 'US', 'America/Indiana/Tell_City'),
  (391, 'US', 'America/Indiana/Knox'), (392, 'US', 'America/Menominee'), (393, 'US', 'America/North_Dakota/Center'),
  (394, 'US', 'America/North_Dakota/New_Salem'), (395, 'US', 'America/North_Dakota/Beulah'),
  (396, 'US', 'America/Denver'), (397, 'US', 'America/Boise'), (398, 'US', 'America/Phoenix'),
  (399, 'US', 'America/Los_Angeles'), (400, 'US', 'America/Anchorage'), (401, 'US', 'America/Juneau'),
  (402, 'US', 'America/Sitka'), (403, 'US', 'America/Metlakatla'), (404, 'US', 'America/Yakutat'),
  (405, 'US', 'America/Nome'), (406, 'US', 'America/Adak'), (407, 'US', 'Pacific/Honolulu'),
  (408, 'UY', 'America/Montevideo'), (409, 'UZ', 'Asia/Samarkand'), (410, 'UZ', 'Asia/Tashkent'),
  (411, 'VA', 'Europe/Vatican'), (412, 'VC', 'America/St_Vincent'), (413, 'VE', 'America/Caracas'),
  (414, 'VG', 'America/Tortola'), (415, 'VI', 'America/St_Thomas'), (416, 'VN', 'Asia/Ho_Chi_Minh'),
  (417, 'VU', 'Pacific/Efate'), (418, 'WF', 'Pacific/Wallis'), (419, 'WS', 'Pacific/Apia'), (420, 'YE', 'Asia/Aden'),
  (421, 'YT', 'Indian/Mayotte'), (422, 'ZA', 'Africa/Johannesburg'), (423, 'ZM', 'Africa/Lusaka'),
  (424, 'ZW', 'Africa/Harare');

UPDATE `users`
SET `country` = ELT(1 + FLOOR(RAND() * 14),
                    'GB', 'US', 'AU', 'AU', 'DE', 'FR', 'CN', 'CA', 'BR',
                    'TO', 'AU', 'GB', 'US', 'CA'
);


ALTER TABLE `users`
  ADD IF NOT EXISTS `language` VARCHAR (32) NULL DEFAULT 'en_AU'
AFTER `country`;

drop table if exists `languages`;
CREATE TABLE IF NOT EXISTS `ajg_todo`.`languages` ( `id` INT NOT NULL AUTO_INCREMENT , `code` TEXT NOT NULL , `title` VARCHAR(128) NOT NULL , `country` VARCHAR(128) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

INSERT INTO languages (`code`, `title`,`country`) VALUES ('af','Afrikaans','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('af_ZA','Afrikaans (South Africa)','ZA');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('ar','Arabic','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('ar_AE','Arabic (U.A.E.)','AE');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('ar_BH','Arabic (Bahrain)','BH');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('ar_DZ','Arabic (Algeria)','DZ');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('ar_EG','Arabic (Egypt)','EG');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('ar_IQ','Arabic (Iraq)','IQ');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('ar_JO','Arabic (Jordan)','JO');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('ar_KW','Arabic (Kuwait)','KW');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('ar_LB','Arabic (Lebanon)','LB');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('ar_LY','Arabic (Libya)','LY');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('ar_MA','Arabic (Morocco)','MA');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('ar_OM','Arabic (Oman)','OM');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('ar_QA','Arabic (Qatar)','QA');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('ar_SA','Arabic (Saudi Arabia)','SA');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('ar_SY','Arabic (Syria)','SY');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('ar_TN','Arabic (Tunisia)','TN');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('ar_YE','Arabic (Yemen)','YE');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('az','Azeri (Latin)','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('az_AZ','Azeri (Latin) (Azerbaijan)','AZ');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('az_AZ','Azeri (Cyrillic) (Azerbaijan)','AZ');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('be','Belarusian','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('be_BY','Belarusian (Belarus)','BY');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('bg','Bulgarian','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('bg_BG','Bulgarian (Bulgaria)','BG');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('bs_BA','Bosnian (Bosnia and Herzegovina)','BA');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('ca','Catalan','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('ca_ES','Catalan (Spain)','ES');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('cs','Czech','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('cs_CZ','Czech (Czech Republic)','CZ');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('cy','Welsh','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('cy_GB','Welsh (United Kingdom)','GB');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('da','Danish','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('da_DK','Danish (Denmark)','DK');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('de','German','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('de_AT','German (Austria)','AT');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('de_CH','German (Switzerland)','CH');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('de_DE','German (Germany)','DE');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('de_LI','German (Liechtenstein)','LI');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('de_LU','German (Luxembourg)','LU');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('dv','Divehi','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('dv_MV','Divehi (Maldives)','MV');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('el','Greek','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('el_GR','Greek (Greece)','GR');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('en','English','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('en_AU','English (Australia)','AU');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('en_BZ','English (Belize)','BZ');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('en_CA','English (Canada)','CA');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('en_CB','English (Caribbean)','CB');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('en_GB','English (United Kingdom)','GB');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('en_IE','English (Ireland)','IE');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('en_JM','English (Jamaica)','JM');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('en_NZ','English (New Zealand)','NZ');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('en_PH','English (Republic of the Philippines)','PH');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('en_TT','English (Trinidad and Tobago)','TT');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('en_US','English (United States)','US');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('en_ZA','English (South Africa)','ZA');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('en_ZW','English (Zimbabwe)','ZW');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('eo','Esperanto','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('es','Spanish','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('es_AR','Spanish (Argentina)','AR');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('es_BO','Spanish (Bolivia)','BO');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('es_CL','Spanish (Chile)','CL');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('es_CO','Spanish (Colombia)','CO');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('es_CR','Spanish (Costa Rica)','CR');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('es_DO','Spanish (Dominican Republic)','DO');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('es_EC','Spanish (Ecuador)','EC');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('es_ES','Spanish (Castilian)','ES');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('es_ES','Spanish (Spain)','ES');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('es_GT','Spanish (Guatemala)','GT');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('es_HN','Spanish (Honduras)','HN');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('es_MX','Spanish (Mexico)','MX');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('es_NI','Spanish (Nicaragua)','NI');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('es_PA','Spanish (Panama)','PA');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('es_PE','Spanish (Peru)','PE');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('es_PR','Spanish (Puerto Rico)','PR');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('es_PY','Spanish (Paraguay)','PY');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('es_SV','Spanish (El Salvador)','SV');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('es_UY','Spanish (Uruguay)','UY');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('es_VE','Spanish (Venezuela)','VE');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('et','Estonian','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('et_EE','Estonian (Estonia)','EE');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('eu','Basque','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('eu_ES','Basque (Spain)','ES');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('fa','Farsi','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('fa_IR','Farsi (Iran)','IR');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('fi','Finnish','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('fi_FI','Finnish (Finland)','FI');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('fo','Faroese','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('fo_FO','Faroese (Faroe Islands)','FO');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('fr','French','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('fr_BE','French (Belgium)','BE');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('fr_CA','French (Canada)','CA');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('fr_CH','French (Switzerland)','CH');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('fr_FR','French (France)','FR');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('fr_LU','French (Luxembourg)','LU');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('fr_MC','French (Principality of Monaco)','MC');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('gl','Galician','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('gl_ES','Galician (Spain)','ES');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('gu','Gujarati','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('gu_IN','Gujarati (India)','IN');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('he','Hebrew','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('he_IL','Hebrew (Israel)','IL');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('hi','Hindi','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('hi_IN','Hindi (India)','IN');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('hr','Croatian','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('hr_BA','Croatian (Bosnia and Herzegovina)','BA');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('hr_HR','Croatian (Croatia)','HR');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('hu','Hungarian','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('hu_HU','Hungarian (Hungary)','HU');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('hy','Armenian','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('hy_AM','Armenian (Armenia)','AM');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('id','Indonesian','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('id_ID','Indonesian (Indonesia)','ID');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('is','Icelandic','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('is_IS','Icelandic (Iceland)','IS');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('it','Italian','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('it_CH','Italian (Switzerland)','CH');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('it_IT','Italian (Italy)','IT');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('ja','Japanese','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('ja_JP','Japanese (Japan)','JP');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('ka','Georgian','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('ka_GE','Georgian (Georgia)','GE');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('kk','Kazakh','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('kk_KZ','Kazakh (Kazakhstan)','KZ');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('kn','Kannada','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('kn_IN','Kannada (India)','IN');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('ko','Korean','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('ko_KR','Korean (Korea)','KR');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('kok','Konkani','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('kok_IN','Konkani (India)','IN');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('ky','Kyrgyz','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('ky_KG','Kyrgyz (Kyrgyzstan)','KG');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('lt','Lithuanian','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('lt_LT','Lithuanian (Lithuania)','LT');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('lv','Latvian','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('lv_LV','Latvian (Latvia)','LV');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('mi','Maori','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('mi_NZ','Maori (New Zealand)','NZ');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('mk','FYRO Macedonian','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('mk_MK','FYRO Macedonian (Former Yugoslav Republic of Macedonia)','MK');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('mn','Mongolian','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('mn_MN','Mongolian (Mongolia)','MN');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('mr','Marathi','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('mr_IN','Marathi (India)','IN');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('ms','Malay','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('ms_BN','Malay (Brunei Darussalam)','BN');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('ms_MY','Malay (Malaysia)','MY');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('mt','Maltese','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('mt_MT','Maltese (Malta)','MT');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('nb','Norwegian (Bokm?l)','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('nb_NO','Norwegian (Bokm?l) (Norway)','NO');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('nl','Dutch','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('nl_BE','Dutch (Belgium)','BE');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('nl_NL','Dutch (Netherlands)','NL');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('nn_NO','Norwegian (Nynorsk) (Norway)','NO');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('ns','Northern Sotho','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('ns_ZA','Northern Sotho (South Africa)','ZA');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('pa','Punjabi','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('pa_IN','Punjabi (India)','IN');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('pl','Polish','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('pl_PL','Polish (Poland)','PL');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('ps','Pashto','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('ps_AR','Pashto (Afghanistan)','AR');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('pt','Portuguese','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('pt_BR','Portuguese (Brazil)','BR');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('pt_PT','Portuguese (Portugal)','PT');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('qu','Quechua','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('qu_BO','Quechua (Bolivia)','BO');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('qu_EC','Quechua (Ecuador)','EC');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('qu_PE','Quechua (Peru)','PE');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('ro','Romanian','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('ro_RO','Romanian (Romania)','RO');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('ru','Russian','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('ru_RU','Russian (Russia)','RU');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('sa','Sanskrit','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('sa_IN','Sanskrit (India)','IN');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('se','Sami (Northern)','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('se_FI','Sami (Northern) (Finland)','FI');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('se_FI','Sami (Skolt) (Finland)','FI');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('se_FI','Sami (Inari) (Finland)','FI');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('se_NO','Sami (Northern) (Norway)','NO');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('se_NO','Sami (Lule) (Norway)','NO');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('se_NO','Sami (Southern) (Norway)','NO');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('se_SE','Sami (Northern) (Sweden)','SE');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('se_SE','Sami (Lule) (Sweden)','SE');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('se_SE','Sami (Southern) (Sweden)','SE');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('sk','Slovak','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('sk_SK','Slovak (Slovakia)','SK');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('sl','Slovenian','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('sl_SI','Slovenian (Slovenia)','SI');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('sq','Albanian','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('sq_AL','Albanian (Albania)','AL');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('sr_BA','Serbian (Latin) (Bosnia and Herzegovina)','BA');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('sr_BA','Serbian (Cyrillic) (Bosnia and Herzegovina)','BA');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('sr_SP','Serbian (Latin) (Serbia and Montenegro)','SP');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('sr_SP','Serbian (Cyrillic) (Serbia and Montenegro)','SP');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('sv','Swedish','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('sv_FI','Swedish (Finland)','FI');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('sv_SE','Swedish (Sweden)','SE');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('sw','Swahili','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('sw_KE','Swahili (Kenya)','KE');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('syr','Syriac','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('syr_SY','Syriac (Syria)','SY');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('ta','Tamil','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('ta_IN','Tamil (India)','IN');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('te','Telugu','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('te_IN','Telugu (India)','IN');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('th','Thai','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('th_TH','Thai (Thailand)','TH');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('tl','Tagalog','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('tl_PH','Tagalog (Philippines)','PH');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('tn','Tswana','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('tn_ZA','Tswana (South Africa)','ZA');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('tr','Turkish','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('tr_TR','Turkish (Turkey)','TR');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('tt','Tatar','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('tt_RU','Tatar (Russia)','RU');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('ts','Tsonga','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('uk','Ukrainian','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('uk_UA','Ukrainian (Ukraine)','UA');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('ur','Urdu','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('ur_PK','Urdu (Islamic Republic of Pakistan)','PK');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('uz','Uzbek (Latin)','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('uz_UZ','Uzbek (Latin) (Uzbekistan)','UZ');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('uz_UZ','Uzbek (Cyrillic) (Uzbekistan)','UZ');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('vi','Vietnamese','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('vi_VN','Vietnamese (Viet Nam)','VN');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('xh','Xhosa','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('xh_ZA','Xhosa (South Africa)','ZA');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('zh','Chinese','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('zh_CN','Chinese (S)','CN');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('zh_HK','Chinese (Hong Kong)','HK');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('zh_MO','Chinese (Macau)','MO');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('zh_SG','Chinese (Singapore)','SG');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('zh_TW','Chinese (T)','TW');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('zu','Zulu','');
INSERT INTO languages (`code`, `title`,`country`) VALUES ('zu_ZA','Zulu (South Africa)','ZA');


ALTER TABLE `users`
  ADD IF NOT EXISTS `hashed` boolean NOT NULL DEFAULT FALSE
AFTER `passwd`;
