<?php
/**
 * Update ToDos
 *
 * This page is used to edit a user's todos. It is for the logged in user
 * only. Admin need a special page for themselves.
 *
 * @File     :  /users-todos-update.php
 * @Project  :  phpToDo
 * @Author   :  Adrian Gould <Adrian.Gould@polytechnic.wa.edu.au>
 * @Date     :  24/11/2016
 * @Version  :  1.0
 * @Copyright:  Copyright 2016 TAFE OpenSource,
 * Released under the Creative Commons Share Alike license
 *
 * History:
 *
 * v 1.0 24/11/2016
 * Initial version
 */

if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}

require_once __DIR__ . "/vendor/autoload.php";

$title = "Update | ToDos | User";

use TAFEOpenSource\Users;
use TAFEOpenSource\Url;
use TAFEOpenSource\ToDoStatuses;
use TAFEOpenSource\ToDos;
use \Respect\Validation\Validator as validator;
use \Carbon\Carbon;

$users = new Users();
$url = new Url();
$root = $url->getUriNoScript();

$todos = new ToDos();
$toDoStatuses = new ToDoStatuses();
$listOfStatuses = $toDoStatuses->getAllToDoStatuses();

if (empty($users->isUserLoggedIn())) {
    session_destroy();
    $_SESSION['userSession'] = false;
    $users->redirect('index.php');
}

// do all the standard setup for the session/page request
$user = $users->getUserById($users->getLoggedInUserId());
$id = $user->id;
$username = $user->username;
$given = $user->givenname;
$last = $user->lastname;
$loginTime = $user->date_login;

// initialise the variables usd on page
$ruleErrors = [];
$tdTitle = '';
$tdDescription = '';
$tdPriority = '';
$tdDateDue = '';
$tdStatus = '';

if (!isset($_POST)) {
    $users->redirect("{$root}users-todos.php");
} else {
    if (!empty($_POST['btnCancelSaveToDo'])) {
        $users->redirect("{$root}users-todos.php");
    } else {
        $toDoID = $_POST['hidToDoID'];

// validate the posted ID is actually a valid one and by the user who is
// logged in, by attempting to get the to do from the table
        $toDoToEdit = $todos->getToDoByUserandID($id, $toDoID);

// if the $toDoToEdit is false then the details were not valid, redirect to
// the users todos list page.
        if (!$toDoToEdit) {
            $users->redirect("{$root}users-todos.php");
        } else {

// get the data from the table
            $tdTitle = $toDoToEdit->title;
            $tdDescription = $toDoToEdit->description;
            $tdPriority = $toDoToEdit->priority;
            $tdDateDue = $toDoToEdit->date_due;
            $tdStatus = $toDoToEdit->status;


// Convert the date from the database into the date format used by the Date
// field / date picker.
            if (false !== strpos($tdDateDue, ":")) {
                $dateFormat = 'Y-m-d H:i:s';
                $toFormat = 'd/m/Y';
            } else {
                $dateFormat = 'Y-m-d';
                $toFormat = 'd/m/Y';
            }
            $tdDateDue = Carbon::createFromFormat($dateFormat, $tdDateDue)
                ->format($toFormat);

// check to see if the form posted and that the submit button was pressed
            if (isset($_POST['btnSaveToDo'])) {

                // get the data from the form fields:
                $tdTitle = $_POST["txtTitle"];
                $tdDescription = $_POST["txtDescription"];
                $tdPriority = $_POST["selPriority"];
                $tdDateDue = $_POST["txtDateDue"];
                $tdStatus = $_POST["selStatus"];

                // define the validator rules
                $rules['txtTitle'] = validator::notEmpty()
                    ->length(5, null)
                    ->setName('Title');
                $rules['txtDescription'] = validator::length(null, 255)
                    ->setName('Description');
                $rules['selPriority'] = validator::intVal()
                    ->between(1, 5)
                    ->setName('Priority');
                $rules['txtDateDue'] = validator::date('d/m/Y')
                    ->setName('Date Due');
                $rules['selStatus'] = validator::notEmpty()
                    ->length(2, 2)
                    ->setName('Status');

                // Check the fields were correctly entered
                try {
                    foreach ($rules as $rule => $validator) {
                        try {
                            $validator->assert($_POST[$rule], $rule);
                        } catch (\Respect\Validation\Exceptions\NestedValidationException $ex) {
                            $ruleErrors[$rule] = ['danger' => $ex->getMessages()];
                        }
                    }

                    // if all fields entered correctly then add to ToDos table
                    if (count($ruleErrors) < 1) {

                        // id = user id of whom is logged in
                        $todos->updateToDo($id, $toDoID, $tdTitle, $tdDescription,
                            $tdPriority, $tdStatus, $tdDateDue);

                        $users->redirect("{$root}users-todos.php");
                    }

                } catch (\Respect\Validation\Exceptions\NestedValidationException $ex) {
                    $ruleErrors[] = $ex->getFullMessage();
                }

            } // end if btnSave clicked

        } // end if toDoEdit was retrieved
    } // end if cancel button pressed

} // end if posted
include_once __DIR__ . "/site-header.php";

?>
    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                User Dashboard
                <small>ToDos</small>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="user-home.php"> Home</a>
                </li>
                <li class="active">
                    <i class="fa fa-list"></i>
                    <a href="users-todos.php"> ToDos</a>
                </li>
                <li class="active">
                    <i class="fa fa-edit"></i> Edit
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->

    <div class="col-xs-12">
        <div class="row">
            <?php
            foreach ($ruleErrors as $errors) {
                foreach ($errors as $errorType => $errorItems) {
                    ?>
                    <div class="col-xs-12 col-sm-6">
                        <div
                                class="alert alert-<?= $errorType; ?> alert-dismissable"
                                role="alert">
                            <button type="button" class="close"
                                    data-dismiss="alert"
                                    aria-label="Close"><span
                                        aria-hidden="true">&times;</span>
                            </button> <?php
                            foreach ($errorItems as $error) {
                                echo "<p>" . $error . "</p>";
                            }
                            ?>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>
        </div>

        <form class="form-horizontal" method="post"
              action="<?= $root; ?>users-todos-update.php">
            <input type="hidden" name="hidToDoID"
                   value="<?= $toDoID; ?>"/>
            <div class="form-group">
                <div class="col-sm-3">
                    <label class="control-label"
                           for="title">Title
                        <i class="fa fa-asterisk text-warning"></i>
                        <span class="sr-only">(Required)</span>
                    </label>
                </div>
                <div class="col-sm-6">
                    <input class="form-control" type="text"
                           id="title"
                           name="txtTitle"
                           placeholder="Title (required)"
                           maxlength="64"
                           minlength="5"
                           autofocus=""
                           value="<?= $tdTitle; ?>">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-3">
                    <label class="control-label"
                           for="description">Description
                    </label>
                </div>
                <div class="col-sm-6">
                    <textarea class="form-control"
                              id="description"
                              name="txtDescription"
                              placeholder="Description (may be blank)"
                              maxlength="2048"
                    ><?= htmlspecialchars($tdDescription); ?></textarea>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-3">
                    <label class="control-label"
                           for="priority">Priority
                        <i class="fa fa-asterisk text-warning"></i>
                        <span class="sr-only">(Required)</span>
                    </label>
                </div>
                <div class="col-sm-6">
                    <select class="form-control"
                            name="selPriority"
                            id="priority"
                            value="<?= $tdPriority; ?>">
                        <option value="1">Low</option>
                        <option value="2">Medium - Low</option>
                        <option value="3">Medium</option>
                        <option value="4">Medium - High</option>
                        <option value="5">High</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-3">
                    <label class="control-label"
                           for="todoStatuses">Status
                        <i class="fa fa-asterisk text-warning"></i>
                        <span class="sr-only">(Required)</span>
                    </label>
                </div>
                <div class="col-sm-6">
                    <select class="form-control"
                            name="selStatus"
                            id="todoStatuses"
                            value="<?= $tdStatus; ?>">
                        <?php
                        if ($listOfStatuses) {
                            foreach ($listOfStatuses as $aStatus) { ?>
                                <option value="<?= $aStatus->id; ?>"
                                    <?= ($aStatus->id == $tdStatus ? 'selected' : '') ?>>
                                    <?= $aStatus->title; ?>
                                </option>
                            <?php } // end for each
                        } // end if
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-3">
                    <label class="control-label" for="dateDue">
                        Date Due
                        <i class="fa fa-asterisk text-warning"></i>
                        <span
                                class="sr-only">(Required)</span><?= $tdDateDue; ?>
                    </label>
                </div>
                <div class="col-sm-6">
                    <div class="input-group">
                        <input type="text" class="form-control"
                               id="dateDue"
                               name="txtDateDue"
                               placeholder="Date Due (dd/mm/yyyy)"
                               value="<?= $tdDateDue; ?>">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar-o"></i>
                        </div>
                    </div>

                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-6 col-sm-offset-3">
                    <button class="btn btn-success" type="submit"
                            name="btnSaveToDo" id="btnSaveToDo"
                            value="Save">
                        <i class="fa fa-check"></i>
                        Save
                    </button>

                    <button class="btn btn-warning" type="submit"
                            name="btnCancelSaveToDo"
                            id="btnCancelSaveToDo"
                            value="Cancel">
                        <i class="fa fa-remove"></i>
                        Cancel
                    </button>

                </div>

            </div>

        </form>

    </div>

<?php

include_once __DIR__ . "/site-footer.php";
