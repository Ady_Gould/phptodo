<?php
/**
 * Demo Classes using Login, Register, Forgotten Password
 *
 * The admin-header.php file is included on every Administrator access page so
 * providing an consistent look and feel to the site. This is repeated for the
 * admin-footer.php file.
 *
 * @File     :  /admin-header.php
 * @Project  :  phpToDo
 * @Author   :  Adrian Gould <Adrian.Gould@polytechnic.wa.edu.au>
 * @Date     :  26/11/2016
 * @Version  :  1.4
 * @Copyright:  Copyright 2016 TAFE OpenSource,
 * Released under the Creative Commons Share Alike license
 *
 * History:
 *
 * v 1.4 27/11/2016
 * added automatic logout for inactive users using new date-activity field
 * that is updated each time a page is accessed. This will also be added to
 * the sample admin pages.
 *
 * v 1.3 23/11/2016
 * Added the DatePicker and Modernizr components to support browsers who do
 * not fully support the HTML5 date input.
 *
 * v 1.2 13/11/2016
 * Included the session start (for login etc) and the autoloading
 * into the file to ensure sessions are started and the required
 * class autoloading may occur.
 *
 * v 1.1 11/11/2016
 * Added title extension to allow the user to pass a title to the page
 *
 * v 1.0 09/11/2016
 * Initial version
 */

if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}
require_once __DIR__ . "/vendor/autoload.php";

use Carbon\Carbon;
use TAFEOpenSource\Url;
use TAFEOpenSource\Users;


$userHeader = new Users();
$url        = new Url();
$root       = $url->getUriNoScript();

if ( ! $userHeader->isUserLoggedIn(7200)) {
    $userHeader->redirect($root);
} else {
    $theUser  = $userHeader->getUserById($userHeader->getLoggedInUserId());
    $uGiven   = $theUser->givenname;
    $uLast    = $theUser->lastname;
    $fullName = $uGiven . ' ' . $uLast;
    $uName    = $theUser->username;
    $uPhoto   = $root . $userHeader->getPhoto($userHeader->getLoggedInUserId());
    $login    = $theUser->date_login;

    //Must add nesbot/carbon via composer
    $loginTimeAgo = Carbon::now('Australia/Perth')
                          ->diffForHumans(
                              Carbon::createFromFormat(
                                  'Y-m-d H:i:s', $login, 'Australia/Perth')
                              , true);
}
?>
    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport"
              content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>
            <?php
            echo (isset($title) && ! empty($title) ? $title . ' | ' : '') . 'phpToDo-v2';
            ?>
        </title>

        <link rel="stylesheet"
              href="<?= $root; ?>assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet"
              href="<?= $root; ?>assets/css/Features-Boxed.css">
        <link rel="stylesheet"
              href="<?= $root; ?>assets/css/flags.css">
        <link rel="stylesheet"
              href="<?= $root; ?>assets/css/Footer-Dark-Fixed.css">
        <link rel="stylesheet"
              href="<?= $root; ?>assets/css/Header-Fixed-Top.css">
        <link rel="stylesheet"
              href="<?= $root; ?>assets/fonts/font-awesome.min.css">
        <link rel="stylesheet"
              href="<?= $root; ?>assets/css/styles.css">

        <link rel="stylesheet"
              href="<?= $root; ?>assets/css/sb-admin.css">

        <!-- Morris Charts CSS -->
        <link rel="stylesheet"
              href="<?= $root; ?>assets/css/morris.css">

        <link rel="stylesheet"
              href="<?= $root; ?>assets/css/bootstrap-datetimepicker.css">

        <link rel="icon" type="image/png"
              href="<?= $root; ?>assets/img/favicon/favicon-32w.png"
              sizes="32x32" />
        <link rel="icon" type="image/png"
              href="<?= $root; ?>assets/img/favicon/favicon-16w.png"
              sizes="16x16" />

        <script
            src="<?= $root; ?>assets/js/vendor/modernizr-custom.js"></script>
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!-- We are loding from a local copy for development online/offline  -->
        <!--[if lt IE 9]>
        <script src="<?= $root;?>assets/js/vendor/html5shiv.min.js"></script>
        <script src="<?= $root;?>assets/js/vendor/respond.min.js"></script>
        <![endif]-->
    </head>

<body>
<div id="wrapper">

    <nav class="navbar navbar-inverse navbar-fixed-top navbar-site">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand navbar-link" href="<?= $root; ?>"><img
                        src="<?= $root; ?>assets/img/favicon/favicon-57w.png"
                        class="navbar-logo">phpToDo</a>
                <button class="navbar-toggle collapsed"
                        data-toggle="collapse" data-target="#navcol-1"><span
                        class="sr-only">Toggle navigation</span><span
                        class="icon-bar"></span><span
                        class="icon-bar"></span><span
                        class="icon-bar"></span></button>
            </div>
            <div class="collapse navbar-collapse" id="navcol-1">
                <ul class="nav navbar-nav navbar-left">

                    <li><a href="admin-home.php">
                            <i class="fa fa-wrench"></i>
                            Admin
                        </a></li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown"
                           aria-expanded="false" href="#">
                            About
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li role="presentation">
                                <a href="<?= $root; ?>company.php">Company</a>
                            </li>
                            <li role="presentation">
                                <a href="<?= $root; ?>team.php">Team</a>
                            </li>
                            <li role="presentation">
                                <a href="<?= $root; ?>careers.php">Careers</a>
                            </li>
                            <li role="presentation">
                                <a href="<?= $root; ?>contact.php">Contact</a>
                            </li>
                        </ul>
                    </li>
                </ul>

                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle"
                           data-toggle="dropdown">
                            <i class="fa fa-user"></i>
                            <?= (isset($fullName) && ! empty($fullName) ? $fullName : ' Log-in/Sign Up'); ?>
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <?php if ( ! $userHeader->isUserLoggedIn()) { ?>
                                <li role="presentation">
                                    <a href="<?= $root; ?>user-login.php">
                                        <i class="fa fa-sign-in text-success"></i>
                                        Log-in
                                    </a>
                                </li>
                                <li role="presentation">
                                    <a href="<?= $root; ?>user-registration.php">
                                        <i class="fa fa-keyboard-o text-primary"></i>
                                        Register
                                    </a>
                                </li>
                            <?php } else { ?>
                            <li class="dropdown-header">
                                Online for <?= $loginTimeAgo ?>
                            </li>
                            <li class="divider"></li>
                            <li role="presentation">
                                <a href="<?= $root; ?>users-todos.php">
                                    <i class="fa fa-fw fa-list"></i>
                                    My ToDos
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="<?= $root; ?>user-home.php">
                                    <i class="fa fa-fw fa-user"></i>
                                    My Profile
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="#">
                                    <i class="fa fa-fw fa-envelope"></i>
                                    My Inbox
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="#">
                                    <i class="fa fa-fw fa-gear"></i>
                                    My Settings
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li role="presentation">
                                <a href="<?= $root; ?>user-logout.php">
                                    <i class="fa fa-fw fa-sign-out text-danger"></i>
                                    Logout
                                </a>
                            </li>

                        </ul>
                        <?php } ?>
                    </li>
                </ul>

            </div>
        </div>

        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <li class="active">
                    <a href="<?= $root; ?>admin-home.php">
                        <i class="fa fa-fw fa-dashboard"></i>
                        Dashboard
                    </a>
                </li>
                <li>
                    <a href="<?= $root; ?>admin-users.php">
                        <i class="fa fa-fw fa-user"></i>
                        Users
                    </a>
                </li>
                <li>
                    <a href="<?= $root; ?>admin-todos.php">
                        <i class="fa fa-fw fa-list"></i>
                        ToDos
                    </a>
                </li>
                <li>
                    <a href="<?= $root; ?>admin-tags.php">
                        <i class="fa fa-fw fa-tags"></i>
                        Tags
                    </a>
                </li>
                <li>
                    <a href="javascript:;"
                       data-toggle="collapse"
                       data-target="#general">
                        <i class="fa fa-fw fa-adjust"></i>
                        General
                        <i class="fa fa-fw fa-caret-down"></i>
                    </a>
                    <ul id="general" class="collapse">
                        <li>
                            <a href="#">User Roles</a>
                        </li>
                        <li>
                            <a href="#">User Statuses</a>
                        </li>
                        <li>
                            <a href="#">To Do Statuses</a>
                        </li>
                        <li>
                            <a href="admin-user-avatars.php">User Avatars</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:;"
                       data-toggle="collapse"
                       data-target="#settings">
                        <i class="fa fa-fw fa-wrench"></i>
                        Settings
                        <i class="fa fa-fw fa-caret-down"></i>
                    </a>
                    <ul id="settings" class="collapse">
                        <li>
                            <a href="#">Dropdown Item</a>
                        </li>
                        <li>
                            <a href="#">Dropdown Item</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>

    <div class="wrapper">
        <div id="page-wrapper">
            <div class="container-fluid">

<?php
