<?php
/**
 * Demo Classes using Login, Register, Forgotten Password
 *
 * Longer description, may be more than one line,
 * usually broken into lines of 80 characters or less.
 *
 * @File     :       /user-verify.php
 * @Project  :    phpToDo
 * @Author   :     Adrian Gould <Adrian.Gould@polytechnic.wa.edu.au>
 * @Date     :       09/11/2016 2:11 PM
 * @Version  :    1.0
 * @Copyright:  Copyright 2016 TAFE OpenSource,
 * Released under the Creative Commons Share Alike license
 *
 * History:
 *
 * v 1.0 09/11/2016
 * Initial version
 */

require_once __DIR__ . "/vendor/autoload.php";

if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}

$title = "Verify Account";

use TAFEOpenSource\Users;
use TAFEOpenSource\Url;

$user = new Users();

if (empty($_GET['id']) && empty($_GET['code'])) {
    $user->redirect('index.php');
}

include_once __DIR__ . "/site-header.php";

if (isset($_GET['id']) && isset($_GET['code'])) {

    // Decode the sent "id" to validate user with sent token.
    $id = base64_decode($_GET['id']);
    $code = $_GET['code'];

    $statusActive = "A";
    $statusWaiting = "W";

    $row = $user->getUserById($id);

    if ($row) {
        if ($row->userstatus == $statusWaiting) {
            $status = $statusActive;
            $user->setStatus($id, $status);

            $eLevel = 'success';
            $msg = "
                <h4>Account Verification Success</h4>
                <p>Your account is now activated. Please use Login to
                 use the account.</p>
                <p><a href='user-login.php' class='btn btn-success'>Login to 
                access your account</a>.</p>
          ";
        } else {
            $eLevel = 'info';
            $msg = "
                <h4>Account Already Activated</h4>
                <p>Your account was already activated. You are 
                now able to sign in to use your account and get 
                your ToDos organised!</p>
                 <p>
                    <a href='user-login.php' class='btn btn-default'>Sign 
                    into your account</a>.
                </p>
          ";
        }
    } else {
        $eLevel = 'danger';
        $msg = "
            <h4>Account Error</h4>
            <p>No Account Found. Please register with us so you are 
            able to organise your ToDos.</p>
            <p><a href='user-registration.php' class='btn btn-default'>Register
             with us</a></p>
         ";
    }
}

?>
<?php if (isset($msg)) {
    echo "<div class='alert alert-{$eLevel}'><button class='close' data-dismiss='alert'>&times;</button>";
    echo $msg;
    echo "</div>";
} ?>

<?php
include_once __DIR__ . "/site-footer.php";


