<?php
/**
 * Demo Classes using Login, Register, Forgotten Password
 *
 * Home page that is displayed to the user on successful login.
 * At the moment it is just the user's profile in brief. Details such
 * as number of ToDos could be added later.
 *
 * @File     :  /user-home.php
 * @Project  :  phpToDo
 * @Author   :  Adrian Gould <Adrian.Gould@polytechnic.wa.edu.au>
 * @Date     :  09/11/2016 2:11 PM
 * @Version  :  1.0
 * @Copyright:  Copyright 2016 TAFE OpenSource,
 * Released under the Creative Commons Share Alike license
 *
 * History:
 *
 * v 1.0 09/11/2016
 * Initial version
 */

require_once __DIR__ . "/vendor/autoload.php";

if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}

$title = "Home";

use TAFEOpenSource\Users;
use TAFEOpenSource\Url;
use TAFEOpenSource\UserRoles;
use TAFEOpenSource\UserStatuses;

$user_home = new Users();
$roles = new UserRoles();
$statuses = new UserStatuses();


if (empty($user_home->isUserLoggedIn())) {
    session_destroy();
    $_SESSION['userSession'] = false;
    $user_home->redirect('index.php');
}

if (empty($_GET['userID'])) {
    $user_home->redirect('admin-users-list.php');
}

$adminuser = $user_home->getUserById($_SESSION['userSession']);
$adminID=$adminuser->id;
$adminUsername = $adminuser->username;
$adminGiven = $adminuser->givenname;
$adminLast = $adminuser->lastname;
$adminRole = $roles->getRoleByID($adminuser->userrole);
$adminStatus = $statuses->getUserStatusByID($adminuser->userstatus);

$userID = $_GET['userID'];
$user = $user_home->getUserById($userID);
$id=$user->id;
$username = $user->username;
$given = $user->givenname;
$last = $user->lastname;
$userRole = $roles->getRoleByID($user->userrole);
$userStatus = $statuses->getUserStatusByID($user->userstatus);


include_once __DIR__ . "/admin-header.php";

?>
    <div class="col-md-3 col-sm-3 col-sm-offset-0 col-xs-3 hidden-xs">
        <img class="img-circle img-responsive img-responsive"
             src="<?= $user_home->getPhoto($id) ?>"
             alt="Picture of <?= $given; ?> <?= $last; ?>">
    </div>
    <div
        class="col-md-8 col-md-offset-0 col-sm-8 col-sm-offset-0 col-xs-12">
        <h1>User Profile</h1>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Property</th>
                    <th>Details</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Name</td>
                    <td><?= $given; ?> <?= $last; ?></td>
                </tr>
                <tr>
                    <td>Username</td>
                    <td><?= $username; ?></td>
                </tr>
                <tr>
                    <td>eMail</td>
                    <td><?= $user->email; ?></td>
                </tr>
                <tr>
                    <td>Role</td>
                    <td>
                        <?= $userRole->title; ?>
                    </td>
                </tr>
                <tr>
                    <td>Status</td>
                    <td><?= $userStatus->title; ?></td>
                </tr>
                <tr>
                    <td>Last Login</td>
                    <td><?= $user->date_login; ?></td>
                </tr>
                <tr>
                    <td>Member Since</td>
                    <td><?= $user->date_added; ?></td>
                </tr>
                </tbody>
                <tfoot>
                <tr>
                    <td></td>
                    <td>
                        <div class="btn-group" role="group">
                            <button class="btn btn-primary"
                                    type="button"><i
                                    class="glyphicon glyphicon-pencil"></i>
                            </button>
                            <button class="btn btn-info" type="button">
                                <i
                                    class="glyphicon glyphicon-ok"></i>
                            </button>
                            <button class="btn btn-warning"
                                    type="button"><i
                                    class="glyphicon glyphicon-stop"></i>
                            </button>
                            <button class="btn btn-danger"
                                    type="button"><i
                                    class="glyphicon glyphicon-remove"></i>
                            </button>
                        </div>
                    </td>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
    </div>

<?php
include_once __DIR__ . "/admin-footer.php";
