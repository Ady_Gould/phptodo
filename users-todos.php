<?php
/**
 * Demo Classes using Login, Register, Forgotten Password
 *
 * Home page that is displayed to the user on successful login.
 * At the moment it is just the user's profile in brief. Details such
 * as number of ToDos could be added later.
 *
 * @File     :  /users-todos.php
 * @Project  :  phpToDo
 * @Author   :  Adrian Gould <Adrian.Gould@polytechnic.wa.edu.au>
 * @Date     :  23/11/2016
 * @Version  :  1.1
 * @Copyright:  Copyright 2016 TAFE OpenSource,
 * Released under the Creative Commons Share Alike license
 *
 * History:
 *
 * v 1.1 23/11/2016
 * changed the search form from the _GET to _POST
 * this helps to hide the search from the likes of Google ;-)
 *
 * v 1.0 09/11/2016
 * Initial version
 */

if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}

require_once __DIR__ . "/vendor/autoload.php";

$title = "ToDos";

use TAFEOpenSource\ToDos;
use TAFEOpenSource\Users;
use Doctrine\Common\Inflector as Inflector;

$user_todos = new Users();
$todos_list = new ToDos();
$inflect    = new Inflector\Inflector();

if (empty($user_todos->isUserLoggedIn())) {
    session_destroy();
    $_SESSION[ 'userSession' ] = false;
    $user_todos->redirect('index.php');
} // end if user is logged in

$sessionUserID = $user_todos->getLoggedInUserId();
$user          = $user_todos->getUserById($sessionUserID);

$id       = $user->id;
$username = $user->username;
$given    = $user->givenname;
$last     = $user->lastname;

$findThis = '';
if (!empty($_POST[ 'searchToDo' ])) {
    $findThis = $_POST[ 'searchToDo' ];
}

$todos = $todos_list->getAllToDosByUser($id, $findThis, 'date_due');

include_once __DIR__ . "/site-header.php";

?>
    <!-- Page Heading -->
    <div class="row">
        <div class="col-xs-12">
            <h1 class="page-header">
                User Dashboard
                <small>ToDos</small>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-users"></i>
                    <a href="user-home.php"> User</a>
                </li>
                <li class="active">
                    <i class="fa fa-list"></i>
                    ToDos
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->

    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <ul class="list-inline">
                            <li>
                                <i class="fa fa-square text-primary"></i>
                                In Progress
                            </li>
                            <li>
                                <i class="fa fa-check-square text-success"></i>
                                Completed
                            </li>
                            <li>
                                <i class="fa fa-exclamation-triangle text-warning"></i>
                                Waiting
                            </li>
                            <li>
                                <i class="fa fa-remove text-danger"></i>
                                Deleted
                            </li>
                            <li>
                                <i class="fa fa-clock-o text-info"></i>
                                Waiting to Start
                            </li>
                            <li>
                                <i class="fa fa-pause text-muted"></i>
                                On Hold
                            </li>
                            <li>
                                <i class="fa fa-question"></i>
                                Unknown
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6">

                <form action="users-todos.php"
                      method="post"
                      name="searchToDos">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <span><i
                                        class="fa fa-search"></i></span>
                            </div>
                            <input class="form-control" type="search"
                                   name="searchToDo"
                                   placeholder="Find..."
                                   value="<?= $findThis; ?>">
                            <div class="input-group-btn">
                                <input class="btn btn-primary"
                                       name="searchToDoButton"
                                       type="submit" value="Go!" />

                            </div>
                        </div>
                    </div>
                </form>

            </div>
            <div class="col-xs-4 text-right">
                <a class="btn btn-primary" href="users-todos-add.php">
                    <i class="fa fa-plus"></i> New ToDo</a>
            </div>
            <div class="col-xs-2 text-right">
                <form action="users-todos-clear-deleted.php"
                      method="post"
                      name="clearDeleted">
                    <button class="btn btn-default"
                            name="btnClearDeleted"
                            id="btnClearDeleted">
                        <i class="fa fa-eraser"></i>
                        Erase Deleted
                    </button>
                </form>
            </div>
        </div>
        <div class="table-responsive">
            <table
                class="table table-bordered table-striped table-hover">
                <thead class="bg-info">
                <tr>
                    <th>Title/Description</th>
                    <th>Pri</th>
                    <th>Due Date</th>
                    <th>Status</th>
                    <th colspan="2">Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                if ($todos != false) {
                    foreach ($todos as $todo) {
                        ?>
                        <tr>
                            <td>
                                <?= $todo->title ?><br>
                                <span class="small text-muted"><?=
                                    $todo->description ?></span>
                            </td>
                            <td><?= $todo->priority ?></td>
                            <td><?= $todo->date_due ?></td>
                            <td><?= (($todo->status == 'IP') ?
                                    '<i class="fa fa-square text-primary"></i>' : '') ?>
                                <?= (($todo->status == 'CO') ?
                                    '<i class="fa fa-check-square text-success"></i>' : '') ?>
                                <?= (($todo->status == 'WO') ?
                                    '<i class="fa fa-exclamation-triangle text-warning"></i>' : '') ?>
                                <?= (($todo->status == 'DE') ?
                                    '<i class="fa fa-remove text-danger"></i>' : '') ?>
                                <?= (($todo->status == 'WC') ?
                                    '<i class="fa fa-clock-o text-info"></i>' : '') ?>
                                <?= (($todo->status == 'HO') ?
                                    '<i class="fa fa-pause text-muted"></i>' : '') ?>
                                <?= (($todo->status == 'XX') ?
                                    '<i class="fa fa-question"></i>' : '') ?>
                            </td>

                            <td>
                                <form
                                    action="<?= $root; ?>users-todos-update.php"
                                    method="post"
                                    name="updateToDos">
                                    <button
                                        class="btn btn-sm btn-primary"
                                        type="submit"
                                        value="Edit"
                                        name="btnToDoEdit">Edit
                                    </button>
                                    <input type="hidden"
                                           name="hidToDoID"
                                           value="<?= ($todo->id); ?>" />
                                </form>
                            </td>

                            <td>
                                <form
                                    action="<?= $root; ?>users-todos-delete.php"
                                    method="post"
                                    name="deleteToDos">
                                    <button
                                        class="btn btn-sm btn-danger"
                                        type="submit"
                                        value="Destroy!"
                                        id="btnD<?= ($todo->id); ?>"
                                        name="btnToDoDelete">
                                        Delete
                                    </button>
                                    <input type="hidden"
                                           name="hidDeleteToDoID"
                                           value="<?= ($todo->id); ?>" />
                                </form>
                            </td>
                        </tr>
                        <?php
                    } // end foreach
                } // end if $todos
                ?>
                </tbody>
                <tfoot class="bg-info">
                <tr>
                    <td colspan="6">Total
                                    ToDos: <?= count($todos) ?></td>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>

<?php
include_once __DIR__ . "/site-footer.php";
