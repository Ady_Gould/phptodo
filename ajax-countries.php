<?php
/**
 * Short Description (1 Line)
 *
 * Longer description, may be more than one line,
 * usually broken into lines of 80 characters or less.
 *
 * @File:       /ajax-countries.php
 * @Project:    phpToDo
 * @Author:     goulda <goulda@EMAIL.ADDRESS>
 * @Date:       30/11/2016 12:46 PM
 * @Version:    RELEASE.MINOR.BUGFIX (eg 2.5.11)
 * @Copyright:  Copyright 2016 TAFE OpenSource,
 * Released under the Creative Commons Share Alike license
 *
 * History:
 *
 * v 1.0 30/11/2016
 * Initial version
 */


require_once __DIR__ . "/vendor/autoload.php";

use TAFEOpenSource\Database;
use TAFEOpenSource\TimeZones;
use TAFEOpenSource\Url;
use Carbon\Carbon;

$url = new Url();
$root = $url->getUriNoScript();
$tz = new TimeZones();

$currentTime = Carbon::now('Australia/Perth')->format('H:i');
$currentDate = Carbon::now('Australia/Perth')->formatLocalized('%A %d %B %Y');

if (isset($_POST["selCountry"]) && !empty($_POST["selCountry"])) {
    //Get all state data
    $countryCode = $_POST['selCountry'];
    $currentTZ = $_POST['hidTZ'];
    $zones = $tz->getTimezonesInCountry($countryCode);
    $region ='';

    if ($zones) {
        echo '<option value="">Select Timezone</option>';
        foreach ($zones as $aZone) {

            $selected = '';
            if (strstr($aZone['timezone'], $currentTZ)) {
                $selected = 'selected';
            }

            if($aZone['region']!==$region) {
                if (!empty($region)) {
                    echo "</optgroup>";
                }
                $region=$aZone['region'];
                echo "<optgroup label='{$aZone['region']}'>";

            }

            echo "<option value='{$aZone['timezone']}' {$selected}>{$aZone['timezone']} [{$aZone['offset']}] ({$aZone['currently']})</option>";
        }
    } else {
        echo '<option value="">Country not available</option>';
    }
}

