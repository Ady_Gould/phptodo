<?php
/**
 * Add ToDos
 *
 * This page is used to ADD todos to a user's list. It is for the logged
 * in user only. Admin users will need a special page for adding todos for
 * anyuser besides themselves.
 *
 * @File     :  /users-todos-add.php
 * @Project  :  phpToDo
 * @Author   :  Adrian Gould <Adrian.Gould@polytechnic.wa.edu.au>
 * @Date     :  24/11/2016
 * @Version  :  1.0
 * @Copyright:  Copyright 2016 TAFE OpenSource,
 * Released under the Creative Commons Share Alike license
 *
 * History:
 *
 * v 1.0 24/11/2016
 * Initial version
 */

if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}

require_once __DIR__ . "/vendor/autoload.php";

$title = "Add | ToDos | User";

use TAFEOpenSource\Users;
use TAFEOpenSource\Url;
use TAFEOpenSource\UserRoles;
use TAFEOpenSource\UserStatuses;
use TAFEOpenSource\ToDos;
use \Respect\Validation\Validator as validator;

$users    = new Users();
$roles    = new UserRoles();
$statuses = new UserStatuses();
$todos    = new ToDos();
$url      = new Url();
$root     = $url->getUriNoScript();

if (empty($users->isUserLoggedIn())) {
    session_destroy();
    $_SESSION['userSession'] = false;
    $users->redirect('index.php');
}

$user       = $users->getUserById($users->getLoggedInUserId());
$id         = $user->id;
$username   = $user->username;
$given      = $user->givenname;
$last       = $user->lastname;
$userRole   = $roles->getRoleByID($user->userrole);
$userStatus = $statuses->getUserStatusByID($user->userstatus);
$loginTime  = $user->date_login;

$ruleErrors    = [];
$tdTitle       = '';
$tdDescription = '';
$tdPriority    = '';
$tdDateDue     = '';

// check to see if the form posted and that the submit button was pressed
if (isset($_POST) && ! empty($_POST['btnSaveToDo'])) {

    // define the validator rules
    $rules['txtTitle']       = validator::notEmpty()
                                        ->length(5, null)
                                        ->setName('Title');
    $rules['txtDescription'] = validator::length(null, 255)
                                        ->setName('Description');
    $rules['selPriority']    = validator::intVal()
                                        ->between(1, 5)
                                        ->setName('Priority');
    $rules['txtDateDue']     = validator::date('d/m/Y')
                                        ->setName('Date Due');


    // Check the fields were correctly entered
    try {
        foreach ($rules as $rule => $validator) {
            try {
                $validator->assert($_POST[$rule], $rule);
            } catch (\Respect\Validation\Exceptions\NestedValidationException $ex) {
                $ruleErrors[$rule] = ['danger' => $ex->getMessages()];
            }
        }

        // get the data from the form fields:
        $tdTitle       = $_POST['txtTitle'];
        $tdDescription = $_POST['txtDescription'];
        $tdPriority    = $_POST['selPriority'];
        $tdDateDue     = $_POST['txtDateDue'];

        // if all fields entered correctly then add to ToDos table
        if (count($ruleErrors) < 1) {

            $tdStatus = 'WC'; // waiting to commence/start

            // id = user id of whom is logged in
            $todos->addToDo($id, $tdTitle, $tdDescription, $tdPriority,
                $tdStatus, $tdDateDue);

            $users->redirect("{$root}users-todos.php");
        }

    } catch (\Respect\Validation\Exceptions\NestedValidationException $ex) {
        $ruleErrors[] = $ex->getFullMessage();
    }

    // If the fields not correctly entered then go back to the form and
    // display the errors
}


include_once __DIR__ . "/site-header.php";

?>
    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                User Dashboard
                <small>ToDos</small>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="user-home.php"> Home</a>
                </li>
                <li class="active">
                    <i class="fa fa-list"></i>
                    <a href="users-todos.php"> ToDos</a>
                </li>
                <li class="active">
                    <i class="fa fa-plus"></i> Add
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->
    <div class="col-xs-12">

        <div class="row">
            <?php
            foreach ($ruleErrors as $errors) {
                foreach ($errors as $errorType => $errorItems) {
                    ?>
                    <div class="col-xs-12 col-sm-6">
                        <div
                            class="alert alert-<?= $errorType; ?> alert-dismissable"
                            role="alert">
                            <button type="button" class="close"
                                    data-dismiss="alert"
                                    aria-label="Close"><span
                                    aria-hidden="true">&times;</span>
                            </button> <?php
                            foreach ($errorItems as $error) {
                                echo "<p>" . $error . "</p>";
                            }
                            ?>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>
        </div>

        <form class="form-horizontal" method="post">
            <div class="form-group">
                <div class="col-sm-3">
                    <label class="control-label"
                           for="title">Title
                        <i class="fa fa-asterisk text-warning"></i>
                        <span class="sr-only">(Required)</span>
                    </label>
                </div>
                <div class="col-sm-6">
                    <input class="form-control" type="text"
                           id="title"
                           name="txtTitle"
                           placeholder="Title (required)"
                           maxlength="32"
                           minlength="5"
                           autofocus=""
                           value="<?= $tdTitle; ?>">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-3">
                    <label class="control-label"
                           for="description">Description
                    </label>
                </div>
                <div class="col-sm-6">
                    <textarea
                        class="form-control"
                        id="description"
                        name="txtDescription"
                        placeholder="Description (may be blank)"
                        maxlength="255"
                        value="<?= $tdDescription; ?>"></textarea>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-3">
                    <label class="control-label"
                           for="priority">Priority
                        <i class="fa fa-asterisk text-warning"></i>
                        <span class="sr-only">(Required)</span>
                    </label>
                </div>
                <div class="col-sm-6">
                    <select class="form-control"
                            name="selPriority"
                            id="priority"
                            value="<?= $tdPriority; ?>>
                        <option value=" 1">Low</option>
                    <option value="2">Medium - Low</option>
                    <option value="3" selected>Medium</option>
                    <option value="4">Medium - High</option>
                    <option value="5">High</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-3">
                    <label class="control-label" for="dateDue">
                        Date Due
                        <i class="fa fa-asterisk text-warning"></i>
                        <span class="sr-only">(Required)</span></label>
                </div>
                <div class="col-sm-6">
                    <div class="input-group">
                        <input type="text" class="form-control"
                               id="dateDue"
                               name="txtDateDue"
                               placeholder="Date Due (dd/mm/yyyy)"
                               value="<?= $tdDateDue; ?>">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar-o"></i>
                        </div>
                    </div>

                </div>
            </div>
            <div class="form-group">

                <div class="col-sm-6 col-sm-offset-3">
                    <button class="btn btn-success" type="submit"
                            name="btnSaveToDo" id="btnSaveToDo"
                            value="Save">
                        <i class="fa fa-check"></i>
                        Save
                    </button>

                    <a class="btn btn-warning" type="submit"
                            name="btnCancelSaveToDo" id="btnCancelSaveToDo"
                            href="<?=$root;?>users-todos.php">
                        <i class="fa fa-remove"></i>
                        Cancel
                    </a>

                    <button class="btn btn-default" type="reset"
                            name="btnReset" id="btnReset"
                            value="Reset">
                        <i class="fa fa-recycle"></i>
                        Clear Form
                    </button>
                </div>
            </div>

        </form>

    </div>

<?php

include_once __DIR__ . "/site-footer.php";
