<?php
/**
 * Description: A brief description of the file. This is left for you to do.
 *
 * @Project  :    phpToDo
 * @File     :    /tafeopensource/src/Database.php
 * @Project  :    phpToDo
 * @Author   :     Adrian Gould <Adrian.Gould@polytechnic.wa.edu.au>
 * @Date     :       21/11/2016
 * @Version  :    1.0.1
 * @Copyright:  Copyright 2016 TAFE OpenSource,
 * Released under the Creative Commons Share Alike license
 *
 * History:
 *
 * v 1.0.1 21/11/2016
 * Minor changes, added block comments to show start of particular
 * class sections, and explain items when needed.
 *
 * v 1.0 09/11/2016
 * Initial version
 */

namespace TAFEOpenSource;

use \PDO;
use \PDOException;

class Database
{
    /***********************************************************************
     * Properties
     ***********************************************************************/

    /***********************************************************************
     * Private properties - only available within instances of the class
     ***********************************************************************/
    private $dbType = 'mysql';
    private $dbName = 'ajg_todo';
    private $dbUser = 'ajg_todo';
    private $dbUserPassword = 'SecretPassword';
    private $dbHost = '127.0.0.1';
    private $dbPort = '3306';
    private $dbEncoding = 'UTF8';

    /***********************************************************************
     * Protected properties - readable by instances of the class
     * example only below, not used by this class
     ***********************************************************************/
    protected $protectedProperty = 'BOO!';

    /***********************************************************************
     * Public properties - available and changeable in instances of the class
     ***********************************************************************/
    public $conn;

    /***********************************************************************
     * Methods
     ***********************************************************************/

    /**
     * Creates the database connection that is used by other classes in
     * this namespace/project
     *
     * @return \PDO
     */
    public function dbConnection()
    {
        $this->conn = null;

        try {
            // Construct the connection string for the MariaDB/MySQL
            $dbConnStr = $this->dbType .
                         ":host=" . $this->dbHost .
                         (! empty($this->dbPort) ? ':' . $this->dbPort : '') .
                         ";dbname=" . $this->dbName .
                         ';charset=' . $this->dbEncoding;

            // Set options for the connection that include Exception errors, use
            // of the fetch data in an object and turn off prepared statement
            // emulation
            $opt = [
                PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
                PDO::ATTR_EMULATE_PREPARES   => false,
            ];

            // Try connecting to the MariaDB/MySQL database
            $this->conn = new PDO(
                $dbConnStr,             // The connection string
                $this->dbUser,          // The DBMS username
                $this->dbUserPassword,  // The DBMS user's password
                $opt                    // The options for PDO's connection
            );


        } catch (PDOException $ex) {
            // Oops we got an error when connecting so display the error in a
            // user friendly manner.
            echo '<div class="row"><div class="col-xs-12">';
            echo '<div class="panel panel-danger"><div class="panel-heading">';
            echo '<h1>Oops, we had a problem...</h1>';
            echo '</div> ';
            echo '<div class="panel-body">';
            echo '<p>Error ' . $ex->getCode() . '</p>';
            echo '<p>' . $ex->getMessage() . '</p>';
            echo '</div></div>';
            echo '</div></div>';
            die();
        }

        return $this->conn;
    }


}// end class Database
