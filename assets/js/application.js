/**
 * Demo Classes using Login, Register, Forgotten Password
 *
 * Custom JS for the application
 *
 * @File     :  /assets/js/application.js
 * @Project  :  phpToDo
 * @Author   :  Adrian Gould <Adrian.Gould@polytechnic.wa.edu.au>
 * @Date     :  23/11/2016
 * @Version  :  1.0
 * @Copyright:  Copyright 2016 TAFE OpenSource,
 * Released under the Creative Commons Share Alike license
 *
 * History:
 *
 *
 * v 1.0 09/11/2016
 * Initial version
 */

$(document).ready(function () {
    // If they do not have HTML5 date then provide a datepicker using javascript
    if (!Modernizr.inputtypes.date) {

        $('#dateDue').datetimepicker({
            format: "DD/MM/YYYY",
        });

    }


    $('#selCountry').on('change', function () {
        var countryID = $(this).val();
        var hidTZ = $('#hidTZ').val();

        if (countryID) {

            $.ajax({
                type: 'POST',
                url: 'ajax-countries.php',
                data: 'selCountry=' + countryID+'&hidTZ=' + hidTZ,
                success: function (html) {
                    $('#selTimezone').html(html);
                }
            });
        } else {
            $('#selTimezone').html('<option value="">Select country first</option>');
        }
    });

    $('#selCountry').on('change', function () {
        var countryID = $(this).val();
        var hidLanguage = $("#hidLanguage").val();
        if (countryID && hidLanguage) {
            $.ajax({
                type: 'POST',
                url: 'ajax-languages.php',
                data: 'selCountry=' + countryID + '&hidLanguage=' + hidLanguage,
                success: function (html) {
                    $('#selLanguage').html(html);
                }
            });
        } else {
            $('#selLanguage').html('<option value="">Select country first</option>');
        }
    });


    $('#selCountry').change();

});
