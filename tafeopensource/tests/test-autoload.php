<?php
/**
 * Short Description (1 Line)
 *
 * Longer description, may be more than one line,
 * usually broken into lines of 80 characters or less.
 *
 * @File:       /FOLDERNAME/test-autoload.php
 * @Project:    phpToDo
 * @Author:     goulda <goulda@EMAIL.ADDRESS>
 * @Date:       09/11/2016 12:02 PM
 * @Version:    RELEASE.MINOR.BUGFIX (eg 2.5.11)
 * @Copyright:  Copyright 2016 TAFE OpenSource,
 * Released under the Creative Commons Share Alike license
 *
 * History:
 *
 * v 1.0 09/11/2016
 * Initial version
 */

// Start the session handler
session_start();

require_once __DIR__ . "/../../vendor/autoload.php";

use TAFEOpenSource\Database as DB;
use TAFEOpenSource\Users;
use TAFEOpenSource\UserRoles;

$db = new DB();
debug_dump("Database", $db);

debug_dump("SESSION", $_SESSION);
$_SESSION['SessionKey']='KeyValue';
debug_dump("SESSION", $_SESSION);

$conn = $db->dbConnection();
debug_dump("Connection", $conn);

$users = new Users();
debug_dump("Users", $users);

$roles = new UserRoles;
debug_dump("Roles", $roles);


$listUsers = $users->getAllUsers();
debug_dump("List Users", $listUsers);


$userToFind = "Teresa.Green@somewhere.local"; // 1) by username, 2) by email address
$foundUser = $users->getUser($userToFind);
debug_dump("foundUser: {$userToFind}", $foundUser);

$userToFind = "admin"; // 1) by username, 2) by email address
$foundUser = $users->getUserId($userToFind);
debug_dump("ID of Admin user", $foundUser);

$role = "X"; // User Role code recovery
$foundRole = $roles->getRoleById($role);
debug_dump("User role {$role} is: ", $foundRole);

$role = "S"; // User Role code recovery
$foundRole = $roles->getRoleById($role);
debug_dump("User role {$role} is: ", $foundRole);



function debug_dump($caption = '', $data = [])
{
    echo "<h3>{$caption}</h3>";
    echo "<pre>";
    var_dump($data);
    echo "</pre>";
}

