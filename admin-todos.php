<?php
/**
 * Demo Classes using Login, Register, Forgotten Password
 *
 * Home page that is displayed to the user on successful login.
 * At the moment it is just the user's profile in brief. Details such
 * as number of ToDos could be added later.
 *
 * @File     :  /user-home.php
 * @Project  :  phpToDo
 * @Author   :  Adrian Gould <Adrian.Gould@polytechnic.wa.edu.au>
 * @Date     :  09/11/2016 2:11 PM
 * @Version  :  1.0
 * @Copyright:  Copyright 2016 TAFE OpenSource,
 * Released under the Creative Commons Share Alike license
 *
 * History:
 *
 * v 1.0 09/11/2016
 * Initial version
 */
if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}
require_once __DIR__ . "/vendor/autoload.php";
$title = "ToDos | Admin";

use TAFEOpenSource\Users;
use TAFEOpenSource\Url;
use TAFEOpenSource\UserRoles;
use TAFEOpenSource\UserStatuses;

$user_home = new Users();
$roles     = new UserRoles();
$statuses  = new UserStatuses();


if (empty($user_home->isUserLoggedIn())) {
    session_destroy();
    $_SESSION['userSession'] = false;
    $user_home->redirect('index.php');
}

$user       = $user_home->getUserById($_SESSION['userSession']);
$id         = $user->id;
$username   = $user->username;
$given      = $user->givenname;
$last       = $user->lastname;
$userRole   = $roles->getRoleByID($user->userrole);
$userStatus = $statuses->getUserStatusByID($user->userstatus);

include_once __DIR__ . "/admin-header.php";
?>
    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Admin Dashboard
                <small>ToDos</small>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-dashboard"></i> <a
                        href="assets/index.html">Dashboard</a>
                </li>
                <li class="active">
                    <i class="fa fa-file"></i> ToDos
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->
<?php
include_once __DIR__ . "/admin-footer.php";
