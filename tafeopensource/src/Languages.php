<?php
/**
 * Short Description (1 Line)
 *
 * Longer description, may be more than one line,
 * usually broken into lines of 80 characters or less.
 *
 * @File:       /FOLDERNAME/Languages.php
 * @Project:    phpToDo
 * @Author:     goulda <goulda@EMAIL.ADDRESS>
 * @Date:       30/11/2016 5:00 PM
 * @Version:    RELEASE.MINOR.BUGFIX (eg 2.5.11)
 * @Copyright:  Copyright 2016 TAFE OpenSource,
 * Released under the Creative Commons Share Alike license
 *
 * History:
 *
 * v 1.0 30/11/2016
 * Initial version
 */


namespace TAFEOpenSource;

use \TAFEOpenSource\Database;
use \PDO;
use \PDOException;

class Languages
{    // Define Properties
    private $conn;

    private $code;
    private $title;


    /**
     * Languages constructor.
     *
     * @param null $langCode
     */
    public function __construct($langCode = null)
    {
        $db = new Database();
        $this->conn = $db->dbConnection();
        if (empty($langCode)) {
            $this->code = '';
            $this->title = '';
        } else {
            $this->code = $langCode;
            $this->title = $this->getTitle($this->code);
        }
    } // end __construct

    /**
     * @param $sql
     *
     * @return \PDOStatement
     */
    public function runQuery($sql)
    {
        $stmt = $this->conn->prepare($sql);

        return $stmt;
    }

    public function getLanguage($code = null)
    {
        $sql = "SELECT * FROM languages
                WHERE code LIKE :theCode";
        $stmt = $this->runQuery($sql);
        $stmt->bindParam(':theCode', $code);
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            // Fetch ONE result
            $record = $stmt->fetch();
            return $record;
        }
        return false;
    }

    /**
     * @param null $code
     *
     * @return bool
     */
    public function getTitle($code = null)
    {
        $sql = "SELECT title FROM languages
                WHERE code = :theCode";
        $stmt = $this->runQuery($sql);
        $stmt->bindParam(':theCode', $code);
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            // Fetch ONE result
            $record = $stmt->fetch();
            return $record->title;
        }
        // if the getRoleById did not find a role then return a false result
        return false;
    }

    public function getLanguages()
    {
        try {
            $sql  = "SELECT * FROM languages"; //
            $stmt = $this->runQuery($sql);
            $stmt->execute();
            $records = $stmt->fetchAll();
            if ($records) {
                return $records;
            } else {
                return false;
            }
        }
        catch(\Exception $ex){
            var_dump($ex);
            die();
        }
    }

    public function getLanguagesForCountry($countryCode = '')
    {
        try {
            $sql  = "SELECT * FROM languages WHERE country = :code"; //
            $stmt = $this->runQuery($sql);
            //$stmt->execute();
            $stmt->execute(['country' => $countryCode]);
            $records = $stmt->fetchAll();
            if ($records) {
                return $records;
            } else {
                return false;
            }
        }
        catch(\Exception $ex){
            var_dump($ex);
            die();
        }
    }
}
