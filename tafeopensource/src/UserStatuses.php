<?php
/**
 * Description: A brief description of the file.
 *
 * @Project:    ${PROJECT}
 * @File   :       /UserStatuses.php
 *
 * @Author :     adrian <email@addre.ss>
 * Author URL:  http://URI_Of_The_Plugin_Author
 * @Date   :       12/11/2016
 * @Version:    1.0
 *
 * License:     A "Slug" license name e.g. GPL2
 *
 * History:
 *
 * v1.0     2016/11/12
 *          Initial version details here.
 *          New versions are inserted BEFORE the intial version, thus listing
 *          in descending order.
 */

namespace TAFEOpenSource;
require_once(__DIR__ . '/../../vendor/phpmailer/phpmailer/PHPMailerAutoload.php');
use \TAFEOpenSource\Database;
use \PDO;
use \PDOException;

class UserStatuses
{

    // Define Properties
    private $conn;

    public function __construct()
    {
        $db         = new Database();
        $this->conn = $db->dbConnection();
    } // end __construct

    public function runQuery($sql)
    {
        $stmt = $this->conn->prepare($sql);
        return $stmt;
    }

    public function getUserStatusByID($statusId)
    {
        $sql = "SELECT * FROM userstatuses
                WHERE id = :theStatusId";
        $stmt = $this->runQuery($sql);
        $stmt->bindParam(':theStatusId', $statusId);
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            // Fetch ONE result
            $record = $stmt->fetch(PDO::FETCH_OBJ);

            return $record;
        }

        // if the getStatusById did not find a status then return a
        // false result
        return false;
    } // end getStatusById

}
