<?php
/**
 * Demo Classes using Login, Register, Forgotten Password
 *
 * Home page that is displayed to the user on successful login.
 * At the moment it is just the user's profile in brief. Details such
 * as number of ToDos could be added later.
 *
 * @File     :  /user-home.php
 * @Project  :  phpToDo
 * @Author   :  Adrian Gould <Adrian.Gould@polytechnic.wa.edu.au>
 * @Date     :  09/11/2016 2:11 PM
 * @Version  :  1.0
 * @Copyright:  Copyright 2016 TAFE OpenSource,
 * Released under the Creative Commons Share Alike license
 *
 * History:
 *
 * v 1.0 09/11/2016
 * Initial version
 */

require_once __DIR__ . "/vendor/autoload.php";

if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}

$title = "Home";

use TAFEOpenSource\Users;
use TAFEOpenSource\Url;
use TAFEOpenSource\UserRoles;
use TAFEOpenSource\UserStatuses;

$usr      = new Users();
$roles    = new UserRoles();
$statuses = new UserStatuses();
$url      = new Url();

$root = $url->getUriNoScript();

if (!$usr->isUserLoggedIn()) {
    session_destroy();
    $_SESSION['userSession'] = false;
    $usr->redirect($root);
}

if ( ! $usr->isAdmin()) {
    $usr->redirect($root . 'user-home.php');
}

$user       = $usr->getUserById($usr->getLoggedInUserId());
$id         = $user->id;
$username   = $user->username;
$given      = $user->givenname;
$last       = $user->lastname;
$userRole   = $roles->getRoleByID($user->userrole);
$userStatus = $statuses->getUserStatusByID($user->userstatus);

$usersList = $usr->getAllUsers();

include_once __DIR__ . "/admin-header.php";

?>
    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Admin Dashboard
                <small>Users</small>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-dashboard"></i> <a
                        href="admin-home.php">Dashboard</a>
                </li>
                <li class="active">
                    <i class="fa fa-users"></i> Users
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-xs-8">
            <div class="panel panel-default">
                <div class="panel-body">
                    <ul class="list-inline">
                        <li>
                            <i class="fa fa-play text-primary"></i>
                            Active
                        </li>
                        <li>
                            <i class="fa fa-lock text-warning"></i>
                            Suspended
                        </li>
                        <li>
                            <i class="fa fa-exclamation-triangle text-warning"></i>
                            Waiting Validation
                        </li>
                        <li>
                            <i class="fa fa-remove text-danger"></i>
                            Removed
                        </li>
                        <li>
                            <i class="fa fa-clock-o text-info"></i>
                            Terminated
                        </li>
                        <li>
                            <i class="fa fa-pause text-muted"></i>
                            -
                        </li>
                        <li>
                            <i class="fa fa-question"></i>
                            Unknown
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-xs-4 text-right">
            Create User:
            <button class="btn btn-primary btn-small">
                <i class="fa fa-plus"></i> NEW
            </button>
        </div>

        <div class="table-responsive col-xs-12">
            <table
                class="table table-bordered table-striped table-hover">
                <thead class="bg-primary">
                <tr>
                    <th>OL</th>
                    <th>ID</th>
                    <th>Given</th>
                    <th>Last</th>
                    <th>User</th>
                    <th>eMail</th>
                    <th>Password<br><small>(Part)</small></th>
                    <th>Role</th>
                    <th>Status</th>
                    <th colspan="2">Action</th>
                </tr>
                </thead>
                <tbody class="">
                <?php

                if ($usersList) {
                    foreach ($usersList as $theUser) {
                        $active = $usr->isActive($theUser->id);

                        ?>
                        <tr>
                            <td><i class="fa fa-circle <?= ($active?'text-success':'text-danger') ?>"></i></td>
                            <td><?= $theUser->id ?></td>
                            <td><?= $theUser->givenname ?></td>
                            <td><?= $theUser->lastname ?></td>
                            <td>
                                <a href="admin-user-view-profile.php?userID=<?=$theUser->id;?>">
                                    <?= $theUser->username ?>
                                </a>
                            </td>                            <td><?= $theUser->email ?></td>
                            <td><?= substr($theUser->passwd,0,10) ?></td>
                            <td><?= $theUser->userrole ?></td>
                            <td><?= $theUser->userstatus ?></td>
                            <td>
                                <button class="btn btn-sm btn-primary">
                                    Edit
                                </button>
                            </td>
                            <td>
                                <button class="btn btn-sm btn-danger">
                                    Delete
                                </button>
                            </td>
                        </tr>
                        <?php
                    } // end foreach
                } // end if $todos
                ?>
                </tbody>
                <tfoot class="bg-primary">
                <tr>
                    <td colspan="10">Total Users:
                        <?= count($usersList) ?>
                    </td>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>


    </div>

<?php
include_once __DIR__ . "/admin-footer.php";
