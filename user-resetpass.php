<?php
/**
 * Demo Classes using Login, Register, Forgotten Password
 *
 * Longer description, may be more than one line,
 * usually broken into lines of 80 characters or less.
 *
 * @File:       /user-resetpass.php
 * @Project:    phpToDo
 * @Author:     Adrian Gould <Adrian.Gould@polytechnic.wa.edu.au>
 * @Date:       09/11/2016 2:11 PM
 * @Version:    1.0
 * @Copyright:  Copyright 2016 TAFE OpenSource,
 * Released under the Creative Commons Share Alike license
 *
 * History:
 *
 * v 1.0 09/11/2016
 * Initial version
 */

require_once __DIR__ . "/vendor/autoload.php";

if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}

$title = "Password Reset";

use TAFEOpenSource\Users;
use TAFEOpenSource\Url;

$users = new Users();
$msgLevel = '';

if (empty($_GET['id']) && empty($_GET['code'])) {
    $users->redirect('index.php');
}

if (isset($_GET['id']) && isset($_GET['code'])) {
    $id = base64_decode($_GET['id']);
    $code = $_GET['code'];

    $user = $users->getUserByIdAndToken($id, $code);
    if ($user) {

        if (isset($_POST['btn-reset-pass'])) {
            $pass = $_POST['pass'];
            $cpass = $_POST['confirm-pass'];

            if ($cpass !== $pass) {
                $msgLevel = "warning";
                $msg = "<div class='alert alert-block'>
                        <button class='close' data-dismiss='alert'>&times;</button>
                        <strong>Sorry!</strong>  Password Doesn't match. 
                        </div>";
            } else {
                if ($users->resetPassword($id, $cpass)) {
                    // change status to active after password change
                    $users->setStatus($id, 'A');
                    $msgLevel = "success";
                    $msg = "<div class='alert alert-success'>
                        <button class='close' data-dismiss='alert'>&times;</button>
                        Password Changed.</div>";
                    header("refresh:5;index.php");
                } else {
                    $msgLevel = "warning";
                    $msg = "div class='alert alert-warning'>
                        <button class='close' data-dismiss='alert'>&times;</button>
                        Password change failed.</div>";
                }
            }
        }
    } else {
        $users->redirect('index.php');
    }
} else {
    $users->redirect('index.php');
}

include_once __DIR__ . "/site-header.php";

?>

    <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-push-2">
            <div class='alert alert-success'>
                <button class='close'
                        data-dismiss='alert'>&times;</button>
            <form class="form-signin" method="post">
                <h1>Password Reset</h1>
                <?php if ($msgLevel != 'success') { ?>
                    <p>Hello, <?= $user->givenname; ?></p>
                    <p>You have requested to reset or change your
                        password.</p>
                <?php } ?>



                </div>

                <?php
                if (isset($msg)) {
                    echo $msg;
                }
                ?>
                <input type="password" class="input-block-level"
                       placeholder="New Password" name="pass" required/>
                <input type="password" class="input-block-level"
                       placeholder="Confirm New Password"
                       name="confirm-pass" required/>
                <hr/>
                <button class="btn btn-large btn-primary" type="submit"
                        name="btn-reset-pass">Reset Your Password
                </button>

            </form>
        </div>
    </div>
<?php
include_once __DIR__ . "/site-footer.php";


