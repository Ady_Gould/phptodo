<?php
/**
 * Demo Classes using Login, Register, Forgotten Password
 *
 * Home page that is displayed to the user on successful login.
 * At the moment it is just the user's profile in brief. Details such
 * as number of ToDos could be added later.
 *
 * @File     :  /user-home.php
 * @Project  :  phpToDo
 * @Author   :  Adrian Gould <Adrian.Gould@polytechnic.wa.edu.au>
 * @Date     :  09/11/2016 2:11 PM
 * @Version  :  1.0
 * @Copyright:  Copyright 2016 TAFE OpenSource,
 * Released under the Creative Commons Share Alike license
 *
 * History:
 *
 * v 1.0 09/11/2016
 * Initial version
 */

require_once __DIR__ . "/vendor/autoload.php";

if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}

$title = "Blank | User";

use TAFEOpenSource\Users;
use TAFEOpenSource\Url;
use TAFEOpenSource\UserRoles;
use TAFEOpenSource\UserStatuses;

$user_home = new Users();
$roles     = new UserRoles();
$statuses  = new UserStatuses();


if (empty($user_home->isUserLoggedIn())) {
    session_destroy();
    $_SESSION['userSession'] = false;
    $user_home->redirect('index.php');
}

$user       = $user_home->getUserById($_SESSION['userSession']);
$id         = $user->id;
$username   = $user->username;
$given      = $user->givenname;
$last       = $user->lastname;
$userRole   = $roles->getRoleByID($user->userrole);
$userStatus = $statuses->getUserStatusByID($user->userstatus);
$loginTime  = $user->date_login;

include_once __DIR__ . "/site-header.php";

?>

    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                User Pages
                <small>Subsection Title</small>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-users"></i>
                    <a href="user-home.php"> User</a>
                </li>
                <li class="active">
                    <i class="fa fa-birthday-cake"></i>
                    <a href="user-home.php"> Second Item</a>
                </li>
                <li class="active">
                    <i class="fa fa-automobile"></i> Third Item
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-xs-12">
            <h3>Some random placeholder data</h3>
            <p>Name: <?= $given . ' ' . $last; ?></p>
            <p>Username: <?= $username; ?></p>
            <p>Logged in: <?= $loginTime; ?></p>
        </div>
    </div>
<?php
include_once __DIR__ . "/site-footer.php";
