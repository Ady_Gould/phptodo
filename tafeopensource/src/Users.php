<?php
/**
 * Demo Classes using Login, Register, Forgotten Password
 *
 * Longer description, may be more than one line,
 * usually broken into lines of 80 characters or less.
 *
 * @File     :    /tafeopensource/src//Users.php
 * @Project  :    phpToDo
 * @Author   :    Adrian Gould <Adrian.Gould@polytechnic.wa.edu.au>
 * @Date     :    14/11/2016
 * @Version  :    1.2
 * @Copyright:  Copyright 2016 TAFE OpenSource,
 * Released under the Creative Commons Share Alike license
 *
 * History:
 *
 * v 1.2 14/11/2016
 * Removed duplicate code and used the methods that have been defined
 * for getUserId, getUser and such. This reduces code and makes Class
 * more maintainable.
 *
 * v 1.1 12/11/2016
 * Added getUser and getUserByID methods. getUserByID calls getUser
 * to return a single user based on the (numeric) ID passed.
 *
 * v 1.0 09/11/2016
 * Initial version
 */

namespace TAFEOpenSource;

use Carbon\Carbon;
use PDO;
use PDOException;

require_once __DIR__ . '/../../vendor/phpmailer/phpmailer/PHPMailerAutoload.php';

class Users
{
    // Constants
    /**
     * Amount of time (in seconds) that the user may be inactive before they
     * are automatically logged out. Standard is 600 seconds. May be directly
     * overridden in the isActive method call.
     */
    const USER_INACTIVE_TIME = 3600;

    // Properties
    private $conn;
    private $lastId;

    // Methods
    /**
     * Users constructor.
     */
    public function __construct()
    {

        if (session_status() !== PHP_SESSION_ACTIVE) {
            session_start();
        }

        $db         = new Database();
        $this->conn = $db->dbConnection();

    } // end __construct

    /**
     * @return array|bool
     */
    public function getAllUsers()
    {
        return $this->retrieveAllUsers();
    }

    /**
     * @return array|bool
     */
    public function retrieveAllUsers()
    {
        // create SQL to retrieve all users
        $sql = "SELECT * FROM users";
        // Prepare the SQL statement
        $stmt = $this->runQuery($sql);
        // Execute the statement
        $stmt->execute();
        // Fetch all the results
        $records = $stmt->fetchAll();
        // Return results to caller
        if ($stmt->rowCount() > 0) {
            return $records;
        }

        // send false result if no records found
        return false;
    }

    /**
     * @param $sql
     *
     * @return \PDOStatement
     */
    public function runQuery($sql)
    {
        $stmt = $this->conn->prepare($sql);

        return $stmt;
    }

    /**
     * Check to see if the user is logged in.
     *
     * Logged in state is stored in sessions, with userSession being the ID of the user that is logged in.
     *
     * @return bool
     */
    public function isUserLoggedIn($inactivityTime = self::USER_INACTIVE_TIME)
    {
        if (isset($_SESSION[ 'userSession' ]) && !empty($_SESSION[ 'userSession' ])) {
            // Check to see if the user has been active recently (within last USER_INACTIVE_TIME seconds)
            if ($this->wasActive($inactivityTime)) {
                $this->updateActivity();
            } else {
                return false;
            }

            return true;
        }

        return false;
    }

    /**
     * Activity Check (Was Active in last X seconds)
     *
     * Checks to see if the user has had any activity in the last
     * $inactiveSeconds seconds. The default is set by USER_INACTIVE_TIME
     * and may be overrriden by the the caller, eg. isActive(3600) gives a
     * one hour inactivity time.
     *
     * @param int $inactiveSeconds
     *
     * @return bool
     */
    public function wasActive($inactiveSeconds = self::USER_INACTIVE_TIME)
    {

        // get user's last activity time [new time]
        $theUser = $this->getUserById($this->getLoggedInUserId());
        if ($theUser->date_activity !== null) {
            $activityDateTime = Carbon::createFromFormat(
                'Y-m-d H:i:s',
                $theUser->date_activity,
                'Australia/Perth');
        } else {
            $activityDateTime = Carbon::now('Australia/Perth');
        }
        // get current time
        $currentDateTime = Carbon::now('Australia/Perth');

        // calculate difference in times

        $timeDiff = $activityDateTime->diffInSeconds($currentDateTime);
        // if time difference is > user inactivity time then auto-logout
        // else update activity time
        if ($timeDiff > $inactiveSeconds) {
            $this->logout();

            return false;
        }

        return true;
    }

    /**
     * @param null $userID
     *
     * @return bool|mixed
     */
    public function getUserById($userID = null)
    {
        $sql  = "SELECT * FROM users
                WHERE id = :uid";
        $stmt = $this->runQuery($sql);
        $stmt->bindParam(':uid', $userID);
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            // Fetch ONE result
            $record = $stmt->fetchAll();

            return $record[0];
        }

        // if the getUser did not find a user then return a false result
        return false;
    }

    /**
     * @return bool
     */
    public function getLoggedInUserId()
    {
        if (isset($_SESSION[ 'userSession' ]) && !empty($_SESSION[ 'userSession' ])) {
            return $_SESSION[ 'userSession' ];
        } else {
            return false;
        }

        return false;
    }

    /**
     *
     */
    public function logout()
    {
        $this->updateLogoutTime($this->getLoggedInUserId());
        session_destroy();
        $_SESSION[ 'userSession' ] = false;
    }

    /**
     * @param null $userID
     *
     * @return bool
     */
    public function updateLogoutTime($userID = null)
    {
        $sql  = "UPDATE users SET date_logout=NOW() WHERE id=:uid";
        $stmt = $this->runQuery($sql);
        $stmt->bindParam(':uid', $userID);
        $result = $stmt->execute();

        return $result;
    }

    /**
     * @param null $userID
     *
     * @return bool|\PDOStatement
     */
    public function updateActivity($userID = null)
    {
        if ($userID === null) {
            $userID = $this->getUserbyId($this->getLoggedInUserId())->id;
        }

        try {
            $sql                    = "UPDATE users SET date_activity = NOW() WHERE id = :userid";
            $stmt                   = $this->runQuery($sql);
            $parameters[ 'userid' ] = $userID;
            $stmt->execute($parameters);

            return $stmt;
        } catch (PDOException $ex) {
            echo $ex->getMessage();

            return false;
        }
    }

    public function isActive(
        $userID = 0,
        $inactiveSeconds = self::USER_INACTIVE_TIME
    ) {

        // get user's last activity time [new time]
        $theUser = $this->getUserById($userID);

        if ($theUser->date_activity !== null) {
            $activityDateTime = Carbon::createFromFormat(
                'Y-m-d H:i:s',
                $theUser->date_activity,
                'Australia/Perth');
        } else {
            $activityDateTime = Carbon::now('Australia/Perth')->subSeconds(self::USER_INACTIVE_TIME * 2);
        }
        // get current time
        $currentDateTime = Carbon::now('Australia/Perth');

        // calculate difference in times

        $timeDiff = $activityDateTime->diffInSeconds($currentDateTime);
        // if time difference is > user inactivity time then auto-logout
        // else update activity time
        if ($timeDiff <= $inactiveSeconds) {

            return true;
        }

        return false;
    }

    public function loggedInUserCount()
    {
        $currentDateTime = Carbon::now('Australia/Perth');
        $activeDate      = $currentDateTime->subSeconds(self::USER_INACTIVE_TIME);

        $sql  = "SELECT id, date_activity FROM users WHERE date_activity>=:dateActive";
        $stmt = $this->runQuery($sql);
        $stmt->bindParam(':dateActive', $activeDate);
        $stmt->execute();

        return $stmt->rowCount();
    }

    public function loggedInUsers()
    {
        $currentDateTime = Carbon::now('Australia/Perth');
        $activeDate      = $currentDateTime->subSeconds(self::USER_INACTIVE_TIME);

        $sql  = "SELECT * FROM users WHERE date_activity>=:dateActive";
        $stmt = $this->runQuery($sql);
        $stmt->bindParam(':dateActive', $activeDate);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    /**
     * Log in user - will allow for hashed and unhashed passwords.
     * on login, unhashed passwords will be hashed ready for next login.
     *
     * @param null $userToLogin
     * @param null $userPass
     *
     * @return bool
     */
    public function login($userToLogin = null, $userPass = null)
    {
        // convert the username/email address to lowercase
        $userNameLC = strtolower(trim($userToLogin));
        $userPass = trim ($userPass);

        try {
            $userRow = $this->getUser($userNameLC);
            $userID  = $this->getUserId($userNameLC);

            if ($userID > 0) {
                // check to see if user is Active
                if ($userRow->userstatus == "A") {
                    // verify password correct
                    if ($userRow->hashed) {
                        $passwordVerify = password_verify($userPass, $userRow->passwd);
                    } else {
                        $passwordVerify = $userRow->passwd === $userPass;
                        $this->hashPasswords($userID);
                    }
                    if ($passwordVerify) {

                        $this->updateLoginTime($userID);

                        // log user in
                        $_SESSION[ 'userSession' ] = $userID;
                        $this->updateActivity($userID);

                        return true;
                    } else {
                        // redirect to index with error code
                        $this->redirect("user-login.php?error=L");
                        exit;
                    }
                } else {
                    // redirect to index with indicator of user status
                    $this->redirect("user-login.php?error={$userRow->userstatus}");
                    exit;
                }
            } else {
                // redirect to index with error code
                $this->redirect("user-login.php?error=L");
                exit;
            } // end if rowcount
        } catch (\PDOException $ex) {
            echo $ex->getMessage();
        } // end try catch
    } // end

    /**
     * @param null $userToFind
     *
     * @return bool|mixed
     */
    public function getUser($userToFind = null)
    {
        $sql  = "SELECT * FROM users
                WHERE username = :uName
                OR email = :email";
        $stmt = $this->runQuery($sql);
        $stmt->bindParam(':uName', $userToFind);
        $stmt->bindParam(':email', $userToFind);
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            // Fetch ONE result
            $record = $stmt->fetch();

            return $record;
        }

        // if the getUser did not find a user then return a false result
        return false;
    }

    /**
     * @param null $userToFind
     *
     * @return mixed
     */
    public function getUserId($userToFind = null)
    {
        $record = $this->getUser($userToFind);

        return $record->id;
    }

    /**
     * @param null $userID
     *
     * @return bool
     */
    public function updateLoginTime($userID = null)
    {
        $sql  = "UPDATE users SET date_login=NOW() WHERE id=:uid";
        $stmt = $this->runQuery($sql);
        $stmt->bindParam(':uid', $userID);
        $result = $stmt->execute();

        return $result;
    }

    /**
     * @param $url
     */
    public function redirect($url)
    {
        header("Location: $url");
    }

    /**
     * register user, now hashes password
     *
     * @param $gName
     * @param $lName
     * @param $uName
     * @param $eMail
     * @param $uPass
     * @param $code
     *
     * @return \PDOStatement
     */
    public function register(
        $gName,
        $lName,
        $uName,
        $eMail,
        $uPass,
        $code
    ) {
        try {
            $hashedPW = password_hash(trim($uPass), PASSWORD_DEFAULT);

            $sql = "INSERT INTO users
                    (givenname, lastname, username, email, 
                    passwd, token, userstatus, hashed)
                  VALUES
                    (:given_name, :last_name, :user_name, :user_mail, 
                    :user_pass, :active_code, 'W', true)";

            $stmt = $this->runQuery($sql);
            $stmt->bindparam(":given_name", $gName);
            $stmt->bindparam(":last_name", $lName);
            $stmt->bindparam(":user_name", $uName);
            $stmt->bindparam(":user_mail", $eMail);
            $stmt->bindparam(":user_pass", $hashedPW);
            $stmt->bindparam(":active_code", $code);
            $stmt->execute();

            $this->lastId = $this->getLastID();

            return $stmt;
        } catch (PDOException $ex) {
            echo $ex->getMessage();
            die(0);
        }
    }

    /**
     * @return string
     */
    public function getLastID()
    {

        $stmt = $this->conn->lastInsertId();

        return $stmt;
    }

    /**
     * @param $email
     * @param $message
     * @param $subject
     */
    public function send_mail($email, $message, $subject)
    {
        // create a new instance of the PHPMailer class
        $mail = new \PHPMailer();

        // tell the Mailer that we will use SMTP for sending mail
        $mail->IsSMTP();
        $mail->SMTPDebug = 0;

        // uncomment these lines when talking to gMail
        //$mail->SMTPAuth   = true;
        //$mail->SMTPSecure = "ssl";

        // Change 127.0.0.1 to smtp.gmail.com to use gmMail.
        //Use 127.0.0.1 for papercut testing
        $mail->Host = "127.0.0.1";
        // use port 465 for gMail and port 25 for Papercut
        $mail->Port = 25; //465

        $mail->AddAddress($email);

        // for gMail you need to give your account username and password
        $mail->Username = "yourgmailid@gmail.com";
        $mail->Password = "yourgmailpassword";

        // set the set from to an address responsible for the
        // application.
        $mail->SetFrom('you@yourdomain.com',
            'ToDos Registration Robot');

        // add reply-to with the same details.
        $mail->AddReplyTo("noreply@yourdomain.com", "No Reply Robot");

        // Set the mail up using the details passed to the method
        $mail->Subject = $subject;

        // set the mail message as an HTML based message
        $mail->MsgHTML($message);

        // send the message
        $mail->Send();
    }

    /**
     * @param $userID
     * @param $status
     *
     * @return bool|\PDOStatement
     */
    public function setStatus($userID, $status)
    {
        try {
            $sql = "UPDATE users
             SET userstatus = :newstatus
             WHERE id = :userid";

            $stmt = $this->runQuery($sql);
            $stmt->bindParam(':userid', $userID);
            $stmt->bindParam(':newstatus', $status);
            $stmt->execute();

            return $stmt;
        } catch (PDOException $ex) {
            echo $ex->getMessage();

            return false;
        }
    }

    /**
     * @param $uid
     *
     * @return bool
     */
    public function setValidatedUser($uid)
    {
        // do things

        return false; // default to fail
    }

    /**
     * @param null $id
     * @param null $code
     *
     * @return bool|mixed
     */
    public function getUserByIdAndToken($id = null, $code = null)
    {
        $stmt = $this->runQuery("SELECT * FROM tbl_users WHERE userID=:uid AND tokenCode=:token");
        $stmt->bindParam(':userid', $userID);
        $stmt->bindParam(':newstatus', $status);
        $stmt->execute(array( ":uid" => $id, ":token" => $code ));
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($stmt->rowCount() > 0) {
            return $result;
        }

        return false;
    }

    /**
     * reset password (change) - now hashes the password.
     *
     * @param null $id
     * @param null $password
     *
     * @return bool
     */
    public function resetPassword($id = null, $password = null)
    {
        $password = trim($password);
        //          create password hash
        $hashedPW = password_hash($password, PASSWORD_DEFAULT);

        $sql  = "UPDATE users SET passwd=:upass, hashed=true WHERE id=:uid";
        $stmt = $this->runQuery($sql);
        $stmt->bindParam(':uid', $id);
        $stmt->bindParam(':upass', $hashedPW);
        $result = $stmt->execute();
        if ($result) {
            return true;
        }

        return false;
    }

    /**
     * @param null $userID
     *
     * @return string
     */
    public function getPhoto($userID = null)
    {
        $user = $this->getUserById($userID);

        if ($user) {
            $photoURL = $user->folder . $user->photo;

            return $photoURL;
        }

        return '';
    }

    /**
     * @return bool
     */
    public function isAdmin()
    {
        $user = $this->getUserById($this->getLoggedInUserId());

        if ($user) {
            $admin = $user->userrole == 'A' || $user->userrole == 'S';

            return $admin;
        }

        return false;
    }

    public function updateUserDetails(
        $userID,
        $newFirst,
        $newLast,
        $newUser,
        $newEmail,
        $newCountry,
        $newTimeZone,
        $newLanguage
    ) {
        if ($userID === null) {
            $userID = $this->getUserbyId($this->getLoggedInUserId())->id;
        }

        try {
            $sql        = "UPDATE users 
                    SET 
                      givenname = :givenN,
                      lastname = :lastN,
                      username = :userN,
                      email = :eMail,
                      country = :country,
                      timezone = :timezone,
                      language = :lingo,
                      date_updated = NOW(),
                      date_activity = NOW() 
                    WHERE id = :userID";
            $stmt       = $this->runQuery($sql);
            $parameters = [
                'userID'   => $userID,
                'givenN'   => $newFirst,
                'lastN'    => $newLast,
                'userN'    => $newUser,
                'eMail'    => $newEmail,
                'country'  => $newCountry,
                'timezone' => $newTimeZone,
                'lingo'    => $newLanguage,
            ];
            $stmt->execute($parameters);

            return $stmt;
        } catch (PDOException $ex) {
            echo $ex->getMessage();
            die();

            return false;
        }
    }

    /**
     * Hash passwords if they are not already stored in hashed format.
     *
     * PASSWORD_DEFAULT - Use the bcrypt algorithm (default as of PHP 5.5.0). Note that this constant is designed to
     * change over time as new and stronger algorithms are added to PHP. For that reason, the length of the result from
     * using this identifier can change over time. Therefore, it is recommended to store the result in a database column
     * that can expand beyond 60 characters (255 characters would be a good choice).
     */
    private function hashPasswords($id = null)
    {
        // before you start, add a new column to the database hashed, boolean to indicate password is hashed


            $allUsers = $this->getAllUsers();


        // for each user,
        foreach ($allUsers as $oneUser) {
            if (!empty($id) && $oneUser->id === $id) {
                $hashed = $oneUser->hashed;
                $userID = $oneUser->id;
                $userPass = $oneUser->passwd;

                //      if the password is not hashed, then
                if (!$hashed) {
                    //          create password hash - this is now done in resetPassword
                    //          update user details with new password
                    //          update hashed to true
                    $this->resetPassword($userID, $userPass);
                    //      end if
                } // end if not hashed
            } // end if user id given
            // end foreach
        }
    }

    /***********************************************************************
     * static methods
     ***********************************************************************/

    /**
     * @param        $var
     * @param string $msg
     */
    public static function dump($var, $msg = '')
    {
        if (!empty($msg)) {
            echo '<h4>' . $msg . '</h4>';
        }
        echo "<pre>";
        var_dump($var);
        echo "</pre>";
    }

    /**
     * @param        $var
     * @param string $msg
     */
    public static function dumpAndDie($var, $msg = '')
    {
        if (!empty($msg)) {
            echo '<h4>' . $msg . '</h4>';
        }
        echo "<pre>";
        var_dump($var);
        echo "</pre>";
        die('Died...');
    }

} // end class Users
