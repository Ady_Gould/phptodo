<?php
/**
 * Created by PhpStorm.
 * User: 13046820
 * Date: 10/11/2016
 * Time: 1:33 PM
 */
/**
 * Demo Classes using Login, Register, Forgotten Password
 *
 * Forgotten Password Retrieval
 *
 * @File:       /user-forgotpass.php
 * @Project:    phpToDo
 * @Author:     13046820 <13046820@EMAIL.ADDRESS>
 * @Date:       21/11/2016
 * @Version:    1.0
 * @Copyright:  Copyright 2016 TAFE OpenSource,
 * Released under the Creative Commons Share Alike license
 *
 * History:
 *
 * v 1.0 10/11/2016
 * Initial version
 */

require_once __DIR__ . "/vendor/autoload.php";

if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}

$title = "Forgotten Password";

use TAFEOpenSource\Users;
use TAFEOpenSource\Url;

$user = new Users();
$url = new Url();
$siteURL = $url->getUriNoScript();

if ($user->isUserLoggedIn() != "") {
    $user->redirect('user-home.php');
}

if (isset($_POST['btn-submit'])) {
    $email = $_POST['txtUserOrEMail'];

    $stmt = $user->runQuery("SELECT id FROM users WHERE email=:email LIMIT
     1");
    $stmt->execute(array(":email" => $email));
    $row = $stmt->fetch();
    if ($stmt->rowCount() == 1) {
        $id = base64_encode($row->id);
        $code = md5(uniqid(rand()));

        $stmt = $user->runQuery("UPDATE users SET token=:token WHERE 
        email=:email");
        $stmt->execute(array(":token" => $code, "email" => $email));

        $message = "Hello , $email
            <br /><br />
            Someone sent a request to reset your password. If you did not 
            send the request, just ignore this email.<br /><br />
            If you did make the request, then follow the link below to 
            reset your password:
            <br /><br />
            <a href='{$siteURL}user-resetpass.php?id=$id&code=$code'>
            Click here to reset your password
            </a>
            <br /><br />
            Thank you :)
            <br /><br />
            The phpToDos Team
            ";
        $subject = "Password Reset";

        $user->send_mail($email, $message, $subject);

        $msg = "<div class='alert alert-success'>
     <button class='close' data-dismiss='alert'>&times;</button>
     We've sent an email to $email.
                    Please click on the password reset link in the email to generate new password. 
      </div>";
    } else {
        $msg = "<div class='alert alert-danger'>
     <button class='close' data-dismiss='alert'>&times;</button>
     <strong>Sorry!</strong>  this email not found. 
       </div>";
    }
}

include_once __DIR__ . "/site-header.php";

?>

    <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-push-2">

            <form class="form-horizontal" method="post">
                <h2 class="form-signin-heading">Forgot Password</h2>

                <?php
                if (isset($msg)) {
                    echo $msg;
                } else {
                    ?>
                    <div class='alert alert-info'>
                        <h3>Oops! So you forgot your password</h3>
                        <p>Please enter your email address or username
                            below.</p>
                        <p>We will send an email to your registered
                            address with a link to create a new
                            password.</p>
                    </div>
                    <?php
                }
                ?>


                <div class="form-group">
                    <div class="col-sm-4">
                        <label class="control-label" for="txtUserOrEMail">
                            eMail Address or Username
                        </label>
                    </div>
                    <div class="col-sm-8">
                        <input class="form-control" type="text"
                               name="txtUserOrEMail" required
                               placeholder="eMail or username">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-4">
                        <label class="control-label"> </label>
                    </div>
                    <div class="col-sm-3">
                        <button class="btn btn-primary" type="submit"
                                name="btn-submit">Password
                            Reset
                        </button>
                    </div>

                    <div class="col-sm-3">
                        <a class="btn btn-default"
                           href="user-registration.php">
                            Register / Sign Up
                        </a>
                    </div>

                    <div class="col-sm-1">
                        <a class="btn btn-default" href="user-login.php">Sign
                            In</a>
                    </div>

                </div>
            </form>
        </div>
    </div>

<?php
include_once __DIR__ . "/site-footer.php";

