<?php
/**
 * Demo Classes using Login, Register, Forgotten Password
 *
 * Home page that is displayed to the user on successful login.
 * At the moment it is just the user's profile in brief. Details such
 * as number of ToDos could be added later.
 *
 * @File     :  /user-home.php
 * @Project  :  phpToDo
 * @Author   :  Adrian Gould <Adrian.Gould@polytechnic.wa.edu.au>
 * @Date     :  09/11/2016 2:11 PM
 * @Version  :  1.0
 * @Copyright:  Copyright 2016 TAFE OpenSource,
 * Released under the Creative Commons Share Alike license
 *
 * History:
 *
 * v 1.0 09/11/2016
 * Initial version
 */

require_once __DIR__ . "/vendor/autoload.php";

if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}

$title = "Home";

use TAFEOpenSource\Users;
use TAFEOpenSource\Url;
use TAFEOpenSource\UserRoles;
use TAFEOpenSource\UserStatuses;
use Carbon\Carbon;

$user_home = new Users();
$roles     = new UserRoles();
$statuses  = new UserStatuses();
$language  = new \TAFEOpenSource\Languages();
$date      = new Carbon();

if (empty($user_home->isUserLoggedIn())) {
    session_destroy();
    $_SESSION[ 'userSession' ] = false;
    $user_home->redirect('index.php');
}

$user          = $user_home->getUserById($_SESSION[ 'userSession' ]);
$id            = $user->id;
$username      = $user->username;
$given         = $user->givenname;
$last          = $user->lastname;
$userRole      = $roles->getRoleByID($user->userrole);
$userStatus    = $statuses->getUserStatusByID($user->userstatus);
$userLang      = $user->language;
$languageTitle = $language->getLanguage($userLang)->title;
$login         = $user->date_login;
$languagePart  = mb_strstr($userLang, '_', true);
$timezone      = $user->timezone;
$country       = $user->country;

$fullname = trim(($given > '' ? $given : '') . ' ' . ($last > '' ? $last : ''));

//Must add nesbot/carbon via composer
try {
    setLocale(LC_TIME, $userLang);
    $lastLogin = $date->createFromFormat(
        'Y-m-d H:i:s', $login,
        $timezone);

    $currentTime = $date->now($timezone)->format('H:i');
    $currentDate = $date->now($timezone)->formatLocalized('%A %d %B %Y');
} catch (ErrorException $ex) {
    //$date->setlocale('en_AU');
}


include_once __DIR__ . "/site-header.php";

?>
    <div class="col-md-3 col-sm-3 col-sm-offset-0 col-xs-3 hidden-xs">
        <img class="img-circle img-responsive img-responsive"
             src="<?= $user_home->getPhoto($id) ?>"
             alt="Picture of <?= $fullname; ?>">
    </div>
    <div
        class="col-md-8 col-md-offset-0 col-sm-8 col-sm-offset-0 col-xs-12">
        <h1>User Profile</h1>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Property</th>
                    <th>Details</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Name</td>
                    <td><?= $given; ?> <?= $last; ?></td>
                </tr>
                <tr>
                    <td>Username</td>
                    <td><?= $username; ?></td>
                </tr>
                <tr>
                    <td>eMail</td>
                    <td><?= $user->email; ?></td>
                </tr>
                <tr>
                    <td>Role</td>
                    <td>
                        <?= $userRole->title; ?>
                    </td>
                </tr>
                <tr>
                    <td>Country</td>
                    <td><?= $user->country; ?></td>
                </tr>
                <tr>
                    <td>Language</td>
                    <td><?= $languageTitle; ?> (<?= $user->language; ?>
                                               )
                    </td>
                </tr>
                <tr>
                    <td>Timezone</td>
                    <td><?= $user->timezone; ?></td>
                </tr>
                <tr>
                    <td>Status</td>
                    <td><?= $userStatus->title; ?></td>
                </tr>
                <tr>
                    <td>Member Since</td>
                    <td><?= $user->date_added; ?></td>
                </tr>
                <tr>
                    <td>Last Login</td>
                    <td><?= $lastLogin; ?></td>
                </tr>
                </tbody>
                <tfoot>
                <tr>
                    <td></td>
                    <td>
                        <a class="btn btn-primary"
                           type="button"
                           href="user-profile-edit.php"><i
                                class="glyphicon glyphicon-pencil"></i>
                        </a>
                        <button class="btn btn-info" type="button">
                            <i
                                class="glyphicon glyphicon-ok"></i>
                        </button>
                        <button class="btn btn-warning"
                                type="button"><i
                                class="glyphicon glyphicon-stop"></i>
                        </button>
                        <button class="btn btn-danger"
                                type="button"><i
                                class="glyphicon glyphicon-remove"></i>
                        </button>
                    </td>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
    </div>

<?php
include_once __DIR__ . "/site-footer.php";
