<?php
/**
 * Url Utility Class
 *
 * @File     :       /TAFEOpenSource/src/Url.php
 * @Project  :    phpToDo
 * @Author   :     Adrian Gould <Adrian.Gould@polytechnic.wa.edu.au>
 * @Date     :       10/11/2016
 * @Version  :    1.0
 * @Copyright:  Copyright 2016 TAFE OpenSource,
 *              Released under the Creative Commons Share Alike license
 *
 * History:
 *
 * v 1.0 10/11/2016
 * Initial version
 */

namespace TAFEOpenSource;

class Url
{

    private $protocol;  // the protocol for this URL (eg http)
    private $domain;    // the domain name of the URL (eg www.example.com)
    private $port;      // the port that is used (eg 80, 443, 8080)
    private $folders;   // the folder(s) the URL has (eg /images/users)
    private $script;    // the script being run (eg user-login.php)
    private $query;     // the _GET query variables and values that appear after the script and ? (eg page=1&search=game)

    /**
     * Url constructor.
     *
     * This looks at the current URL (that instantiated the class) and
     * deconstructs the URL into constiutent parts. For example, the
     * URL: https://www.example.com:99/admin/list-users.php?page=4 would
     * be deconstructed into:
     * protocol     https://
     * domain       www.example.com
     * port         99
     * folders      /admin/
     * script       list-users.php
     * query        page=4
     */
    public function __construct()
    {
        $this->protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
        $this->domain = $_SERVER['HTTP_HOST'];
        $this->port = $_SERVER['SERVER_PORT'];

        $this->folders = $this->getCurrentDir($_SERVER['REQUEST_URI']);
        $this->script = pathinfo($_SERVER['REQUEST_URI'],
                PATHINFO_FILENAME)
            . '.'
            . pathinfo($_SERVER['REQUEST_URI'],
                PATHINFO_EXTENSION);
        $this->query = $_SERVER['QUERY_STRING'];
    }

    /**
     * @return string
     *
     * reconstructs the URL without the script
     *
     */
    public function getUriNoScript()
    {
        return $this->getProtocol() .
        $this->getDomain() .
        (!empty($this->getPort()) ? ':' . $this->getPort() : '') .
        $this->getFolders();
    }

    /**
     * @return string
     */
    public function getProtocol(): string
    {
        return $this->protocol;
    }

    /**
     * @return mixed
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * @return mixed
     */
    public function getPort()
    {
        $po = $this->port;
        $pc=$this->getProtocol();
        if (($pc=='http://' && $po=='80') || ($pc='https://' &&
                $po=443)) {
            return '';
        }
        return $this->port;
    }

    /**
     * @return mixed
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * @return string
     */
    public function getFolders(): string
    {
        return $this->folders;
    }

    /**
     * @return string
     */
    public function getScript(): string
    {
        return $this->script;
    }

    /**
     * @param $url
     *
     * @return string
     *
     * http://stackoverflow.com/questions/3429262/get-base-directory-of-current-script
     * Answer provided by mgutt 19/03/2015
     */
    private function getCurrentDir($url)
    {
        // note: anything without a scheme ("example.com", "example.com:80/", etc.) is a folder
        // remove query (protection against "?url=http://example.com/")
        if ($first_query = strpos($url, '?')) {
            $url = substr($url, 0, $first_query);
        }
        // remove fragment (protection against "#http://example.com/")
        if ($first_fragment = strpos($url, '#')) {
            $url = substr($url, 0, $first_fragment);
        }
        // folder only
        $last_slash = strrpos($url, '/');
        if (!$last_slash) {
            return '/';
        }
        // add ending slash to "http://example.com"
        if (($first_colon = strpos($url,
                '://')) !== false && $first_colon + 2 == $last_slash
        ) {
            return $url . '/';
        }

        return substr($url, 0, $last_slash + 1);
    }

}
