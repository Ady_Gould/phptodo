<?php
/**
 * Demo Classes using Login, Register, Forgotten Password
 *
 * ToDos class that interacts with the todos table.
 *
 * @File     :  /tafeopensource/src/ToDos.php
 * @Project  :  phpToDo
 * @Author   :  Adrian Gould <Adrian.Gould@polytechnic.wa.edu.au>
 * @Date     :  24/11/2016
 * @Version  :  1.1
 * @Copyright:  Copyright 2016 TAFE OpenSource,
 * Released under the Creative Commons Share Alike license
 *
 * History:
 *
 * v 1.1 24/11/2016
 * Added the addToDo method, complete with Carbon date reformatting.
 *
 * v 1.0 23/11/2016
 * Initial version
 */

namespace TAFEOpenSource;

use Carbon\Carbon;
use PDOException;


class ToDos
{

    // Properties
    /**
     * @var \PDO
     */
    private $conn;
    /**
     * @var int
     */
    private $lastId;

    // Methods
    /**
     * ToDos constructor.
     */
    public function __construct()
    {
        $db         = new Database();
        $this->conn = $db->dbConnection();
    } // end __construct

    /**
     * @param $sql
     *
     * @return \PDOStatement
     */
    public function runQuery($sql)
    {
        $stmt = $this->conn->prepare($sql);

        return $stmt;
    }// end get all ToDos

    /**
     * @param int    $id
     * @param string $title
     * @param string $description
     * @param int    $priority
     * @param string $status
     * @param string $dateDue
     *
     * @return bool
     */
    public function addToDo(
        $id = 0,
        $title = '-ERROR IN TITLE-',
        $description = '',
        $priority = 0,
        $status = 'XX',
        $dateDue = '00/00/0000'
    ) {
        try {

            // Convert the date from the form into MySQL format
            if (strstr($dateDue, ':')) {
                $dateFormat = 'd/m/Y h:i:s';
            } else {
                $dateFormat = 'd/m/Y';
            }
            $dueDate = Carbon::createFromFormat($dateFormat, $dateDue)
                             ->toDateString();

            $sql = "INSERT INTO todos(
                        user_id, title, description, date_due, priority, 
                        status, date_added, date_updated)
                    VALUES (
                        :uID, :tdTitle, :tdDesc, :tdDue, :tdPri, 
                        :tdStat, NOW(), NOW())";

            $stmt = $this->runQuery($sql);
            $stmt->bindParam(':uID', $id);
            $stmt->bindParam(':tdTitle', $title);
            $stmt->bindParam(':tdDesc', $description);
            $stmt->bindParam(':tdDue', $dueDate);
            $stmt->bindParam(':tdPri', $priority);
            $stmt->bindParam(':tdStat', $status);

            $result = $stmt->execute();

            if ($result === true) {
                return true;
            }

        } catch (PDOException $ex) {
            return false;
        }

        return false;
    } // end add To Do method

    /**
     * @param string $searchFor
     * @param string $orderBy
     *
     * @return array|bool
     */
    public function getAllToDos(
        $searchFor = '',
        $orderBy = ''
    ) {
        // create SQL to retrieve all users
        $sql = "SELECT * FROM todos";
        // add the search term if needed
        if ( ! empty($searchFor)) {
            $sql .= " AND (title LIKE :searchT";
            $sql .= " OR description LIKE :searchD)";
        }
        if ( ! empty($orderBy)) {
            $sql .= " ORDER BY " . $orderBy;
        }
        // Prepare the SQL statement
        $stmt = $this->runQuery($sql);
        // Execute the statement
        $stmt->execute();
        // Fetch all the results
        $records = $stmt->fetchAll();
        // Return results to caller
        if ($stmt->rowCount() > 0) {
            return $records;
        }

        return false;
    }

    /**
     * @param null   $userID
     * @param string $searchFor
     * @param string $orderBy
     *
     * @return array|bool
     */
    public function getAllToDosByUser(
        $userID = null,
        $searchFor = '',
        $orderBy = ''
    ) {
        try {
            $sql = "SELECT * FROM todos
                WHERE user_id = :uid";
            if ( ! empty($searchFor)) {
                $sql .= " AND (title LIKE :searchT";
                $sql .= " OR description LIKE :searchD)";
            }
            if ( ! empty($orderBy)) {
                $sql .= " ORDER BY " . $orderBy;
            }
            $stmt = $this->runQuery($sql);
            $stmt->bindParam(':uid', $userID);
            if ( ! empty($searchFor)) {
                $searchTerm = "%{$searchFor}%";
                $stmt->bindParam(':searchT', $searchTerm);
                $stmt->bindParam(':searchD', $searchTerm);
            }
            $stmt->execute();

            if ($stmt->rowCount() > 0) {
                $records = $stmt->fetchAll();

                return $records;
            }
        } catch (PDOException $ex) {
            var_dump($ex);
            die();
        }

        return false;
    } // end get All the ToDos by A User

    /**
     * @param null   $userID
     * @param string $status
     * @param string $orderBy
     *
     * @return array|bool
     */
    public function getAllToDosByUserWithStatus(
        $userID = null,
        $status = '',
        $orderBy = 'date_updated DESC'
    ) {
        try {
            $sql = "SELECT * FROM todos
                WHERE (user_id = :uid)";
            if ( ! empty($status)) {
                $sql .= " AND (status = :statusID)";
            }
            if ( ! empty($orderBy)) {
                $sql .= " ORDER BY " . $orderBy;
            }
            $stmt = $this->runQuery($sql);

            $parameters = ['uid' => $userID];
            if ( ! empty($status)) {
                $parameters['statusID'] = $status;
            }

            $stmt->execute($parameters);

            if ($stmt->rowCount() > 0) {
                $records = $stmt->fetchAll();

                return $records;
            }
        } catch (PDOException $ex) {
            Users::dumpAndDie($ex);
        }

        return false;
    } // end get All the ToDos by A User

    public function getToDoByUserandID($userID = 0, $toDoID = 0)
    {
        // create SQL to retrieve all users
        $sql = "SELECT * FROM todos WHERE user_id = :uID AND id = :tdID LIMIT 1";

        // Prepare the SQL statement
        $stmt = $this->runQuery($sql);
        $stmt->bindParam(':uID', $userID);
        $stmt->bindParam(':tdID', $toDoID);

        // Execute the statement
        $stmt->execute();

        // Fetch the result
        $record = $stmt->fetch();

        // Return results to caller
        if ($stmt->rowCount() > 0) {
            return $record;
        }

        return false;
    }

    public function deleteToDo($userID, $toDoID)
    {
        $this->updateToDoStatus($userID, $toDoID, 'DE');
    } // end delete To Do method

    /**
     *
     * ADD YOUR OWN DESCRIPTION HERE
     *
     * This method does PRESUME that all the data will be sent, no error
     * checking for missing data is made. No defaults are given for this
     * method.
     *
     * @param int    $id
     * @param string $title
     * @param string $description
     * @param int    $priority
     * @param string $status
     * @param string $dateDue
     *
     * @return bool
     */
    public function updateToDo(
        $userID,
        $toDoID,
        $title,
        $description,
        $priority,
        $status,
        $dateDue
    ) {
        try {

            // Convert the date from the form into MySQL format
            if (strstr($dateDue, ':')) {
                $dateFormat = 'd/m/Y H:i:s';
            } else {
                $dateFormat = 'd/m/Y';
            }
            $dueDate = Carbon::createFromFormat($dateFormat, $dateDue)
                             ->toDateString();

            $sql = "UPDATE todos
                    SET  
                        title           = :tdTitle, 
                        description     = :tdDesc, 
                        date_due        = :tdDue, 
                        priority        = :tdPri, 
                        status          = :tdStat, 
                        date_updated    = NOW()
                    WHERE
                        user_id         = :uID
                        AND
                        id              = :tdID";

            $stmt       = $this->runQuery($sql);
            $parameters = [
                'tdID'    => $toDoID,
                'uID'     => $userID,
                'tdTitle' => $title,
                'tdDesc'  => $description,
                'tdDue'   => $dueDate,
                'tdPri'   => $priority,
                'tdStat'  => $status,
            ];
            $result     = $stmt->execute($parameters);

            if ($result === true) {
                return true;
            }

        } catch (PDOException $ex) {
            // really need to do some error handling here
            Users::dump($ex);

            return false;
        }

        return false;
    } // end update To Do method

    /**
     *
     * ADD YOUR OWN DESCRIPTION HERE
     *
     * This method does PRESUME that all the data will be sent, no error
     * checking for missing data is made. No defaults are given for this
     * method.
     *
     * @param int    $id
     * @param string $title
     * @param string $description
     * @param int    $priority
     * @param string $status
     * @param string $dateDue
     *
     * @return bool
     */
    public function updateToDoStatus($userID, $toDoID, $tdStatus)
    {

        try {

            $sql = "UPDATE todos
                    SET  
                        status       = :tdStat,
                        date_updated = NOW()
                    WHERE
                        user_id      = :uID
                        AND
                        id           = :tdID";

            $stmt       = $this->runQuery($sql);
            $parameters = [
                'tdID'   => $toDoID,
                'uID'    => $userID,
                'tdStat' => $tdStatus,
            ];
            $result     = $stmt->execute($parameters);

            if ($result) {
                return true;
            }

        } catch (PDOException $ex) {
            // really need to do some error handling here
            Users::dump($ex);

            return false;
        }

        return false;
    }

    public function clearDeletedToDos($userID = 0)
    {
        // create SQL to retrieve all users
        $sql = "DELETE FROM todos WHERE user_id = :uID AND status = :tdStatus";

        $delStatus = "DE";

        // Prepare the SQL statement
        $stmt       = $this->runQuery($sql);
        $parameters = [
            'uID'      => $userID,
            'tdStatus' => $delStatus,
        ];

        $result = $stmt->execute($parameters);

        return $result;
    }

} // end class
