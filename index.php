<?php
/**
 * Demo Classes using Login, Register, Forgotten Password
 *
 * The index.php page is the one that gets shown automatically. We will
 * use this for demonstrating the login feature, as well as linking
 * to reset password and other functions.
 *
 * @File     :       /index.php
 * @Project  :    phpToDo
 * @Author   :     Adrian Gould <Adrian.Gould@polytechnic.wa.edu.au>
 * @Date     :       09/11/2016 2:11 PM
 * @Version  :    1.0
 * @Copyright:  Copyright 2016 TAFE OpenSource,
 * Released under the Creative Commons Share Alike license
 *
 * History:
 *
 * v 1.0 09/11/2016
 * Initial version
 */

if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}
require_once __DIR__ . "/vendor/autoload.php";

$title    = "Welcome";

use \TAFEOpenSource\Users;
use \TAFEOpenSource\Url;

include_once "site-header.php";


?>

    <div class="features-boxed">
        <div class="container">
            <div class="intro">
                <h2 class="text-center">Features </h2>
                <p class="text-center">phpToDo, the Open Source ToDo App brings many features usually reserved for pricey alternatives.</p>
            </div>
            <div class="row features">
                <div class="col-md-4 col-sm-6 item">
                    <div class="box"><i class="glyphicon glyphicon-map-marker icon"></i>
                        <h3 class="name">Works everywhere</h3>
                        <p class="description">Aenean tortor est, vulputate quis leo in, vehicula rhoncus lacus. Praesent aliquam in tellus eu.</p><a href="#" class="learn-more">Learn more »</a></div>
                </div>
                <div class="col-md-4 col-sm-6 item">
                    <div class="box"><i class="glyphicon glyphicon-time icon"></i>
                        <h3 class="name">Always available</h3>
                        <p class="description">Aenean tortor est, vulputate quis leo in, vehicula rhoncus lacus. Praesent aliquam in tellus eu.</p><a href="#" class="learn-more">Learn more »</a></div>
                </div>
                <div class="col-md-4 col-sm-6 item">
                    <div class="box"><i class="glyphicon glyphicon-list-alt icon"></i>
                        <h3 class="name">Customizable </h3>
                        <p class="description">Aenean tortor est, vulputate quis leo in, vehicula rhoncus lacus. Praesent aliquam in tellus eu.</p><a href="#" class="learn-more">Learn more »</a></div>
                </div>
                <div class="col-md-4 col-sm-6 item">
                    <div class="box"><i class="glyphicon glyphicon-leaf icon"></i>
                        <h3 class="name">Organic </h3>
                        <p class="description">Aenean tortor est, vulputate quis leo in, vehicula rhoncus lacus. Praesent aliquam in tellus eu.</p><a href="#" class="learn-more">Learn more »</a></div>
                </div>
                <div class="col-md-4 col-sm-6 item">
                    <div class="box"><i class="glyphicon glyphicon-plane icon"></i>
                        <h3 class="name">Fast </h3>
                        <p class="description">Aenean tortor est, vulputate quis leo in, vehicula rhoncus lacus. Praesent aliquam in tellus eu.</p><a href="#" class="learn-more">Learn more »</a></div>
                </div>
                <div class="col-md-4 col-sm-6 item">
                    <div class="box"><i class="glyphicon glyphicon-phone icon"></i>
                        <h3 class="name">Mobile-first</h3>
                        <p class="description">Aenean tortor est, vulputate quis leo in, vehicula rhoncus lacus. Praesent aliquam in tellus eu.</p><a href="#" class="learn-more">Learn more »</a></div>
                </div>
            </div>
        </div>
    </div>

<?php
include_once __DIR__ . "/site-footer.php";

