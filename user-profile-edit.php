<?php
/**
 * Demo Classes using Login, Register, Forgotten Password
 *
 * Home page that is displayed to the user on successful login.
 * At the moment it is just the user's profile in brief. Details such
 * as number of ToDos could be added later.
 *
 * @File     :  /user-home.php
 * @Project  :  phpToDo
 * @Author   :  Adrian Gould <Adrian.Gould@polytechnic.wa.edu.au>
 * @Date     :  09/11/2016 2:11 PM
 * @Version  :  1.0
 * @Copyright:  Copyright 2016 TAFE OpenSource,
 * Released under the Creative Commons Share Alike license
 *
 * History:
 *
 * v 1.0 09/11/2016
 * Initial version
 */

require_once __DIR__ . "/vendor/autoload.php";

if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}

$title = "Home";

use TAFEOpenSource\Users;
use TAFEOpenSource\Url;
use TAFEOpenSource\UserRoles;
use TAFEOpenSource\UserStatuses;
use TAFEOpenSource\TimeZones;
use TAFEOpenSource\Languages;

$user_home = new Users();
$roles = new UserRoles();
$statuses = new UserStatuses();
$tz = new TimeZones();
$languages = new Languages();
$ruleErrors = [];

if (empty($user_home->isUserLoggedIn())) {
    session_destroy();
    $_SESSION['userSession'] = false;
    $user_home->redirect('index.php');
}

$user = $user_home->getUserById($_SESSION['userSession'])[0];
$id = $user->id;
$username = $user->username;
$given = $user->givenname;
$last = $user->lastname;
$eMail = $user->email;
$timezone = $user->timezone;
$country = $user->country;
$language = $user->language;

$userRole = $roles->getRoleByID($user->userrole);
$userStatus = $statuses->getUserStatusByID($user->userstatus);

$countries = $tz->getCountries();

if (isset($_POST) && !empty($_POST)) {
    $newFirst = trim($_POST['txtGiven']);
    $newLast = trim($_POST['txtLast']);
    $newUsername = trim($_POST['txtUser']);
    $newEmail = trim($_POST['txtEMail']);
    $newCountry = trim($_POST['selCountry']);
    $newLanguage = trim($_POST['selLanguage']);
    $newTimeZone = trim($_POST['selTimezone']);
    $userID = $id;

    // DO VALIDATION HERE

    $user_home->updateUserDetails(
        $userID, $newFirst, $newLast, $newUsername,
        $newEmail, $newCountry, $newTimeZone, $newLanguage);

    $user_home->redirect('user-profile.php');
}

include_once __DIR__ . "/site-header.php";

?>
    <div class="row">
        <div class="col-xs-12">
            <h1>Edit Profile</h1>
        </div>
        <div class="col-xs-12">

            <div class="row">
                <?php
                foreach ($ruleErrors as $errors) {
                    foreach ($errors as $errorType => $errorItems) {
                        ?>
                        <div class="col-xs-12 col-sm-6">
                            <div
                                    class="alert alert-<?= $errorType; ?> alert-dismissable"
                                    role="alert">
                                <button type="button" class="close"
                                        data-dismiss="alert"
                                        aria-label="Close"><span
                                            aria-hidden="true">&times;</span>
                                </button> <?php
                                foreach ($errorItems as $error) {
                                    echo "<p>" . $error . "</p>";
                                }
                                ?>
                            </div>
                        </div>
                        <?php
                    }
                }
                ?>
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-3 col-sm-offset-0 col-xs-3 hidden-xs">
                    <img class="img-circle img-responsive img-responsive"
                         src="<?= $user_home->getPhoto($id) ?>"
                         alt="Picture of <?= $user->givenname; ?> <?= $user->lastname; ?>">
                </div>
            </div>

            <form class="form-horizontal" method="post">
                <div class="form-group">
                    <div class="col-sm-3">
                        <label class="control-label"
                               for="gName">Given Name
                            <i class="fa fa-asterisk text-warning"></i>
                            <span class="sr-only">(Required)</span>
                        </label>
                    </div>
                    <div class="col-sm-6">
                        <input class="form-control" type="text"
                               id="gName"
                               name="txtGiven"
                               placeholder="Given Name (required)"
                               maxlength="32"
                               autofocus=""
                               value="<?= $given; ?>">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-3">
                        <label class="control-label"
                               for="gName">Last Name
                            <i class="fa fa-asterisk text-warning"></i>
                            <span class="sr-only">(Required)</span>
                        </label>
                    </div>
                    <div class="col-sm-6">
                        <input class="form-control" type="text"
                               id="lName"
                               name="txtLast"
                               placeholder="Last Name (required)"
                               maxlength="32"
                               autofocus=""
                               value="<?= $last; ?>">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-3">
                        <label class="control-label"
                               for="gName">User Name
                            <i class="fa fa-asterisk text-warning"></i>
                            <span class="sr-only">(Required)</span>
                        </label>
                    </div>
                    <div class="col-sm-6">
                        <input class="form-control" type="text"
                               id="uName"
                               name="txtUser"
                               placeholder="User Name (required)"
                               maxlength="32"
                               autofocus=""
                               value="<?= $username; ?>">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-3">
                        <label class="control-label"
                               for="eMail">eMail Address
                            <i class="fa fa-asterisk text-warning"></i>
                            <span class="sr-only">(Required)</span>
                        </label>
                    </div>
                    <div class="col-sm-6">
                        <input class="form-control" type="email"
                               id="eMail"
                               name="txtEMail"
                               placeholder="eMail Address (required)"
                               maxlength="32"
                               autofocus=""
                               value="<?= $eMail; ?>">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-3">
                        <label class="control-label"
                               for="description">Country
                        </label>
                    </div>
                    <div class="col-sm-6">
                        <select
                                class="form-control"
                                id="selCountry"
                                name="selCountry"
                                placeholder="Country"
                                value="<?= $country; ?>">
                            <?php
                            foreach ($countries as $aCountry) {
                                $selected = '';
                                if ($aCountry->country_code == $country) {
                                    $selected = 'selected';
                                }
                                echo "<option value='{$aCountry->country_code}' {$selected}>{$aCountry->country_name}</option>";
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <input type="hidden" id="hidTZ" name="hidTZ"
                           value="<?= $timezone ?>"/>

                    <div class="col-sm-3">
                        <label class="control-label"
                               for="priority">Timezone
                            <i class="fa fa-asterisk text-warning"></i>
                            <span class="sr-only">(Required)</span>
                        </label>
                    </div>
                    <div class="col-sm-6">
                        <select class="form-control"
                                name="selTimezone"
                                id="selTimezone"
                                value="<?= $timezone; ?>">
                        </select>

                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-3">
                        <label class="control-label"
                               for="priority">Language
                            <i class="fa fa-asterisk text-warning"></i>
                            <span class="sr-only">(Required)</span>
                        </label>
                    </div>
                    <div class="col-sm-6">

                        <input type="hidden" id="hidLanguage" name="hidLanguage"
                               value="<?= $language ?>"/>

                        <select class="form-control"
                                name="selLanguage"
                                id="selLanguage"
                                value="<?= $language; ?>">

                        </select>

                    </div>
                </div>

                <div class="form-group">

                    <div class="col-sm-6 col-sm-offset-3">
                        <button class="btn btn-success" type="submit"
                                name="btnSaveToDo" id="btnSaveToDo"
                                value="Save">
                            <i class="fa fa-check"></i>
                            Save
                        </button>

                        <a class="btn btn-warning" type="submit"
                           name="btnCancelSaveToDo" id="btnCancelSaveToDo"
                           href="<?= $root; ?>users-todos.php">
                            <i class="fa fa-remove"></i>
                            Cancel
                        </a>

                    </div>
                </div>

            </form>
        </div>
    </div>

<?php
include_once __DIR__ . "/site-footer.php";
