<?php
/**
 * Delete ToDos
 *
 * This page is used to Mark ToDos for Deletion on a user's list. It is
 * for logged in users only. Admin users will need a special page for deleting
 * todos for any user besides themselves.
 *
 * @File     :  /users-todos-delete.php
 * @Project  :  phpToDo
 * @Author   :  Adrian Gould <Adrian.Gould@polytechnic.wa.edu.au>
 * @Date     :  26/11/2016
 * @Version  :  1.0
 * @Copyright:  Copyright 2016 TAFE OpenSource,
 * Released under the Creative Commons Share Alike license
 *
 * History:
 *
 * v 1.0 24611/2016
 * Initial version
 */

if (session_status() !== PHP_SESSION_ACTIVE) {
    session_start();
}

require_once __DIR__ . "/vendor/autoload.php";

$title = "Clear | Deleted | ToDos | User";

use TAFEOpenSource\ToDos;
use TAFEOpenSource\Url;
use TAFEOpenSource\UserRoles;
use TAFEOpenSource\Users;
use TAFEOpenSource\UserStatuses;

$users    = new Users();
$roles    = new UserRoles();
$statuses = new UserStatuses();
$todos    = new ToDos();
$url      = new Url();
$root     = $url->getUriNoScript();

if ( ! isset($_POST) || empty($users->isUserLoggedIn())) {
    session_destroy();
    $_SESSION['userSession'] = false;
    $users->redirect('index.php');
} else {

    // get logged in user details
    $user       = $users->getUserById($users->getLoggedInUserId());
    $id         = $user->id;
    $username   = $user->username;
    $given      = $user->givenname;
    $last       = $user->lastname;
    $userRole   = $roles->getRoleByID($user->userrole);
    $userStatus = $statuses->getUserStatusByID($user->userstatus);
    $loginTime  = $user->date_login;

    $deletedToDos=$todos->getAllToDosByUserWithStatus($id,'DE');

        // check to see if the submit (btnClearDeleted) button was pressed
        if ( ! empty($_POST['btnConfirmClearDeleted'])) {
            // id = user id of whom is logged in
            $todos->clearDeletedToDos($id);
            // presume no errors and redirect
            $users->redirect("{$root}users-todos.php");

        } else {

            if ( ! isset($_POST['btnClearDeleted'])) {
                // redirect back to the user's ToDos
                $users->redirect($root . 'users-todos.php');
            } // end if not btnToDoDelete

        } //end if btnConfirmDeleteToDo

} // end if not logged in

include_once __DIR__ . "/site-header.php";

?>
    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                User Dashboard
                <small>ToDos</small>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="user-home.php"> Home</a>
                </li>
                <li class="active">
                    <i class="fa fa-list"></i>
                    <a href="users-todos.php"> ToDos</a>
                </li>
                <li class="active">
                    <i class="fa fa-remove"></i> Deleted
                </li>
                <li class="active">
                    <i class="fa fa-eraser"></i> Clear
                </li>
            </ol>
        </div>
    </div>
    <!-- /.row -->
    <div class="col-xs-12">
        <div class="alert alert-danger">
            <h2>Clear Deleted ToDos</h2>
            <p>You have <strong><?=count($deletedToDos);?></strong>
               ToDos marked for deletion.</p>
            <p>Are you sure you wish to clear all deleted ToDos from your
               list(s)?</p>
            <br>

            <form class="form-horizontal" method="post">

                <div class="form-group">

                    <div class="col-xs-6 col-sm-2">
                        <a class="btn btn-primary"
                           href="<?= $root ?>users-todos.php">
                            <i class="fa fa-stop"></i>
                            Cancel
                        </a>
                    </div>

                    <div class="col-xs-6 col-sm-8">
                        <button class="btn btn-danger" type="submit"
                                name="btnConfirmClearDeleted"
                                id="btnConfirmClearDeleted"
                                value="Clear!">
                            <i class="fa fa-play"></i>
                            Clear!
                        </button>
                    </div>

                </div>

            </form>

        </div>

    </div>

<?php

include_once __DIR__ . "/site-footer.php";
